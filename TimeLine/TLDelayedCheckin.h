//
//  TLDelayedCheckin.h
//  TimeLine
//
//  Created by Evgenii Oborin on 21.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


NS_ASSUME_NONNULL_BEGIN

@class TLPlaceData;

@interface TLDelayedCheckin : NSManagedObject

- (CLLocationCoordinate2D)locationCoordinate;

+ (id)maxValueWithKey:(NSString *)key;

+ (NSArray*) definedArrayInContext:(NSManagedObjectContext*) context;
+ (NSArray*) definedArray;

+ (NSArray*) notDefinedArray;

+ (TLDelayedCheckin*) checkinWithOrderValue:(NSNumber*) value inContext:(NSManagedObjectContext*) context;

@end

NS_ASSUME_NONNULL_END

#import "TLDelayedCheckin+CoreDataProperties.h"

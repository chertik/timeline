//
//  TLAddToMapList.m
//  TimeLine
//
//  Created by Evgenii Oborin on 29.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLAddToMapList.h"
#import "TLAddToMapListCell.h"

@interface TLAddToMapList () <TLAddToMapListCellDelegate>

@property (strong,nonatomic) NSMutableArray *tasksArray;

@property (strong,nonatomic) NSMutableArray *confirmedFlagsArray;

@property (strong,nonatomic) NSTimer *dissapearTimer;

@end

@implementation TLAddToMapList

static NSString * const reuseIdentifier = @"TLAddToMapCellReuseID";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.backgroundColor=[UIColor clearColor];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
//    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    self.tasksArray = [[NSMutableArray alloc] init];
    self.confirmedFlagsArray = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) addTask:(TLUserTask *)task{
    [self.dissapearTimer invalidate];
    [self.tasksArray insertObject:task atIndex:0];
    [self.confirmedFlagsArray insertObject:@0 atIndex:0];
    [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]];
}

- (void) clearTasks{
    [self.tasksArray removeAllObjects];
    [self.confirmedFlagsArray removeAllObjects];
    [self.collectionView reloadData];
}

- (BOOL) removerRecentTask{
    [self.tasksArray removeObjectAtIndex:0];
    [self.confirmedFlagsArray removeObjectAtIndex:0];
    [self.collectionView deleteItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]]];
    
    if(self.tasksArray.count){
        return NO;
    } else {
        return YES;
    }
}

- (void) updateInsets{
    
//    [self.collectionView setContentInset:<#(UIEdgeInsets)#>]
    
}

- (void) confirmTaskByCell:(TLAddToMapListCell *)cell {
    
    NSIndexPath *index=[self.collectionView indexPathForCell:cell];
    TLUserTask *task=self.tasksArray[index.row];
    [self.confirmedFlagsArray replaceObjectAtIndex:index.row withObject:@1];
    
    [task moveTaskToFuture];
    
    [self.delegate refreshMapData];

    if([self allAreTasksConfirmed]){
    
        
//        NSMethodSignature *ms=[NSMethodSignature methodSignatureForSelector:@selector(dissapear)];
//        NSInvocation *inv=[NSInvocation invocationWithMethodSignature:ms];
//        self.dissapearTimer=[NSTimer scheduledTimerWithTimeInterval:4 invocation:inv repeats:NO];
        
        self.dissapearTimer=[NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(dissapear) userInfo:nil repeats:NO];
        
//        self.dissapearTimer = [NSTimer scheduledTimerWithTimeInterval:4
//                                                              repeats:NO block:^(NSTimer * _Nonnull timer) {
//                                                                  [self dissapear];
//                                                              }];
    }
}

- (void) dissapear{
    [self.tasksArray removeAllObjects];
    [self.confirmedFlagsArray removeAllObjects];
    [self.collectionView reloadData];
    [self.delegate didClearedAllTasks];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL) allAreTasksConfirmed{
    for(NSNumber *num in self.confirmedFlagsArray){
        if(!num.boolValue)
            return NO;
    }
    return YES;
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.tasksArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TLAddToMapListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    cell.delegate=self;
    
    TLUserTask *task=self.tasksArray[indexPath.row];
    
    [task iconForSize:CGSizeMake(60, 60) callback:^(UIImage * _Nonnull image) {
        [cell.iconImageView setImage:image];
    }];
    
    cell.nameLabel.text=task.desc;
    [cell setButtonIsAdd:YES];    
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end

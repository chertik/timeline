//
//  UITableViewCell+Additional.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 08/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Additional)

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier width:(CGFloat)width;

@end

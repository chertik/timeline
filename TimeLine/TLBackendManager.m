//
//  TLBackendManager.m
//  TimeLine
//
//  Created by Evgenii Oborin on 07.06.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLBackendManager.h"
#import "TLUserLoginResponse.h"

@import RestKit;

#define kDefaultsKeyLastUserToken @"kDefaultsKeyLastUserToken"
#define kDefaultsKeyVKUserID @"kDefaultsKeyVKUserID"
#define kDefaultsKeyVKToken @"kDefaultsKeyVKToken"

@interface TLBackendManager()

@property (strong,nonatomic) NSString *lastToken;

@property (strong,nonatomic) NSString *vkUserID;
@property (strong, nonatomic) NSString *vkToken;

@property (strong,nonatomic) RKObjectManager *objectManager;

@end

@implementation TLBackendManager

@synthesize lastToken = _lastToken;
@synthesize vkUserID = _vkUserID;
@synthesize vkToken = _vkToken;

+ (instancetype) sharedManager{
    static TLBackendManager *manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager=[[TLBackendManager alloc]init];
    });
    return manager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initRestKit];
    }
    return self;
}

- (void) initRestKit{
    
    NSURL *baseURL = [NSURL URLWithString:@"http://lifein.western-studio.ru"];
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseURL];
    self.objectManager=objectManager;
    [RKMIMETypeSerialization registerClass:[RKNSJSONSerialization class] forMIMEType:@"text/html"];
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);

    //3.1.1 user-auth
    RKObjectMapping *userAuthMapping=[RKObjectMapping mappingForClass:[TLUserLoginResponse class]];
    [userAuthMapping addAttributeMappingsFromDictionary:@{
                                                          @"token_key" : @"tokenKey"
                                                          }];
    
    RKResponseDescriptor *uservkAuthDescriptor=[RKResponseDescriptor responseDescriptorWithMapping:userAuthMapping method:RKRequestMethodAny pathPattern:@"/user-auth-vk" keyPath:@"data" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:uservkAuthDescriptor];
    
    RKResponseDescriptor *userfbAuthDescriptor=[RKResponseDescriptor responseDescriptorWithMapping:userAuthMapping method:RKRequestMethodAny pathPattern:@"/user-auth-fb" keyPath:@"data" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:userfbAuthDescriptor];
    
    RKResponseDescriptor *userinstAuthDescriptor=[RKResponseDescriptor responseDescriptorWithMapping:userAuthMapping method:RKRequestMethodAny pathPattern:@"/user-auth-inst" keyPath:@"data" statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:userinstAuthDescriptor];
}

- (NSString*) lastToken{
    if(!_lastToken){
        _lastToken=[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsKeyLastUserToken];
    }
    return _lastToken;
}

- (void) setLastToken:(NSString *)lastToken{
    _lastToken = _lastToken;
    [[NSUserDefaults standardUserDefaults] setObject:lastToken forKey:kDefaultsKeyLastUserToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*) vkUserID {
    if(!_vkUserID){
        _vkUserID = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsKeyVKUserID];
    }
    return _vkUserID;
}

- (void) setVkUserID:(NSString *)vkUserID{
    _vkUserID = vkUserID;
    [[NSUserDefaults standardUserDefaults] setObject:vkUserID forKey:kDefaultsKeyVKUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*) vkToken {
    if(!_vkToken) {
        _vkToken = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsKeyVKToken];
    }
    
    return _vkToken;
}

- (void) setVkToken:(NSString *)vkToken {
    _vkToken = vkToken;
    [[NSUserDefaults standardUserDefaults] setObject:vkToken forKey:kDefaultsKeyVKToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL) isLoggedIn{
    if(self.lastToken){
        return YES;
    } else {
        return NO;
    }
}

- (void) loginInBackgroundWithVkId:(NSString*)vkId block:(void(^)(NSError *error))completionBlock{
    NSDictionary *parameters=@{
                               @"user_id":vkId
                               };
    [self.objectManager getObjectsAtPath:@"/user-auth-vk" parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        TLUserLoginResponse *loginResponse=mappingResult.firstObject;
        self.lastToken=loginResponse.tokenKey;
        self.vkUserID=vkId;
        self.vkToken = loginResponse.tokenKey;
    
        completionBlock(nil);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        completionBlock(error);
    }];
}


- (void) loginInBackgroundWithFbId:(NSString*)fbId block:(void(^)(NSError *error))completionBlock{
    NSDictionary *parameters=@{
                               @"user_id":fbId
                               };
    [self.objectManager getObjectsAtPath:@"/user-auth-fb" parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        TLUserLoginResponse *loginResponse=mappingResult.firstObject;
        self.lastToken=loginResponse.tokenKey;
        
        completionBlock(nil);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        completionBlock(error);
    }];
}


- (void) loginInBackgroundWithInstagramId:(NSString*)instagramId block:(void(^)(NSError *error))completionBlock{
    NSDictionary *parameters=@{
                               @"user_id":instagramId
                               };
    [self.objectManager getObjectsAtPath:@"/user-auth-inst" parameters:parameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        TLUserLoginResponse *loginResponse=mappingResult.firstObject;
        self.lastToken=loginResponse.tokenKey;
        
        completionBlock(nil);
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        completionBlock(error);
    }];
}


- (void) logoutInBackgroundWithBlock:(void (^)(NSError *error))completionBlock{
    self.lastToken=nil;
}


@end

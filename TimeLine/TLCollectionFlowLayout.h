//
//  TLCollectionFlowLayout.h
//  TimeLine
//
//  Created by Evgenii Oborin on 12.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLCollectionFlowLayout : UICollectionViewFlowLayout

@end

//
//  UIView+Graphics.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Graphics)

@property (nonatomic, assign) CGFloat heightEllipseCap;

- (void)setOrigin:(CGPoint)point;
- (void)setOrigin:(CGPoint)point animated:(BOOL)animated;
- (void)setAlpha:(CGFloat)alpha animated:(BOOL)animated;
- (void)setCornerRadius:(CGFloat)radius;

- (void)drawHalfEllipseWithBackgroundColor:(UIColor *)color opacity:(CGFloat)opacity;
- (void)drawHalfFlippedEllipseWithBackgroundColor:(UIColor *)color opacity:(CGFloat)opacity;
- (void)drawHalfEllipseWithBackgroundColor:(UIColor *)color opacity:(CGFloat)opacity flippedOver:(BOOL)flipped;
- (void)drawHalfEllipseWithShapeOffsetX:(CGFloat)shapeOffset_x backgroundColor:(UIColor *)color opacity:(CGFloat)opacity flippedOver:(BOOL)flipped;
- (void)drawHalfEllipseWithShapeOffsetX:(CGFloat)shapeOffset_x heightShapeCap:(CGFloat)heightShapeCap backgroundColor:(UIColor *)color opacity:(CGFloat)opacity flippedOver:(BOOL)flipped;

- (void)setShadowWithColor:(UIColor *)color radius:(CGFloat)radius opacity:(CGFloat)opacity;

- (void)slideTopWithDuration:(NSTimeInterval)interval Offset:(CGPoint)offset completion:(void (^)())completionBlock;
- (void)slideBottomWithDuration:(NSTimeInterval)interval Offset:(CGPoint)offset completion:(void (^)())completionBlock;

@end
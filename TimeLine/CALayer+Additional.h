//
//  CALayer+Additional.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 10/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (Additional)

+ (instancetype)squareLayerWithBorderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color cornerRadius:(CGFloat)radius;
+ (instancetype)rhombusLayerWithBorderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color cornerRadius:(CGFloat)radius;

@end

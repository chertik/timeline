//
//  ASMapView.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface ASMapView : GMSMapView

- (void)setupParameters;
- (void)setPositionWithLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude zoom:(float)zoom;
- (void)addMarkerWithCoordinate:(CLLocationCoordinate2D)coordinate image:(UIImage *)image title:(NSString *)title zindex:(NSInteger)zindex anchorPoint:(CGPoint) anchorPoint;
- (void)addRouteFrom:(CLLocationCoordinate2D)coordinateA to:(CLLocationCoordinate2D)coordinateB withZIndex:(NSInteger)zindex;
- (void)removePolylineWithZIndex:(NSInteger)zindex;

@end

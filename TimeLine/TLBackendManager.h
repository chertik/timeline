//
//  TLBackendManager.h
//  TimeLine
//
//  Created by Evgenii Oborin on 07.06.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLBackendManager : NSObject

+ (instancetype) sharedManager;

- (BOOL) isLoggedIn;
- (NSString*) lastToken;
- (NSString*) vkUserID;
- (NSString*) vkToken;

- (void) loginInBackgroundWithVkId:(NSString*)vkId block:(void(^)(NSError *error))completionBlock;
- (void) loginInBackgroundWithFbId:(NSString*)fbId block:(void(^)(NSError *error))completionBlock;
- (void) loginInBackgroundWithInstagramId:(NSString*)instagramId block:(void(^)(NSError *error))completionBlock;
- (void) logoutInBackgroundWithBlock:(void (^)(NSError *error))completionBlock;

@end

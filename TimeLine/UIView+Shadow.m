//
//  UIView+Shadow.m

#import "UIView+Shadow.h"

#define kShadowViewTag 2132
#define kValidDirections [NSArray arrayWithObjects: @"top", @"bottom", @"left", @"right",nil]

@implementation UIView (Shadow)

- (void)makeInsetShadow {
    NSArray *shadowDirections = [NSArray arrayWithObjects:@"top", @"bottom", @"left" , @"right" , nil];
    UIColor *color = [UIColor colorWithRed:(0.0) green:(0.0) blue:(0.0) alpha:0.5];
    
    UIView *shadowView = [self createShadowViewWithRadius:3 color:color directions:shadowDirections];
    shadowView.tag = kShadowViewTag;
    
    [self addSubview:shadowView];
}

- (void)makeInsetShadowWithRadius:(float)radius alpha:(float)alpha {
    NSArray *shadowDirections = [NSArray arrayWithObjects:@"top", @"bottom", @"left" , @"right" , nil];
    UIColor *color = [UIColor colorWithRed:(0.0) green:(0.0) blue:(0.0) alpha:alpha];
    
    UIView *shadowView = [self createShadowViewWithRadius:radius color:color directions:shadowDirections];
    
    shadowView.tag = kShadowViewTag;
    shadowView.alpha = 0;
    
    [self addSubview:shadowView];
    
    [UIView animateWithDuration:0.35f animations:^{
        shadowView.alpha = 1.0f;
    }];
}

- (void)makeTopInsetShadowWithRadius:(CGFloat)radius color:(UIColor*)color alpha:(CGFloat)alpha {
    if ([self viewWithTag:kShadowViewTag])
        [[self viewWithTag:kShadowViewTag] removeFromSuperview]; 
    
    NSArray *shadowDirections = [NSArray arrayWithObjects:@"top", nil];
    UIView *shadowView = [self createShadowViewWithRadius:radius color:color directions:shadowDirections];
    
    shadowView.tag = kShadowViewTag;
    shadowView.alpha = 0;
    
    [self addSubview:shadowView];
    
    [UIView animateWithDuration:0.4f animations:^{
        shadowView.alpha = 1.0f;
    }];
}

- (void)makeInsetShadowWithRadius:(float)radius color:(UIColor *)color directions:(NSArray *)directions {
    UIView *shadowView = [self createShadowViewWithRadius:radius color:color directions:directions];
    
    shadowView.tag = kShadowViewTag;
    shadowView.alpha = 0;
    
    [self addSubview:shadowView];
    
    [UIView animateWithDuration:0.4f animations:^{
        shadowView.alpha = 1.0f;
    }];
}

- (UIView *)createShadowViewWithRadius:(float)radius color:(UIColor *)color directions:(NSArray *)directions {
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    shadowView.backgroundColor = [UIColor clearColor];
    
    // Ignore duplicate direction
    NSMutableDictionary *directionDict = [[NSMutableDictionary alloc] init];
    for (NSString *direction in directions) [directionDict setObject:@"1" forKey:direction];
    
    for (NSString *direction in directionDict) {
        // Ignore invalid direction
        if ([kValidDirections containsObject:direction])
        {
            CAGradientLayer *shadow = [CAGradientLayer layer];
            
            if ([direction isEqualToString:@"top"]) {
                [shadow setStartPoint:CGPointMake(0.5, 0.0)];
                [shadow setEndPoint:CGPointMake(0.5, 1.0)];
                shadow.frame = CGRectMake(0, 0, self.bounds.size.width, radius);
            }
            else if ([direction isEqualToString:@"bottom"])
            {
                [shadow setStartPoint:CGPointMake(0.5, 1.0)];
                [shadow setEndPoint:CGPointMake(0.5, 0.0)];
                shadow.frame = CGRectMake(0, self.bounds.size.height - radius, self.bounds.size.width, radius);
            } else if ([direction isEqualToString:@"left"])
            {
                shadow.frame = CGRectMake(0, 0, radius, self.bounds.size.height);
                [shadow setStartPoint:CGPointMake(0.0, 0.5)];
                [shadow setEndPoint:CGPointMake(1.0, 0.5)];
            } else if ([direction isEqualToString:@"right"])
            {
                shadow.frame = CGRectMake(self.bounds.size.width - radius, 0, radius, self.bounds.size.height);
                [shadow setStartPoint:CGPointMake(1.0, 0.5)];
                [shadow setEndPoint:CGPointMake(0.0, 0.5)];
            }
            
            shadow.colors = [NSArray arrayWithObjects:(id)[color CGColor], (id)[[UIColor clearColor] CGColor], nil];
            [shadowView.layer insertSublayer:shadow atIndex:0];
        }
    }
    
    [shadowView.layer setMasksToBounds:YES];
    [shadowView.layer setCornerRadius:2.0f];
    
    return shadowView;
}

- (void)removeInsetShadow {
    UIView *shadowView = [self viewWithTag:kShadowViewTag];
    
    [UIView animateWithDuration:0.4f animations:^{
        shadowView.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [shadowView removeFromSuperview];
    }];
}

@end

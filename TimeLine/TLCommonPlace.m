//
//  TLCommonPlace.m
//  TimeLine
//
//  Created by Evgenii Oborin on 25.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLCommonPlace.h"

@implementation TLCommonPlace

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name=@"Some coole place";
    }
    return self;
}

+ (instancetype) commonPlaceWithGooglePlace:(TLGPlace *)place{
    
    TLCommonPlace *commonPlace=[[TLCommonPlace alloc] init];
    
    commonPlace.name=place.name;
    
    commonPlace.address=[place.details shortAddress];
    commonPlace.rating=place.details.rating;
    commonPlace.distance=place.distance;
    
    return commonPlace;
}

+ (instancetype) commonPlaceWithFoursquarePlace:(TLVenue *)venue{
    
    TLCommonPlace *commonPlace=[[TLCommonPlace alloc] init];
    
    commonPlace.name=venue.name;
    commonPlace.address=[venue getShortAddress];
    commonPlace.rating=venue.rating;
    commonPlace.distance=venue.distance;
    
    return commonPlace;
}

- (NSString*) description {
    return [NSString stringWithFormat:@"Name:%@ distance:%f rating:%@ address:%@",self.name,self.distance.doubleValue,self.rating,self.address];
}

@end

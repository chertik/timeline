//
//  TLTimelineVC.m
//  TimeLine
//
//  Created by Evgenii Oborin on 28.09.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLTimelineVC.h"
#import "TLTimelineCollectionCell.h"
#import "TLUserTask.h"
#import "UIImage+TLIcon.h"
#import "TLListCollectionVC.h"
#import "TLDetailVC.h"
#import "UICollectionView+NSFetchedResultsController.h"
#import "TLPlaceData+CoreDataClass.h"
#import "TLGeoManager.h"
#import "TLThemeManager.h"
#import "Constants.h"
#import "TLBackendManager.h"


#define kGrayTimeLineDot @"GrayTimelinedot"
#define kPinkTimeLineDot @"PinkTimelineDot"

#define kAutocheckinMarkRed @"AutocheckinMarkRed"
#define kAutocheckinMarkGray @"AutocheckinMarkGray"
#define kManualcheckinMark @"ManualcheckinMarkPink"


@interface TLTimelineVC () <UICollectionViewDelegate,UICollectionViewDataSource, NSFetchedResultsControllerDelegate, UIScrollViewDelegate,TLListCollectionVCDelegate,TLDetailVCDelegate,TLTimelineCollectionCellDelegate,UICollectionViewDelegateFlowLayout>

// OUTLETS ----------------------------------------------------------------------------------------------------------
@property (weak,nonatomic) CAGradientLayer *gradientLine;

@property (weak, nonatomic) IBOutlet UIView *upperView;
@property (weak, nonatomic) IBOutlet UIView *navContainerView;
@property (weak, nonatomic) IBOutlet UIView *collectionContainerView;
@property (weak, nonatomic) IBOutlet UIButton *leftArrowButton;
@property (weak, nonatomic) IBOutlet UIButton *rightArrowButton;

@property (weak,nonatomic) UIView *dismissalView;

@property (strong,nonatomic) TLListCollectionVC *childListCollectionVC;

//For extra-compact view
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *upperViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftArrowTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftArrowLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightArrowTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightArrowTrailingConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewTrailingConstraint;

// PROPERTIES ----------------------------------------------------------------------------------------------------------
@property (strong, nonatomic) NSFetchedResultsController *fetchedTimeline;


@property (strong,nonatomic) UINavigationController *childNavigationController;

@property (strong,nonatomic) UIScrollView *childScrollView;
@property (strong,nonatomic) TLDetailVC *childDetailVC;

@property (nonatomic) BOOL isManualMoving;
@property (nonatomic) BOOL addedToMap;
@property (nonatomic) BOOL removedFromMap;

@property (strong,atomic) NSString *currentAddress;
@property (atomic) BOOL hasCurrentAddressCell;
@property (nonatomic) NSInteger currentAddressInd;

@property (nonatomic) NSRange visibleRange;




@end

@implementation TLTimelineVC
{
    NSInteger gradientInd; //Gradient breaking index in collection view
    NSDateFormatter *cellDateFormatter;

    //CollectionView reorder variables
    CGFloat draggingBeginYCoord;
    NSIndexPath *movingIndexPath;
    TLTimelineCollectionCell *movingCollectionCell;
    
    //Extra-CompactView variables
    CGFloat extraCompactViewPercent;
    BOOL isInteractivelyHiding;
    CGFloat collectionViewXOffsetAtStartOfInteraction;
}

static NSString* const reuseId=@"CollectionCellReuseID";

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Initial values
    self.contentState=TLPanelPresentationStateTable;
    self.isManualMoving=NO;
    self.addedToMap=NO;
    self.removedFromMap=NO;
    self.hasCurrentAddressCell=NO;
    self.visibleRange=NSMakeRange(1, 0);
    
    self.detailPanelHeight=370;
    
    extraCompactViewPercent=0;
    
    // Collection setup
    [self.collectionView registerNib:[UINib nibWithNibName:@"TLTimelineCollectionCell" bundle:nil] forCellWithReuseIdentifier:reuseId];
    
    self.collectionView.contentInset=UIEdgeInsetsMake(0, 26, 0, 26);
    
    //Setup dateformatter
    cellDateFormatter=[[NSDateFormatter alloc] init];
    [cellDateFormatter setDateFormat:@"dd.MM"];
    
    //Setup re-ordering gesture recognizer
    UILongPressGestureRecognizer *gr=[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleCollectionLongTap:)];
    [self.collectionView addGestureRecognizer:gr];
    
    //Setup collectionView layout parameters
    UICollectionViewFlowLayout *layout=(UICollectionViewFlowLayout*) self.collectionView.collectionViewLayout;
    
    layout.sectionInset=UIEdgeInsetsMake(0, 0, 0, 0);
    layout.minimumLineSpacing=0;
    layout.minimumInteritemSpacing=1000.0;
    layout.itemSize=CGSizeMake(58, 78);
    layout.scrollDirection=UICollectionViewScrollDirectionHorizontal;
    
    //Gradient line setup
    CAGradientLayer *gradientLine=[CAGradientLayer layer];
    gradientLine.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 2);
    gradientLine.colors = [NSArray arrayWithObjects:(id)[TLThemeManager lighterGrayColor].CGColor, (id)dThemeColorRed.CGColor, nil];
    gradientLine.startPoint = CGPointMake(-1,0);
    gradientLine.endPoint = CGPointMake(-0.5, 0);
    
    [self.stripView.layer addSublayer:gradientLine];
    self.gradientLine=gradientLine;
    
    CGRect bounds=self.view.bounds;
    bounds.size.width=[UIScreen mainScreen].bounds.size.width; 
    self.view.bounds=bounds;
    
    //Blur view setup
    Class customClass=NSClassFromString(@"_UICustomBlurEffect");
    UIBlurEffect *customBlurEffect=[[customClass alloc] init];
    [customBlurEffect setValue:@2.5 forKey:@"blurRadius"];
    UIVisualEffectView *blurView=[[UIVisualEffectView alloc] initWithEffect:customBlurEffect];
    blurView.frame=self.view.bounds;
    [self.view insertSubview:blurView atIndex:0];
    
    //Setup child VC's
    [self setupViewControllers];
    
    if(![[TLBackendManager sharedManager] isLoggedIn]){
        self.collectionView.alpha=0;
        self.navContainerView.alpha=0;
    }
    
    //Registering for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCurrentAddress:) name:kGeoManagerUpdatedCurrentAddress object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(firstLocationUpdate:) name:kGeoManagerUpdatedCurrentLocation object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshTimeline:) name:kTLAddedCheckins object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTimelineAfterRegistration:) name:kUserdefaultsPassedAuthorization object:nil];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void) setupViewControllers{
    
    UIScrollView *scrollView=[[UIScrollView alloc] init];
    self.childScrollView=scrollView;
    scrollView.delegate=self;
    scrollView.translatesAutoresizingMaskIntoConstraints=NO;
    scrollView.scrollEnabled=NO;
    scrollView.delaysContentTouches=NO;
    [self.navContainerView addSubview:scrollView];
    [scrollView.topAnchor constraintEqualToAnchor:self.navContainerView.topAnchor].active=YES;
    [scrollView.leftAnchor constraintEqualToAnchor:self.navContainerView.leftAnchor].active=YES;
    [scrollView.rightAnchor constraintEqualToAnchor:self.navContainerView.rightAnchor].active=YES;
    [scrollView.bottomAnchor constraintEqualToAnchor:self.navContainerView.bottomAnchor].active=YES;
    
    [self.navContainerView layoutIfNeeded];
    scrollView.contentSize = CGSizeMake(self.navContainerView.bounds.size.width*2, self.navContainerView.bounds.size.height);
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc]init];
    self.childListCollectionVC=[[TLListCollectionVC alloc] initWithCollectionViewLayout:layout];
    self.childListCollectionVC.delegate=self;
    self.childListCollectionVC.collectionView.contentInset=UIEdgeInsetsMake(0, 0, 10, 0);
    [self addChildViewController:self.childListCollectionVC];
    self.childListCollectionVC.view.translatesAutoresizingMaskIntoConstraints=NO;
    [self.childListCollectionVC didMoveToParentViewController:self];
    [self.childListCollectionVC setAutomaticallyAdjustsScrollViewInsets:NO];
    
    [scrollView addSubview:self.childListCollectionVC.view];
    self.childListCollectionVC.view.frame=self.navContainerView.bounds;
    
    TLDetailVC *detailVC=[[TLDetailVC alloc] init];
    detailVC.delegate=self;
    self.childDetailVC=detailVC;
    CGRect frame=CGRectMake(self.navContainerView.bounds.size.width,0,self.navContainerView.bounds.size.width,242);
    detailVC.view.frame=frame;
    
    [self addChildViewController:detailVC];
    [scrollView addSubview:detailVC.view];    
    [detailVC didMoveToParentViewController:self];
}

- (void) viewDidLayoutSubviews{

    //For correctly setting frames before setting gradient line
    [self.upperView layoutIfNeeded];

    BOOL fade=YES;
    
    if(fade){
        
        CGFloat crossoverPercent=1-kCompactViewCollectionViewTopConstraintDif/kCompactViewHeightDif;
        
        //Setup CollectionView mask for fading
        CALayer *maskLayer=[CALayer layer];
        maskLayer.frame=self.collectionView.superview.bounds;
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        CGRect colBounds=self.collectionView.superview.bounds;
        CGFloat expand=10.0;
        
        CGFloat gradientxExpand;
        if(extraCompactViewPercent<crossoverPercent){
            gradientxExpand=26*(1-extraCompactViewPercent/crossoverPercent*2);
        } else{
            gradientxExpand=-26;
        }
        gradient.frame =CGRectMake(gradientxExpand, -expand, colBounds.size.width-2*gradientxExpand, colBounds.size.height+2*expand);
//        gradient.frame =CGRectMake(0, -expand, colBounds.size.width, colBounds.size.height+2*expand);
        gradient.colors = @[(id)[UIColor clearColor].CGColor, (id)[UIColor blackColor].CGColor,(id)[UIColor blackColor].CGColor,(id)[UIColor clearColor].CGColor];
        // Here, percentage would be the percentage of the collection view
        // you wish to blur from the top. This depends on the relative sizes
        // of your collection view and the header.
        CGFloat margin=0.025;
//        CGFloat margin=0.1;
        gradient.locations = @[@0.0, @(margin),@(1-margin),@1.0];
        gradient.startPoint=CGPointMake(0.0, 0.5);
        gradient.endPoint=CGPointMake(1.0, 0.5);
        
        [maskLayer addSublayer:gradient];
        
        CALayer *lowerPartMaskSublayer=[CALayer layer];
        lowerPartMaskSublayer.backgroundColor=[UIColor blackColor].CGColor;
        CGFloat lowerPartY;//=50;
        
        
        
        if(extraCompactViewPercent<crossoverPercent){
            lowerPartY=50-extraCompactViewPercent*kCompactViewHeightDif;
        } else{
            lowerPartY=50-kCompactViewCellHeightDif;
        }
        
        CGFloat xExpand=100;
        lowerPartMaskSublayer.frame=CGRectMake(-xExpand, lowerPartY+expand, colBounds.size.width+2*xExpand, colBounds.size.height+2*expand-lowerPartY);
        
        [maskLayer addSublayer:lowerPartMaskSublayer];
        
//        [self.collectionView.superview.layer addSublayer:maskLayer];
        self.collectionView.superview.layer.mask = maskLayer;
    } else {
        CALayer *maskLayer=[CALayer layer];
        CGRect colBounds=self.collectionView.superview.bounds;
        CGFloat expand=10.0;
        maskLayer.frame =CGRectMake(0, -expand, colBounds.size.width, colBounds.size.height+2*expand);
        maskLayer.backgroundColor=[UIColor blackColor].CGColor;
        self.collectionView.superview.layer.mask=maskLayer;
        
    }
    
    gradientInd=[self findGradientBreakInd];
    
    if(!self.isEditing &&!isInteractivelyHiding){
        if([self numberOfItems]!=0){
            if(gradientInd>=[self numberOfItems]){
                [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[self numberOfItems]-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
            } else if(gradientInd<0){
                 [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
            } else {
                [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:gradientInd inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
            }
        }
    }
    [self setGradientToIndex:gradientInd];
    [self recalculateVisibleTasksAndSendCallback];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark custom setups

- (NSFetchedResultsController *)fetchedTimeline {
    
    if (_fetchedTimeline != nil) {
        return _fetchedTimeline;
    }
    _fetchedTimeline = [TLUserTask MR_fetchAllSortedBy:@"id.integerValue" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    return _fetchedTimeline;
}


#pragma mark - CollectionView

#pragma mark Methods for dealing with Current Address Cell

- (void) firstLocationUpdate:(NSNotification*) note{
    
    BOOL firstUpdate=!self.hasCurrentAddressCell;
    
    self.hasCurrentAddressCell=YES;
    self.currentAddressInd=[self findCurrentAddressInd];
    
    if(firstUpdate){
        gradientInd=[self findGradientBreakInd];
        if ([self numberOfItems]==1) {
            [self.collectionView reloadData];
        } else {
            if (self.collectionView.window != nil){
                [self.collectionView insertItemsAtIndexPaths:@[
                                                               [NSIndexPath indexPathForRow:self.currentAddressInd inSection:0]
                                                               ]];
            } else {
                [self.collectionView reloadData];
            }
        }
        
        
        [self setGradientToIndex:gradientInd];
    }
}

- (void) changeCurrentAddress:(NSNotification*) note{
    self.currentAddress=[[TLGeoManager sharedManager] currentAddress];
}

- (NSInteger) numberOfItems{
    
    if(!self.hasCurrentAddressCell){
        return [[[self.fetchedTimeline sections] objectAtIndex:0] numberOfObjects];
    } else {
        return [[[self.fetchedTimeline sections] objectAtIndex:0] numberOfObjects]+1;
    }
}

- (NSInteger) findCurrentAddressInd{
    //Find first non-past cell
    NSPredicate *predicateNonPast = [NSPredicate predicateWithFormat:@"is_past = %@", @0];
    TLUserTask *firstNonPastTask=[TLUserTask MR_findFirstWithPredicate:predicateNonPast sortedBy:@"id" ascending:YES];
    
    //define if have past tasks
    BOOL havePast=NO;
    NSPredicate *predicatePast = [NSPredicate predicateWithFormat:@"is_past = %@",@1];
    TLUserTask *firstPastTask=[TLUserTask MR_findFirstWithPredicate:predicatePast];
    if (firstPastTask) { havePast=YES; }
    
    
    //define index
    if (firstNonPastTask){
        if(havePast){
            return firstNonPastTask.id.integerValue-1;
        } else {
            return 0;
        }
    } else {
        return [[[self.fetchedTimeline sections] objectAtIndex:0] numberOfObjects];
    }
}

- (TLUserTask*) taskForIndex:(NSIndexPath*) index{
    
    TLUserTask *userTask;
    if(!self.hasCurrentAddressCell){
        userTask = [_fetchedTimeline objectAtIndexPath:index];
    } else {
        if(index.row>self.currentAddressInd){
            userTask=[_fetchedTimeline objectAtIndexPath:[NSIndexPath indexPathForRow:index.row-1 inSection:0]];
        } else if(index.row<self.currentAddressInd){
            userTask = [_fetchedTimeline objectAtIndexPath:index];
        } else {
            return nil;
        }
    }
    
    return userTask;
}


#pragma mark - Collection Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self numberOfItems];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TLTimelineCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseId forIndexPath:indexPath];
    
    TLUserTask *userTask = [self taskForIndex:indexPath];

    UIImage *imageForDot;
    if(userTask){
        //FOR REAL TASKS
        
        //0 - Scaling
        cell.borderView.transform=CGAffineTransformIdentity;
        cell.borderView.contentScaleFactor=[[UIScreen mainScreen] scale];

        //1 - Border and dot color
        if(userTask.is_past.boolValue){
            cell.borderView.layer.borderColor=dThemeColorGray.CGColor;
            imageForDot=[UIImage imageNamed:kGrayTimeLineDot];
        } else {
            cell.borderView.layer.borderColor=dThemeColorRed.CGColor;
            imageForDot=[UIImage imageNamed:kPinkTimeLineDot];
        }
        [cell.colorDot setImage:imageForDot];
        
        //2 - Icon image
        if (userTask.taskPhoto && userTask.taskPhoto.count != 0) {
            TLUserTaskPhoto *firstPhoto = userTask.taskPhoto.firstObject;
            [cell.imageView setImage: (UIImage*) firstPhoto.image];
            cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
        } else {
            [userTask iconForSize:CGSizeMake(38, 38) callback:^(UIImage * _Nonnull image) {
                [cell.imageView setImage:image];
                cell.imageView.contentMode = UIViewContentModeScaleToFill;
            }];
        }
        
        //3 - Name label
        cell.nameLabel.text=userTask.desc;
        cell.idLabel.text=[NSString stringWithFormat:@"id %ld",userTask.id.integerValue];
        NSDateFormatter *timeFormatter=[[NSDateFormatter alloc] init];
        timeFormatter.dateFormat=@"hh:mm:ss";
        cell.timeLabel.text=[timeFormatter stringFromDate:userTask.datetime];
        
        //4 - Date label
        if(!userTask.datetime){
            NSLog(@"userTaskWithID:%ld doesn't have datetime",userTask.id.integerValue);
        }
        cell.dateLabel.text=[self relativeDateStringFromDate:userTask.datetime];

        //5 - Checkin marker
        cell.markView.hidden=NO;
        if(userTask.is_checked.boolValue){
            if(userTask.is_autocheckin.boolValue){
                [cell.markView setImage:[UIImage imageNamed:kAutocheckinMarkRed]];
            } else {
                [cell.markView setImage:[UIImage imageNamed:kManualcheckinMark]];
            }
        } else if(userTask.is_autocheckin.boolValue){
            [cell.markView setImage:[UIImage imageNamed:kAutocheckinMarkGray]];
        } else {
            cell.markView.hidden=YES;
        }
        
        //6 - Wiggling, delete button
        if(self.editing){
            if(!userTask.is_past){
                [cell startWiggling];
            } else{
                [cell stopWiggling];
            }
            
            [cell showDeleteButtonAnimated:NO];
        } else {
            [cell stopWiggling];
             [cell hideDeleteButtonAnimated:NO];
        }
        
        
    } else {
        //FOR CURRENT ADDRESS CELL
        
        //0 - Scaling
        CGFloat enlargeScale=1.18421053;
        cell.borderView.contentScaleFactor=[[UIScreen mainScreen] scale]*enlargeScale;
        CGAffineTransform scTr=CGAffineTransformMakeScale(enlargeScale, enlargeScale);
        CGAffineTransform trans=CGAffineTransformTranslate(scTr, 0, -2);
        cell.borderView.transform=trans;
        
        //1 - Border and dot color
        cell.borderView.layer.borderColor=dThemeColorRed.CGColor;
        imageForDot=[UIImage imageNamed:kPinkTimeLineDot];
        [cell.colorDot setImage:imageForDot];
        
        
        //2 - Icon image
        [cell.imageView setImage:[UIImage imageNamed:@"CurAddrIcAltrntv2"]];

        //3 - Name label ??
        cell.nameLabel.text=@"Вы тут";
        
        //4 - Date label
        cell.dateLabel.text=@"Сейчас";
        
        
        //5 - Checkin marker
        cell.markView.hidden=YES;
        
        //6 - Wiggling
        [cell stopWiggling];
        [cell hideDeleteButtonAnimated:NO];
    }
    
    
    if(extraCompactViewPercent==1){
        cell.iconContainerView.alpha=0;
    } else {
        cell.iconContainerView.alpha=1;
        cell.iconContainerView.transform=CGAffineTransformIdentity;
    }
    
    
    cell.delegate=self;
    
    return cell;
}

- (void) collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    TLUserTask *movedTask=[self taskForIndex:sourceIndexPath];
    
    TLUserTask *destinationTask=[self taskForIndex:destinationIndexPath];
    [movedTask moveToId:destinationTask.id.integerValue];
    NSLog(@"from index:%@ to index:%@",sourceIndexPath,destinationIndexPath);
    NSLog(@"movedTask id:%ld",movedTask.id.integerValue);
   
    
    //[self.collectionView reloadData];
    
}

- (BOOL) collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath{
    TLUserTask *task=[self taskForIndex:indexPath];
    if(task){
        if(task.is_past.boolValue){
            return NO;
        }
    }
    if(self.hasCurrentAddressCell){
        if(indexPath.row!=[self currentAddressInd]){
            return YES;
        } else {
            return NO;
        }
    } else {
        return YES;
    }
}

#pragma mark Delegate

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
  
    [self exitEditingMode];
    
    TLUserTask *userTask=[self taskForIndex:indexPath];
    
    if(userTask){
        [self.delegate zoomToCoordinate:userTask.locationCoordinate];
    } else{
        CLLocation *curLoc=[[TLGeoManager sharedManager] getCurrentLocation];
        [self.delegate zoomToCoordinate:curLoc.coordinate];
    }
    
    [self pushDetailViewWithTask:userTask animated:self.contentState==TLPanelPresentationStateCompact? NO : YES];
    
//    [self setDetailPhoto:(UIImage*)userTask.taskPhoto.image];
//
//    for(TLPlaceData *placeD in userTask.placesList)
//        NSLog(@"%@",placeD);
    
//     [self.childDetailVC setUserTask:userTask];
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView==self.childScrollView){ //scrolling list
        return;
    }
    //scrolling collection
    [self setGradientToIndex:gradientInd];

    
}

- (void) recalculateVisibleTasksAndSendCallback{
    NSArray<NSIndexPath*> *visiblePaths=[self.collectionView indexPathsForVisibleItems];
    if(visiblePaths.count==0) return;
    
    NSInteger maxIndRow=[visiblePaths firstObject].row;
    
    for(NSIndexPath *indPath in visiblePaths){
        if(indPath.row>maxIndRow){
            maxIndRow=indPath.row;
        }
    }
    
    NSInteger minIndRow=[visiblePaths firstObject].row;
    
    for(NSIndexPath *indPath in visiblePaths){
        if(indPath.row<minIndRow){
            minIndRow=indPath.row;
        }
    }
    
    TLUserTask *firstTask=[self taskForIndex:[NSIndexPath indexPathForRow:minIndRow inSection:0]];
    NSInteger firstId,lastId;
    if(firstTask){
        firstId=firstTask.id.integerValue;
    } else{
        firstId=maxIndRow+2;
    }
    
    TLUserTask *lastTask=[self taskForIndex:[NSIndexPath indexPathForRow:maxIndRow inSection:0]];
    if(lastTask){
        lastId=lastTask.id.integerValue;
    } else{
        lastId=maxIndRow;
    }
    
    NSInteger length;
    
    if(lastId-firstId<0){
        length=0;
    }
    
    NSRange range=NSMakeRange(firstId, lastId-firstId);
    NSLog(@"%@",NSStringFromRange(range));
    
    if(range.length==self.visibleRange.length && range.location==self.visibleRange.location){
        return;
    } else{
        [self.delegate changedVisibleTasksToRange:range];
        self.visibleRange=range;
    }
}

-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    [self recalculateVisibleTasksAndSendCallback];
}

- (void) scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self recalculateVisibleTasksAndSendCallback];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self recalculateVisibleTasksAndSendCallback];
}

- (NSIndexPath *)collectionView:(UICollectionView *)collectionView targetIndexPathForMoveFromItemAtIndexPath:(NSIndexPath *)originalIndexPath toProposedIndexPath:(NSIndexPath *)proposedIndexPath{
    
    if(proposedIndexPath.row<0) { return [NSIndexPath indexPathForRow:0 inSection:0]; }
    if(proposedIndexPath.row>=[self numberOfItems]) { return [NSIndexPath indexPathForRow:[self numberOfItems]-1 inSection:0]; }

   TLUserTask *movedTask=[self taskForIndex:originalIndexPath];
    
    if(movedTask.is_past.boolValue){ //for moving past task
//        TLUserTask *taskOnLeftOfProposedIndex=[self taskForIndex:proposedIndexPath];
//        if(taskOnLeftOfProposedIndex){
//            if(taskOnLeftOfProposedIndex.is_past.boolValue){
//                return proposedIndexPath;
//            } else { //return index of last past task
//                NSPredicate *predicateIsPast=[NSPredicate predicateWithFormat:@"is_past = %@",@1];
//                
//                TLUserTask *lastPastTask=[TLUserTask MR_findFirstWithPredicate:predicateIsPast sortedBy:@"id" ascending:NO];
//                return [NSIndexPath indexPathForRow:lastPastTask.id.integerValue-1 inSection:0];
//            }
//        } else {
//            NSPredicate *predicateIsPast=[NSPredicate predicateWithFormat:@"is_past = %@",@1];
//            
//            TLUserTask *lastPastTask=[TLUserTask MR_findFirstWithPredicate:predicateIsPast sortedBy:@"id" ascending:NO];
//            return [NSIndexPath indexPathForRow:lastPastTask.id.integerValue-1 inSection:0];
//
//        }
        return originalIndexPath;
    } else {
        // for future tasks
        
        TLUserTask *taskOnRightOfProposedIndex=[self taskForIndex:[NSIndexPath indexPathForRow:proposedIndexPath.row inSection:0]];
        NSLog(@"original row:%ld. proposed row:%ld",originalIndexPath.row,proposedIndexPath.row);
        NSLog(@"original task id:%ld proposed task id:%ld",movedTask.id.integerValue,taskOnRightOfProposedIndex? taskOnRightOfProposedIndex.id.integerValue : -1 );
        if(taskOnRightOfProposedIndex){
            if(!taskOnRightOfProposedIndex.is_past.boolValue){
                return proposedIndexPath;
            } else { //return index of first non-past task
                NSPredicate *predicateIsPast=[NSPredicate predicateWithFormat:@"is_past = %@",@0];
                
                TLUserTask *lastPastTask=[TLUserTask MR_findFirstWithPredicate:predicateIsPast sortedBy:@"id" ascending:YES];
                if(self.hasCurrentAddressCell){
                    return [NSIndexPath indexPathForRow:lastPastTask.id.integerValue inSection:0];
                } else {
                    return [NSIndexPath indexPathForRow:lastPastTask.id.integerValue-1 inSection:0];
                }
            }
        } else {
            return [NSIndexPath indexPathForRow:proposedIndexPath.row+1 inSection:0];
        }
        return proposedIndexPath;
    }
}

- (void) collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TLTimelineCollectionCell *tlCell=(TLTimelineCollectionCell*) cell;
    if(self.editing){
        TLUserTask *task=[self taskForIndex:indexPath];
        if(task) {
            if(!task.is_past.boolValue){
                [tlCell startWiggling];
            } else{
                [tlCell stopWiggling];
            }
        } else{
            [tlCell stopWiggling];
        }
        
    } else {
        [tlCell stopWiggling];
    }
}

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
 return YES;
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 - (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
 }
 
 - (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
 }
 
 - (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
 }
 */

#pragma mark - FetchedResultsController Delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{

//    [self.collectionView addChangeForSection:sectionInfo atIndex:sectionIndex forChangeType:type];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
/*
    NSIndexPath *indexPathProcessed, *newIndexPathProcessed;
    if(self.hasCurrentAddressCell){
        if(type==NSFetchedResultsChangeMove){
            if(indexPath.row>=self.currentAddressInd){
                indexPathProcessed=[NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
            } else {
                indexPathProcessed=indexPath;
            }
            if(newIndexPath.row>=self.currentAddressInd){
                newIndexPathProcessed=[NSIndexPath indexPathForRow:newIndexPath.row+1 inSection:newIndexPath.section];
            } else {
                newIndexPathProcessed=newIndexPath;
            }
            [self.collectionView addChangeForObjectAtIndexPath:indexPathProcessed forChangeType:type newIndexPath:newIndexPathProcessed];
            
        } else {
            //MAKE VARIANT FOR INSERT
            //if indexPath==currentAddressInd
            //if new task is past ind=ind
            //in new task is future ind=ind+1
            
            if(indexPath.row>=[self findGradientBreakInd]){
                indexPathProcessed=[NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
            } else {
                indexPathProcessed=indexPath;
            }
            
            if(newIndexPath.row>[self findGradientBreakInd]){
                newIndexPathProcessed=[NSIndexPath indexPathForRow:newIndexPath.row+1 inSection:newIndexPath.section];
            } else if(newIndexPath.row==[self findGradientBreakInd]){
                TLUserTask *task=[controller objectAtIndexPath:newIndexPath];
                if(task.is_past.boolValue){
                    newIndexPathProcessed=newIndexPath;
                } else {
                    newIndexPathProcessed=[NSIndexPath indexPathForRow:newIndexPath.row+1 inSection:newIndexPath.section];
                }
            } else {
                newIndexPathProcessed=newIndexPath;
            }
            [self.collectionView addChangeForObjectAtIndexPath:newIndexPathProcessed forChangeType:type newIndexPath:newIndexPathProcessed];
        }

    } else {
        [self.collectionView addChangeForObjectAtIndexPath:indexPath forChangeType:type newIndexPath:newIndexPath];
    }*/
    
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    if(_isManualMoving){
        [self.collectionView clearChanges];
        _isManualMoving=NO;
    } else {
        self.currentAddressInd=[self findCurrentAddressInd];
//        [self.collectionView commitChanges];
        [self.collectionView clearChanges];
        [self.collectionView performBatchUpdates:^{
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
        } completion:nil];
    }
    
    if(self.hasCurrentAddressCell){
        NSInteger addrInd=self.currentAddressInd;
        [self.collectionView reloadItemsAtIndexPaths:@[
                                                       [NSIndexPath indexPathForRow:addrInd inSection:0]
                                                       ]];
    }

    gradientInd= [self findGradientBreakInd];
    [self setGradientToIndex:gradientInd];
    
}



#pragma mark - Actions

- (void) showTimelineAfterRegistration:(NSNotification*) note{
    [UIView animateWithDuration:0.15 delay:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.collectionView.alpha=1;
        self.navContainerView.alpha=1;
    } completion:nil];
    
}

- (void) dismissTap:(UITapGestureRecognizer*) gr{
 
    [self.dismissalView removeFromSuperview];
    self.dismissalView=nil;
    [self.view layoutIfNeeded];
    
    [self setEditing:NO];
}

- (void) handleCollectionLongTap:(UILongPressGestureRecognizer*)gesture{
    

    if (gesture.state==UIGestureRecognizerStateBegan){
        NSIndexPath *selectedIndexPath = [self.collectionView indexPathForItemAtPoint:[gesture locationInView:self.collectionView]];
        
        if(!selectedIndexPath)
            return;
        
        if(![self taskForIndex:selectedIndexPath]){
            return;
        }
        
        movingIndexPath=selectedIndexPath;
        
        [self setEditing:YES];
        _isManualMoving=YES;
        TLUserTask *task=[self taskForIndex:selectedIndexPath];
        if(task){
            if(!task.is_past.boolValue){
                [self.collectionView beginInteractiveMovementForItemAtIndexPath:selectedIndexPath];
            }
        }
        
        
        TLTimelineCollectionCell *pickedCell=[self pickedUpCell];
        movingCollectionCell=pickedCell;
        [pickedCell stopWiggling];
        [self animatePickingUpCell:pickedCell];
        UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:selectedIndexPath];
        
        CGRect cellRect = attributes.frame;
        CGRect rect=[self.collectionView convertRect:cellRect toView:self.collectionView];
//        rectstart=rect.origin.x/[UIScreen mainScreen].bounds.size.width;

        draggingBeginYCoord=rect.origin.y;
        
        //Add dismissal view
        if(!self.dismissalView){
            UIView *dismissalView=[[UIView alloc] initWithFrame:CGRectMake(0, -800, self.view.bounds.size.width, 800)];
            dismissalView.opaque=NO;
            dismissalView.backgroundColor=[UIColor colorWithWhite:0.2 alpha:0.3];
            UITapGestureRecognizer *tapGr=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissTap:)];
            [dismissalView addGestureRecognizer:tapGr];
            self.dismissalView=dismissalView;
            [self.view addSubview:dismissalView];
        }
        
    } else if (gesture.state==UIGestureRecognizerStateChanged) {
        
        TLUserTask *task=[self taskForIndex:[self.collectionView indexPathForCell:movingCollectionCell]];
        if(!task) return;
        if(!self.addedToMap){
            if([gesture locationInView:self.collectionView].y<=-50){
                
                [self.delegate addMapToTask:task];
                self.addedToMap=YES;
                self.removedFromMap=NO;
            }
        } else {
            
            if(!self.removedFromMap && [gesture locationInView:self.collectionView].y>-50 ){
                [self.delegate removeLastTask];
                self.removedFromMap=YES;
                self.addedToMap=NO;
            }
            
        }
        
        CGFloat xPoint=[gesture locationInView:self.collectionContainerView].x;
        
        CGFloat margin=25;
        
        NSLog(@"%f",xPoint);
        if(xPoint<margin){
            xPoint=margin;
        }
        if(xPoint>[UIScreen mainScreen].bounds.size.width-margin){
            xPoint=[UIScreen mainScreen].bounds.size.width-margin;
        }
        
        [self.collectionView updateInteractiveMovementTargetPosition:CGPointMake(xPoint+self.collectionView.contentOffset.x,39)];
        
    } else {
//        TLUserTask *task=[self taskForIndex:[self.collectionView indexPathForCell:movingCollectionCell]];
//        if(!task)return;
        if (gesture.state==UIGestureRecognizerStateEnded) {
            [self.collectionView endInteractiveMovement];
        } else {
            [self.collectionView cancelInteractiveMovement];
        }
        
        [self animatePuttingDownCell:movingCollectionCell];
        movingIndexPath=nil;
        movingCollectionCell=nil;
        
        self.isManualMoving=NO;
        self.addedToMap=NO;
//        [self setEditing:NO];
    }
    
    
}


- (IBAction)scrollToNextVisibleItem:(id)sender {
    
    NSArray<NSIndexPath*> *visiblePaths=[self.collectionView indexPathsForVisibleItems];
     if(visiblePaths.count==0) return;
    
    NSInteger maxIndRow=[visiblePaths firstObject].row;
    
    for(NSIndexPath *indPath in visiblePaths){
        if(indPath.row>maxIndRow){
            maxIndRow=indPath.row;
        }
    }
    if(maxIndRow>=[self.collectionView numberOfItemsInSection:0]-1){
        maxIndRow=[self.collectionView numberOfItemsInSection:0]-2;
    }
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:maxIndRow+1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
    
    
}

- (IBAction)scrollToPreviousVisibleItem:(id)sender {
    
    NSArray<NSIndexPath*> *visiblePaths=[self.collectionView indexPathsForVisibleItems];
    
    if(visiblePaths.count==0) return;
    
    NSInteger minIndRow=[visiblePaths firstObject].row;
    
    for(NSIndexPath *indPath in visiblePaths){
        if(indPath.row<minIndRow){
            minIndRow=indPath.row;
        }
    }
    if(minIndRow==0){
        minIndRow=1;
    }
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:minIndRow-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
    
}

#pragma mark - Other delegates 


- (void) pushDetailViewWithTask:(TLUserTask *)task animated:(BOOL) animated{
    [self.childDetailVC setUserTask:task];
    
    if(animated){
        [UIView animateWithDuration:0.4 animations:^{
            [self.childScrollView setContentOffset:CGPointMake(self.navContainerView.bounds.size.width, 0) animated:NO];
        }];
    } else {
        [self.childScrollView setContentOffset:CGPointMake(self.navContainerView.bounds.size.width, 0) animated:NO];
    }
//    [self.delegate switchToStateWithoutLayingOut:TLPanelPresentationStateDetail];
    self.contentState=TLPanelPresentationStateDetail;
    [self.delegate panelChangedContentStateTo:TLPanelPresentationStateDetail];
    
}

- (void) pushDetailViewWithTask:(TLUserTask *)task{
    [self pushDetailViewWithTask:task animated:YES];
}

- (void) popBack{
//    [self.delegate switchToState:TLPanelPresentationStateTable];
    self.contentState=TLPanelPresentationStateTable;
    [self.delegate panelChangedContentStateTo:TLPanelPresentationStateTable];
    
    [UIView animateWithDuration:0.4 animations:^{
        [self.childScrollView setContentOffset:CGPointMake(0, 0)];
    }];
    
}

- (void) showChangePlaceForTask:(TLUserTask *)task{
    [self.delegate showPlacesListForTask:task];
}

- (void) keyBoardWillShowWithHeight:(CGFloat)height{
    [self.delegate keyBoardWillShowWithHeight:height];
}

- (void) keyBoardWillBeHidden{
    [self.delegate keyBoardWillBeHidden];
}

- (void) setDetailPhoto:(NSOrderedSet<TLUserTaskPhoto*> *)photo{
    [self.delegate setDetailPhoto:photo];
}

//Cell tapped delete
- (void) didTappedDeleteAtCell:(TLTimelineCollectionCell *)cell{
    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Действительно удалить элемент?" message:@"Восстановление невозможно" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *confirmAction=[UIAlertAction actionWithTitle:@"Удалить" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        //DELETING TASK
        
        NSIndexPath *index=[self.collectionView indexPathForCell:cell];
        TLUserTask *userTask=[self taskForIndex:index];
        
        if([self.childDetailVC currentTask].id.integerValue==userTask.id.integerValue){
//            [self.delegate switchToState:TLPanelPresentationStateCompact];
            self.contentState=TLPanelPresentationStateCompact;
            [self.delegate panelChangedContentStateTo:TLPanelPresentationStateCompact];
        }
        
        [userTask deleteTaskWithCompletion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kTLTasksChangedInDB object:nil];
        }];
        

    }];
    
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:confirmAction];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - Helper methods

#pragma mark Collection Reordering

- (TLTimelineCollectionCell*) pickedUpCell{
    if(!movingIndexPath) return nil;
    
    return (TLTimelineCollectionCell*)[self.collectionView cellForItemAtIndexPath:movingIndexPath];
}

- (void) setEditing:(BOOL)editing{
    [super setEditing:editing];
    
    [self startWigglingAllVisibleCells];
}


- (void) animatePickingUpCell:(TLTimelineCollectionCell*)cell{
    
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
        cell.alpha=0.7;
//        cell.transform=CGAffineTransformMakeScale(1.3, 1.3);
        
        cell.dateLabel.alpha=0.0;
        cell.colorDot.alpha=0.0;
        cell.deleteCellButton.alpha=0.0;
    } completion:nil];
}

- (void) animatePuttingDownCell:(TLTimelineCollectionCell*) cell{
    [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState animations:^{
        cell.alpha=1.0;
        cell.colorDot.alpha=1.0;
        cell.dateLabel.alpha=1.0;
        if(self.editing){
            cell.deleteCellButton.alpha=1;
        }
        TLUserTask *task=[self taskForIndex:[self.collectionView indexPathForCell:cell]];
        cell.dateLabel.text= [self relativeDateStringFromDate:task.datetime];
//        cell.transform=CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        if(self.editing){
            TLUserTask *task=[self taskForIndex:[self.collectionView indexPathForCell:cell]];
            if(task){
                if(!task.is_past.boolValue){
                    [cell startWiggling];
                }
            }
            
        }
    }];
}

- (void) startWigglingAllVisibleCells{
    
    //only future task
    
    NSArray<TLTimelineCollectionCell*> *cells=[self.collectionView visibleCells];
    
    for(TLTimelineCollectionCell *cell in cells){
        if (self.editing) {
            TLUserTask *task=[self taskForIndex:[self.collectionView indexPathForCell:cell]];

            if(self.hasCurrentAddressCell){
                if(!([self.collectionView indexPathForCell:cell].row==[self currentAddressInd])){
                    
                    if(!task.is_past.boolValue){
                        [cell startWiggling];
                    }
                    [cell showDeleteButtonAnimated:YES];
                }
            } else {
                NSLog(@"%@ %@",task.desc,task.is_past.boolValue? @"past" : @"future");
                if(!task.is_past.boolValue){
                    [cell startWiggling];
                }
                [cell showDeleteButtonAnimated:YES];
            }
        } else {
            [cell stopWigglingAnimated];
            [cell hideDeleteButtonAnimated:YES];
//            [self.collectionView performBatchUpdates:^{
//                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
//            } completion:nil];
        }
    }
    
}


#pragma mark Other

- (void) refreshTimeline:(NSNotification*) note{
    [self.collectionView reloadData];
}

- (void) exitEditingMode{
    if(self.editing){
        [self.dismissalView removeFromSuperview];
        self.dismissalView=nil;
        [self.view layoutIfNeeded];
        
        [self setEditing:NO];
    }
}

- (void) changeToListView{
    [self.childScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
}



- (NSInteger) findGradientBreakInd {
    
    //Find first non-past cell
    NSPredicate *predicateNonPast = [NSPredicate predicateWithFormat:@"is_past = %@", @0];
    TLUserTask *firstNonPastTask=[TLUserTask MR_findFirstWithPredicate:predicateNonPast sortedBy:@"id" ascending:YES];
    
    //define if have past tasks
    BOOL havePast=NO;
    NSPredicate *predicatePast = [NSPredicate predicateWithFormat:@"is_past = %@",@1];
    TLUserTask *firstPastTask=[TLUserTask MR_findFirstWithPredicate:predicatePast];
    if (firstPastTask) { havePast=YES; }
    
    
    //define index
    if (firstNonPastTask){
        if(havePast){
            return firstNonPastTask.id.integerValue-1;
        } else {
            return -1;
        }
    } else {
        if(!self.hasCurrentAddressCell){
            return [[[self.fetchedTimeline sections] objectAtIndex:0] numberOfObjects] + 1;
        } else {
            return self.currentAddressInd;
        }
    }

}

- (void) setGradientToIndex:(NSInteger) index{
    
    CGFloat rectstart;
    
    
    if(index<0){
        rectstart=-0.5;
    } else {
        if(index>=[self.collectionView numberOfItemsInSection:0]){
            rectstart=1.5;
        } else {
            UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            
            if (attributes) {
                CGRect cellRect = attributes.frame;
                CGRect rect=[self.collectionView convertRect:cellRect toView:self.view];
                rectstart=rect.origin.x/[UIScreen mainScreen].bounds.size.width;
            }
        }
    }
    
    if(rectstart<-0.5)
        rectstart=-0.5;
    if (rectstart>1.5)
        rectstart=1.5;
    
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    self.gradientLine.startPoint=CGPointMake(rectstart-0.05, 0.5);
    self.gradientLine.endPoint=CGPointMake(rectstart+0.05, 0.5);
    
    [CATransaction commit];
    

}

- (NSString*) relativeDateStringFromDate:(NSDate*) date{
    
    NSCalendarUnit units = NSCalendarUnitDay;
    
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];

    NSInteger days=components.day;
    
    switch (days) {
        case 0:
            return @"Сегодня";
            break;
            
        case 1:
            return @"Вчера";
            break;
            
        default:
            return [cellDateFormatter stringFromDate:date];
            break;
    }

}


#pragma mark - <Flow layout delegate>

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat crossoverPercent=1-kCompactViewCollectionViewTopConstraintDif/kCompactViewHeightDif;
    
    if(extraCompactViewPercent>crossoverPercent){
        return CGSizeMake(58, 78-kCompactViewCellHeightDif);
    } else{
        return CGSizeMake(58, 78-kCompactViewHeightDif*extraCompactViewPercent);
    }
    
    
}

#pragma mark - Public methods

- (void) startInteractiveHiding{
    isInteractivelyHiding=YES;
    collectionViewXOffsetAtStartOfInteraction=self.collectionView.contentOffset.x;
}

- (void) finishInteractiveHidingAndHide:(BOOL)hide withDuration:(CGFloat)duration{
    isInteractivelyHiding=NO;
    
    //-If we're already in needed state dont trigger animations. Just return
    if(extraCompactViewPercent==(hide? 1 : 0)) return;
    
    [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [self setInteractiveHidingPercent:hide? 1 : 0];
    } completion:nil];
}

- (void) setInteractiveHidingPercent:(CGFloat)percent{
    
    if(percent==extraCompactViewPercent) return;
    
    extraCompactViewPercent=percent;
    
    CGFloat crossoverPercent=1-kCompactViewCollectionViewTopConstraintDif/kCompactViewHeightDif;
    
    if(percent<=crossoverPercent){
        self.collectionViewHeightConstraint.constant=78-kCompactViewHeightDif*percent;
        
        self.collectionViewTopConstraint.constant=29;
        
        self.leftArrowTopConstraint.constant=29-kCompactViewHeightDif*percent;
                
        self.rightArrowTopConstraint.constant=29-kCompactViewHeightDif*percent;
        
        self.leftArrowLeadingConstraint.constant=-22*percent/crossoverPercent*2; //multiply 2 for faster moving
        self.rightArrowTrailingConstraint.constant=-22*percent/crossoverPercent*2;
        
        self.leftArrowButton.alpha=fmax(1-percent/crossoverPercent*2,0);
        self.rightArrowButton.alpha=fmax(1-percent/crossoverPercent*2,0);
        
//        self.collectionViewLeadingConstraint.constant=26-26*percent/crossoverPercent;
//        self.collectionViewTrailingConstraint.constant=26-26*percent/crossoverPercent;
//        
//        CGFloat correctedOffset=collectionViewXOffsetAtStartOfInteraction-26*percent/crossoverPercent;
//        if(correctedOffset<0){
//            correctedOffset=0;
//        }
//        
////        if(correctedOffset>=self.collectionView.contentSize.width-self.collectionView.bounds.size.width-4){
////            correctedOffset=self.collectionView.contentSize.width-self.collectionView.bounds.size.width-4;
////        }
//        [self.collectionView setContentOffset:CGPointMake(correctedOffset, 0) animated:NO];
        
        
    } else {
        self.collectionViewHeightConstraint.constant=78-kCompactViewCellHeightDif;
        
        self.collectionViewTopConstraint.constant=29-(kCompactViewHeightDif*percent-kCompactViewCellHeightDif);
        
        self.leftArrowTopConstraint.constant=29-kCompactViewCellHeightDif;
        self.rightArrowTopConstraint.constant=29-kCompactViewCellHeightDif;
        
        self.leftArrowLeadingConstraint.constant=-22;
        self.rightArrowTrailingConstraint.constant=-22;
        
//        self.collectionViewLeadingConstraint.constant=0;
//        self.collectionViewTrailingConstraint.constant=0;
    }
    self.upperViewHeightConstraint.constant=120-kCompactViewHeightDif*percent;
    
    NSArray<UICollectionViewCell*> *visibleCells=[self.collectionView visibleCells];
    for(TLTimelineCollectionCell *cell in visibleCells){
        
        if(percent<crossoverPercent){
            
            //            CGFloat almostZero=0.0000000001;
            
            CGFloat containerHeight=cell.iconContainerView.bounds.size.height;
            
            cell.iconContainerView.transform=CGAffineTransformMakeTranslation(0, -containerHeight/2);
            cell.iconContainerView.transform=CGAffineTransformScale(cell.iconContainerView.transform,1-percent/crossoverPercent, 1-percent/crossoverPercent);
            cell.iconContainerView.transform=CGAffineTransformTranslate(cell.iconContainerView.transform,0, containerHeight/2);
            
            cell.iconContainerView.alpha=fmax(1-percent/crossoverPercent*1.5,0);
            
        } else{
            
            cell.iconContainerView.transform=CGAffineTransformMakeScale(0.0000001, 0.00001);
        }
        
    }
    
    [self.collectionView.collectionViewLayout invalidateLayout];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

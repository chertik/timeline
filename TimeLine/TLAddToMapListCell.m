//
//  TLAddToMapListCell.m
//  TimeLine
//
//  Created by Evgenii Oborin on 29.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLAddToMapListCell.h"
#import "TLThemeManager.h"

#define kImageAddToMapPlus [UIImage imageNamed:@"AddToMapPlus"]
#define kImageAddToMapOk [UIImage imageNamed:@"AddToMapOk"]


@implementation TLAddToMapListCell

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.layer.cornerRadius=8;
        
        CAGradientLayer *gradient=[CAGradientLayer layer];
        [gradient setFrame:self.contentView.bounds];
//        gradient.colors=@[(id) [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:1].CGColor,(id)[UIColor colorWithRed:223/255.0 green:116/255.0 blue:143/255.0 alpha:1].CGColor, (id) [UIColor colorWithRed:241/255.0 green:106/255.0 blue:141/255.0 alpha:1].CGColor];
        gradient.colors=@[(id) [TLThemeManager lighterGrayColor].CGColor, (id) [TLThemeManager mainAccentColor].CGColor];

        gradient.locations=@[@0,@1];
        gradient.startPoint=CGPointMake(0, 0);
        gradient.endPoint=CGPointMake(1, 0);
        
        CGFloat borderWidth=2;
        
        CALayer *solidLayer=[CALayer layer];
        solidLayer.backgroundColor=[UIColor whiteColor].CGColor;
        solidLayer.frame=CGRectMake(borderWidth, borderWidth, gradient.bounds.size.width-borderWidth*2, gradient.bounds.size.height-borderWidth*2);
        solidLayer.cornerRadius=6;
        
        [self.layer insertSublayer:solidLayer atIndex:0];
        [self.layer insertSublayer:gradient atIndex:0];
        self.addButton.userInteractionEnabled=YES;
        
    }
    return self;
}

- (IBAction)addButtonTapped:(id)sender {
    
    [self.delegate confirmTaskByCell:self];
    
    [self setButtonIsAdd:NO];
    self.addButton.userInteractionEnabled=NO;
}

- (void) setButtonIsAdd:(BOOL)isAdd{
    
    [self setButtonIsAdd:isAdd animated:YES];
}

- (void) setButtonIsAdd:(BOOL)isAdd animated:(BOOL)animated{
    
    [UIView animateWithDuration: animated ? 0.2 : 0.0 animations:^{
        [self.addButton setImage:isAdd ? kImageAddToMapPlus : kImageAddToMapOk forState:UIControlStateNormal];
        [self.addButton setTintColor:isAdd ? dThemeColorGray : dThemeColorRed];
        [self.addButton setUserInteractionEnabled: isAdd? YES : NO];
    }];
}


@end

//
//  TLPhotoMarker.h
//  TimeLine
//
//  Created by Evgenii Oborin on 02.11.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLPhotoMarker : UIView

- (void) setPhoto:(UIImage*)photo;

@end

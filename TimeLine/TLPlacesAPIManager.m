//
//  TLPlacesAPIManager.m
//  TimeLine
//
//  Created by Evgenii Oborin on 25.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLPlacesAPIManager.h"
//#import <RestKit/RestKit.h>

#import "TLCommonPlace.h"

@import RestKit;

@interface TLPlacesAPIManager ()

@property (strong,nonatomic) RKObjectManager *fsObjectManager;
@property (strong,nonatomic) RKObjectManager *gObjectManager;

@end


@implementation TLPlacesAPIManager


#pragma mark - initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureRestKit];
    }
    return self;
}


+ (instancetype) sharedManager{
    
    static TLPlacesAPIManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TLPlacesAPIManager alloc] init];
    });
    
    return sharedInstance;
    
}

#pragma mark setting up restkit


- (void) configureRestKit{
    
//    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    
    //Configuring foursquare kit
    NSURL *baseFSURL=[NSURL URLWithString:@"https://api.foursquare.com"];
    
    AFHTTPClient *fsClient=[[AFHTTPClient alloc] initWithBaseURL:baseFSURL];
    
    RKObjectManager *fsObjectManager=[[RKObjectManager alloc] initWithHTTPClient:fsClient];
    
    RKObjectMapping *venueMapping=[RKObjectMapping mappingForClass:[TLVenue class]];
    [venueMapping addAttributeMappingsFromDictionary:@{
                                                       @"name":@"name",
                                                       @"id":@"placeID",
                                                       @"location.lat":@"lat",
                                                       @"location.lng":@"lon",
                                                       @"location.formattedAddress":@"formattedAddress",
                                                       @"location.distance":@"distance",
                                                       @"rating":@"rating"
                                                       }];
    
    
    RKResponseDescriptor *venueResponseDescriptor=[RKResponseDescriptor responseDescriptorWithMapping:venueMapping
                                                                                          method:RKRequestMethodGET
                                                                                     pathPattern:@"/v2/venues/search"
                                                                                         keyPath:@"response.venues"
                                                                                     statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [fsObjectManager addResponseDescriptor:venueResponseDescriptor];
    self.fsObjectManager=fsObjectManager;
    
    
    //Configure google places web api kit
    NSURL *gBaseURL=[NSURL URLWithString:@"https://maps.googleapis.com"];
    
    AFHTTPClient *gClient=[[AFHTTPClient alloc] initWithBaseURL:gBaseURL];
    
    RKObjectManager *gObjectManager=[[RKObjectManager alloc] initWithHTTPClient:gClient];
    
    RKObjectMapping *gPlaceMapping=[RKObjectMapping mappingForClass:[TLGPlace class]];
    [gPlaceMapping addAttributeMappingsFromArray:@[@"name"]];
    [gPlaceMapping addAttributeMappingsFromDictionary:@{
                                                        @"place_id":@"placeID",
                                                        @"geometry.location.lat":@"lat",
                                              @"geometry.location.lng":@"lon"
                                                        }];
    
    RKResponseDescriptor *gResponseDescriptor=[RKResponseDescriptor responseDescriptorWithMapping:gPlaceMapping
                                                                                          method:RKRequestMethodGET
                                                                                     pathPattern:@"/maps/api/place/nearbysearch/json"
                                                                                         keyPath:@"results"
                                                                                     statusCodes:[NSIndexSet indexSetWithIndex:200]];
    
    [gObjectManager addResponseDescriptor:gResponseDescriptor];
    
    self.gObjectManager=gObjectManager;
    
    //Configure mappings and response for Google place details
    RKObjectMapping *gPlaceAdressComponentTypeMapping=[RKObjectMapping mappingForClass:[TLGPlaceAdressComponentType class]];
    [gPlaceAdressComponentTypeMapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"componentType"]];
    
    RKObjectMapping *gPlaceAdressComponentMapping=[RKObjectMapping mappingForClass:[TLGPlaceAdressComponent class]];
    [gPlaceAdressComponentMapping addAttributeMappingsFromDictionary:@{
                                                                       @"long_name":@"longName",
                                                                       @"short_name":@"shortName"
                                                            }];
    [gPlaceAdressComponentMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"types" toKeyPath:@"types" withMapping:gPlaceAdressComponentTypeMapping]];
    
    RKObjectMapping *gPlaceDetailsMapping=[RKObjectMapping mappingForClass:[TLGPlaceDetails class]];
    [gPlaceDetailsMapping addAttributeMappingsFromDictionary:@{
                                                               @"rating":@"rating",
                                                               @"formatted_address":@"formattedAddress"
                                                               }];
    [gPlaceDetailsMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"address_components" toKeyPath:@"addressComponents" withMapping:gPlaceAdressComponentMapping]];
    
    RKResponseDescriptor *gPlaceDetailsResponseDescriptor=[RKResponseDescriptor responseDescriptorWithMapping:gPlaceDetailsMapping
                                                                                               method:RKRequestMethodGET
                                                                                          pathPattern:@"/maps/api/place/details/json"
                                                                                              keyPath:@"result" statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [gObjectManager addResponseDescriptor:gPlaceDetailsResponseDescriptor];
    
    //Configure mappings and response for Google geocoding
    RKObjectMapping *gPlaceAdressComponentTypeMappingForGC=[RKObjectMapping mappingForClass:[TLGPlaceAdressComponentType class]];
    [gPlaceAdressComponentTypeMappingForGC addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"componentType"]];
    
    RKObjectMapping *gPlaceAdressComponentMappingGC=[RKObjectMapping mappingForClass:[TLGPlaceAdressComponent class]];
    [gPlaceAdressComponentMappingGC addAttributeMappingsFromDictionary:@{
                                                                       @"long_name":@"longName",
                                                                       @"short_name":@"shortName"
                                                                       }];
    [gPlaceAdressComponentMappingGC addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"types" toKeyPath:@"types" withMapping:gPlaceAdressComponentTypeMappingForGC]];
    
    RKObjectMapping *gPlaceDetailsMappingGC=[RKObjectMapping mappingForClass:[TLGPlaceDetails class]];
    [gPlaceDetailsMappingGC addAttributeMappingsFromDictionary:@{
                                                               @"formatted_address":@"formattedAddress"
                                                               }];
    [gPlaceDetailsMappingGC addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"address_components" toKeyPath:@"addressComponents" withMapping:gPlaceAdressComponentMappingGC]];
    
    RKResponseDescriptor *gPlaceDetailsResponseDescriptorGC=[RKResponseDescriptor responseDescriptorWithMapping:gPlaceDetailsMapping
                                                                                                       method:RKRequestMethodGET
                                                                                                  pathPattern:@"/maps/api/geocode/json"
                                                                                                      keyPath:@"results" statusCodes:[NSIndexSet indexSetWithIndex:200]];
    [gObjectManager addResponseDescriptor:gPlaceDetailsResponseDescriptorGC];
    
    
}

- (void) getPlacesForCoordinates:(CLLocationCoordinate2D) coord withAPI:(TLPlacesAPIType)type withCompletion:(void (^)(NSArray<TLCommonPlace*> *places))completionBlock failure:(void(^)(NSError *error))failureBlock{
    
    switch (type) {
        case TLPlacesAPITypeGoogle:
        {
            
            [self getGooglePlacesForCoordinates:coord withCompletion:^(NSArray<TLGPlace *> *places) {
                
                NSMutableArray<TLCommonPlace*> *commonPlacesArray = [[NSMutableArray alloc] init];
                
                for(TLGPlace *place in places){
                    TLCommonPlace *cp=[TLCommonPlace commonPlaceWithGooglePlace:place];
                    [commonPlacesArray addObject:cp];
                }
                
                completionBlock(commonPlacesArray);
                
            } failure:^(NSError *error) {
                failureBlock(error);
            }];
            
            break;
        }
    
        case TLPlacesAPITypeFoursquare:
        {
            [self getFoursquareVenuesForCoordinate:coord withCompletion:^(NSArray<TLVenue *> *venues) {
                
                NSMutableArray<TLCommonPlace*> *commonPlacesArray = [[NSMutableArray alloc] init];
                
                for(TLVenue *venue in venues){
                    TLCommonPlace *cp=[TLCommonPlace commonPlaceWithFoursquarePlace:venue];
                    [commonPlacesArray addObject:cp];
                }
                
                completionBlock(commonPlacesArray);
                
            } failure:^(NSError *error) {
                failureBlock(error);
            }];
            break;
        }
        default:
            
            [NSException raise:@"Wrong APIType" format:@"your APIType isn't one of the accepted"];
            
            break;
    }
    
}

- (void) getFoursquareVenuesForCoordinate:(CLLocationCoordinate2D) coord withCompletion:(void (^)(NSArray<TLVenue*> *venues))completionBlock failure:(void(^)(NSError *error))failureBlock{
    
    NSString *latLon = [NSString stringWithFormat:@"%f,%f",coord.latitude,coord.longitude];
    
    NSString *clientID = kFS_CLIENTID;
    NSString *clientSecret = kFS_CLIENTSECRET;
    
    NSDictionary *queryParams = @{@"ll" : latLon,
                                  @"client_id" : clientID,
                                  @"client_secret" : clientSecret,
                                  @"v" : @"20140806",
                                  @"m" : @"foursquare"
                                  };
    
    
    [self.fsObjectManager getObjectsAtPath:@"/v2/venues/search"
                                           parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                                  NSArray<TLVenue*> *venues = mappingResult.array;
                                                  completionBlock(venues);
                                              }
                                              failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                                  failureBlock(error);
                                              }];
    
    
    
}


- (void) getGooglePlacesForCoordinates:(CLLocationCoordinate2D) coord withCompletion:(void (^)(NSArray<TLGPlace*> *places))completionBlock failure:(void(^)(NSError *error))failureBlock{
    
    NSString *latLon = [NSString stringWithFormat:@"%f,%f",coord.latitude,coord.longitude];
    NSString *apiKey=kGooglePlacesWebKey;
    
    NSDictionary *queryParams=@{
                                @"location" : latLon,
                                @"key":apiKey,
                                @"radius":@150,
                                @"language":@"ru",
                                @"types":@"establishment"
                                };
    
    CLLocation *sourceLocation=[[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
    
    [self.gObjectManager getObjectsAtPath:@"/maps/api/place/nearbysearch/json"
                                           parameters:queryParams
                                              success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
 
                                                  NSArray<TLGPlace*> *places=mappingResult.array;
                                                  for(TLGPlace *place in places){
                                                      place.distance=@([sourceLocation distanceFromLocation:place.location]);
                                                  }
                                                  
                                                  [self getPlacesDetailsForArray:places
                                                                  withCompletion:^{
                                                                      completionBlock(places);
                                                                  } failure:failureBlock];
                                              }
                                  failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                      failureBlock(error);
                                  }];
    
    
}

- (void) getPlaceDetailsForPlaceWithID:(NSString*) placeId withCompletion:(void (^)(TLGPlaceDetails *detailObj))completionBlock failure:(void(^)(NSError *error))failureBlock{
    
    NSString *apiKey=kGooglePlacesWebKey;
    NSDictionary *queryParams=@{
                                @"placeid":placeId,
                                @"key":apiKey,
                                @"language":@"ru"
                                };
    [self.gObjectManager getObjectsAtPath:@"/maps/api/place/details/json"
                               parameters:queryParams
                                  success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                      TLGPlaceDetails *details=mappingResult.firstObject;
                                      completionBlock(details);
                                  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                      failureBlock(error);
                                  }];
}

- (void) getPlacesDetailsForArray:(NSArray<TLGPlace*>*) places withCompletion:(void(^)(void))completionBlock failure:(void(^)(NSError *error))failureBlock{
    
    if(!places){
        if(completionBlock){
            completionBlock();
            return;
        }
    }
    
    if(places.count==0){
        if(completionBlock){
            completionBlock();
            return;
        }
    }
    
    __block NSArray<TLGPlace*> *placesArr=places;
    __block int i=0;
    
    __block void (^completionHandler)(TLGPlaceDetails *detailObj)=NULL;
    completionHandler=[^(TLGPlaceDetails *detailObj){
        
        placesArr[i].details=detailObj;
        
        if(i<placesArr.count-1){
            i++;
            
            [self getPlaceDetailsForPlaceWithID:placesArr[i].placeID
                                 withCompletion:completionHandler
                                        failure:failureBlock];
            
            
        } else {
            completionBlock();
        }
        
    } copy];
    
    [self getPlaceDetailsForPlaceWithID:placesArr[i].placeID
                         withCompletion:completionHandler
                                failure:failureBlock];
    
}

- (void) getGoogleAddressForLocation:(CLLocation*)location withCompletion:(void (^)(NSString *address))completionBlock failure:(void(^)(NSError *error))failureBlock{
    
    NSString *apiKey=kGooglePlacesWebKey;
    NSString *latlng=[NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude];
    NSDictionary *queryParams=@{
                                @"latlng":latlng,
                                @"key":apiKey,
                                @"language":@"ru"
                                };
    [self.gObjectManager getObjectsAtPath:@"/maps/api/geocode/json"
                               parameters:queryParams
                                  success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                      TLGPlaceDetails *details=mappingResult.firstObject;
                                      NSString *addressString=[details shortAddress];
                                      NSLog(@"%@", addressString);
                                      if(completionBlock)
                                          completionBlock(addressString);
                                  } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                      if(failureBlock)
                                          failureBlock(error);
                                  }];

}




@end

//
//  TLDelayedCheckin+CoreDataProperties.m
//  TimeLine
//
//  Created by Evgenii Oborin on 13.03.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLDelayedCheckin+CoreDataProperties.h"

@implementation TLDelayedCheckin (CoreDataProperties)

+ (NSFetchRequest<TLDelayedCheckin *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TLDelayedCheckin"];
}

@dynamic isDefined;
@dynamic latitude;
@dynamic longitude;
@dynamic orderValue;
@dynamic time;
@dynamic placesList;
@dynamic route;

@end

//
//  TLGeoManager.m
//  TimeLine
//
//  Created by Evgenii Oborin on 28.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLGeoManager.h"
#import "TLUserTask.h"
#import "TLPlaceData+CoreDataClass.h"
#import "TLLocation+CoreDataClass.h"
#import "TLDelayedCheckin.h"
#import "TLPlacesAPIManager.h"
#import "TLCommonPlace.h"

#define kUserDefaultsCurrentRoute @"kUserDefaultsCurrentRoute"
#define kUserDefaultsIsRouteTrackingEnabled @"kUserDefaultsIsRouteTrackingEnabled"
#define kUserDefaultsIsCurrentRouteTrackingCancelled @"kUserDefaultsIsCurrentRouteTrackingCancelled"

@interface TLGeoManager () <CLLocationManagerDelegate>

@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) CLLocation *lastLocation;
@property (strong,nonatomic) CLLocation *lastKnownValidLocation;
@property (strong,nonatomic) NSMutableArray<CLLocation*>* currentRoute;

@property (nonatomic) BOOL isRouteTrackingEnabled;
@property (nonatomic) BOOL isCurrentRouteTrackingCancelled;


@end


@implementation TLGeoManager
{
    NSDate *adressUpdateLastDate;
    BOOL firstLocationUpdate;
    BOOL isDeferringUpdates;
}


#pragma mark - setup

- (instancetype)init
{
    self = [super init];
    if (self) {
        //configuration
        self.currentAddress=nil;
        firstLocationUpdate=YES;
        
        _isRouteTrackingEnabled=[[NSUserDefaults standardUserDefaults] boolForKey:kSettingsRouteTrackingEnabled];
        _isCurrentRouteTrackingCancelled=[[NSUserDefaults standardUserDefaults] boolForKey:kUserDefaultsIsCurrentRouteTrackingCancelled];
        
       
        [self setupLocationManager];
//        [self setMonitoringForActiveTasks];
    }
    return self;
}


+ (instancetype) sharedManager{
    
    static TLGeoManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TLGeoManager alloc] init];
    });
    
    return sharedInstance;
    
}

- (void) setupLocationManager{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate=self;
    _locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    _locationManager.distanceFilter=kCLDistanceFilterNone;
    _locationManager.pausesLocationUpdatesAutomatically=NO;
    
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        [_locationManager requestAlwaysAuthorization];
    
    if([CLLocationManager locationServicesEnabled]){
        [_locationManager startUpdatingLocation];
        
        if(_isRouteTrackingEnabled){
            [self privateSetupForBackgroundTracking];
        }
        
        [_locationManager startMonitoringVisits];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppWillTerminateNotifiaction:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppDidRealaunched:) name:UIApplicationDidFinishLaunchingNotification object:nil];
    
}

- (void) stopUpdatingLocations{
    [_locationManager stopUpdatingLocation];
}

- (void) startUpdatingLocations{
    [_locationManager startUpdatingLocation];
}

- (void) enableRouteTracking{
    if(!self.isRouteTrackingEnabled){
        
        self.isRouteTrackingEnabled=YES;
        [self privateSetupForBackgroundTracking];
        
        [self saveCurrentRouteAndSettings];
    }
    
}

- (void) stopRouteTracking{
    if(self.isRouteTrackingEnabled){
        self.isRouteTrackingEnabled=NO;
        _locationManager.allowsBackgroundLocationUpdates=NO;
//        [self cancelCurrentRouteTracking];
        [_locationManager disallowDeferredLocationUpdates];
        [self saveCurrentRouteAndSettings];
    }
}



// ----------------------------------
// ------------CHECK IT-------------

#warning region monitoring should be tested and needs functionality to monitor more than 10 regions

- (void) setMonitoringForActiveTasks{
    NSArray *array = [TLUserTask MR_findAllSortedBy:@"id" ascending:YES];
    for(TLUserTask *userTask in array){
        if(!userTask.is_past.boolValue){
            BOOL active=NO;
            for(CLCircularRegion *reg in _locationManager.monitoredRegions){
                if(reg.identifier==[NSString stringWithFormat:@"%ld",userTask.id.integerValue]){
                    active=YES;
                }
            }
            if(!active){
                CLCircularRegion *regionToMonitor=[self regionFromTask:userTask];
                [_locationManager startMonitoringForRegion:regionToMonitor];
            }
        }
    }
}

// ----------------------------------

#pragma mark - Private methods

- (void) privateSetupForBackgroundTracking{
    if(!self.isCurrentRouteTrackingCancelled){
        [_locationManager setAllowsBackgroundLocationUpdates:YES];
//        NSLog(@"%@",[CLLocationManager deferredLocationUpdatesAvailable] ? @"YES" : @"NO");
//        isDeferringUpdates=YES;
//        [_locationManager allowDeferredLocationUpdatesUntilTraveled:500 timeout:2*60];
    } else {
        [_locationManager setAllowsBackgroundLocationUpdates:NO];
        [_locationManager disallowDeferredLocationUpdates];
    }
}

- (CLCircularRegion*) regionFromTask:(TLUserTask*) task{
    
    CLLocationCoordinate2D location=CLLocationCoordinate2DMake(task.latitude.floatValue, task.longitude.floatValue);
    CLLocationDistance radius=100; //100 meters
    
    CLCircularRegion* region=[[CLCircularRegion alloc] initWithCenter:location radius:radius identifier:[NSString stringWithFormat:@"%ld",task.id.integerValue]];
    
    region.notifyOnEntry=YES;
    
    return region;
}

- (BOOL) isLocationTrackingNotWorking{
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        return YES;
    } else return NO;
}

- (void) addLocationsToCurrentRoute:(NSArray<CLLocation*>*)locatons{
    // Step1: Get current route
    NSMutableArray *route=[self getCurrentRouteOrInitialize];
    
    // Step 2: add locations
    [route addObjectsFromArray:locatons];
    
//    NSLog(@"added locations count %ld",locatons.count);
}

#pragma mark Utilites

- (void) postLoggingLocalNotificationWithText:(NSString*) text{
    
    BOOL loggingEnabled=[[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsLoggingEnabled] boolValue];
    if(!loggingEnabled) return;
    
    UILocalNotification *notification=[[UILocalNotification alloc] init];
    notification.alertBody=text;
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

- (NSMutableArray<CLLocation*>*) getCurrentRouteOrInitialize{
    NSMutableArray *route=self.currentRoute;
    if(!route){
        NSArray *restoredArray=[self getCurrentRouteFromDisk];
        if(!restoredArray){
            route=[NSMutableArray array];
            self.currentRoute=route;
//            NSLog(@"created new %@",route);
        } else{
            route=[restoredArray mutableCopy];
            self.currentRoute=route;
//            NSLog(@"loaded from nsuserdef %@",route);
        }
    } else {
//        NSLog(@"is in memory %@",route);
    }
    return route;
}

- (NSArray<CLLocation*>*) getCurrentRouteFromDisk{
    NSData *archivedArray=[[NSUserDefaults standardUserDefaults] objectForKey:kUserDefaultsCurrentRoute];
    NSArray *array=[NSKeyedUnarchiver unarchiveObjectWithData:archivedArray];
    return array;
}

#pragma  mark Notification Handlers

- (void) handleAppWillTerminateNotifiaction:(NSNotification*)note{
    
    BOOL hackIsEnabled=[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsAppWakingHackIsEnabled] boolValue];
    if(!hackIsEnabled) return;
    if(self.isRouteTrackingEnabled){
        [self.locationManager stopUpdatingLocation];
        [self.locationManager startMonitoringSignificantLocationChanges];
        
        UILocalNotification *notification=[[UILocalNotification alloc] init];
        notification.alertAction=@"App was killed";
        notification.alertBody=@"App was killed by system or user, but thanks to hack will be launched ASAP";
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}

- (void) handleAppDidRealaunched:(NSNotification*) note{
    
    BOOL hackIsEnabled=[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsAppWakingHackIsEnabled] boolValue];
    if(!hackIsEnabled) return;
    
    [self.locationManager stopMonitoringSignificantLocationChanges];
    if(_isRouteTrackingEnabled){
        [self privateSetupForBackgroundTracking];
        
        UILocalNotification *notification=[[UILocalNotification alloc] init];
        notification.alertAction=@"App is alive";
        notification.alertBody=@"App was relaunched by significantLocationUpdates service and is tracking route again";
        notification.fireDate=[NSDate dateWithTimeIntervalSinceNow:1];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
    }
}

#pragma mark - Public methods

- (void) saveCurrentRouteAndSettings{

    NSData *archivedRoute=[NSKeyedArchiver archivedDataWithRootObject:self.currentRoute];
    
    [[NSUserDefaults standardUserDefaults] setObject:archivedRoute forKey:kUserDefaultsCurrentRoute];
    [[NSUserDefaults standardUserDefaults] setBool:self.isRouteTrackingEnabled forKey:kSettingsRouteTrackingEnabled];
    [[NSUserDefaults standardUserDefaults] setBool:self.isCurrentRouteTrackingCancelled forKey:kUserDefaultsIsCurrentRouteTrackingCancelled];
}

- (void) cancelCurrentRouteTracking{
    self.isCurrentRouteTrackingCancelled=YES;
    self.currentRoute=nil;
    
    [self privateSetupForBackgroundTracking];
    [self saveCurrentRouteAndSettings];
}

- (NSArray<CLLocation *> *)getRoute{
    return self.currentRoute;
}

- (CLLocation*) getCurrentLocation{
    return _lastLocation;
}

- (void) makeCheckinWithUserTask:(TLUserTask *)userTask withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock{
    
    CLLocation *location=[[CLLocation alloc] initWithLatitude:userTask.locationCoordinate.latitude longitude:userTask.locationCoordinate.longitude];    
    [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:userTask completionBlock:completionBlock failureBlock:failureBlock withDate:0];
}

- (void) makeCheckinWithLocation:(CLLocation *)location withAPI:(TLPlacesAPIType)placesAPIType withDate:(int)checkinDate{
    [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:nil completionBlock:nil failureBlock:nil withDate:checkinDate];
}

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock withDate:(int)checkinDate{
    [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:nil completionBlock:completionBlock failureBlock:failureBlock withDate:checkinDate];
    
}

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock withDate:(int)checkinDate photo:(UIImage*)photoImage {
    [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:nil completionBlock:completionBlock failureBlock:failureBlock isCurrentAddress:NO withDate:checkinDate photo:photoImage];
}

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock isCurrentAdress:(BOOL) isCurrentAdress withDate:(int)checkinDate{
    [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:nil completionBlock:completionBlock failureBlock:failureBlock isCurrentAddress:isCurrentAdress withDate:checkinDate photo: nil];
    
}

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType withTask:(TLUserTask*)userTask completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock withDate:(int)checkinDate{
    [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:userTask completionBlock:completionBlock failureBlock:failureBlock isCurrentAddress:NO withDate:checkinDate photo: nil];
}

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock isCurrentAdress:(BOOL) isCurrentAdress withDate:(int)checkinDate photo:(UIImage*) photoImage {
    [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:nil completionBlock:completionBlock failureBlock:failureBlock isCurrentAddress:isCurrentAdress withDate:checkinDate photo:photoImage];
}

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType withTask:(TLUserTask*)userTask completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock isCurrentAddress:(BOOL)isCurrentAddress withDate:(int)checkinDate photo:(UIImage*) photoImage{
    
    [[TLPlacesAPIManager sharedManager] getPlacesForCoordinates:location.coordinate withAPI:placesAPIType withCompletion:^(NSArray<TLCommonPlace *> *places) {
        if(places.count!=0){
            if([UIApplication sharedApplication].applicationState==UIApplicationStateActive){
                
                //FOR RUNNING APPLICATION JUST ADD CHECKIN
                [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                
                    NSMutableOrderedSet *placesSet=[[NSMutableOrderedSet alloc] init];
                    
                    for(TLCommonPlace *place in places){
                        TLPlaceData *placeData=[TLPlaceData createPlaceDataWithCommonPlace:place inContext:localContext];
                        [placesSet addObject:placeData];
                    }
                    
                    NSDate *currentDate;
                    if (checkinDate != 0) {
                        currentDate = [NSDate dateWithTimeIntervalSince1970:checkinDate];
                    } else { currentDate = [NSDate date]; }
                
                    TLUserTask *newVisit;
                    
                    if(userTask){
                        newVisit=[TLUserTask userTaskWithId:userTask.id inContext:localContext];
                        
                        newVisit.is_checked=@1;
                        newVisit.placesList=placesSet;
                        newVisit.is_timeline=@1;
                        
                        TLPlaceData *firstData=[placesSet firstObject];
                        newVisit.selected_place_id=firstData.id;
                        newVisit.address=firstData.address;
                        
                        if(!newVisit.is_past.boolValue){

                            [newVisit moveTaskToRecentPastInContext:localContext];
                            newVisit.datetime = currentDate;
                        }
                        
                    } else{
                        newVisit=[TLUserTask MR_createEntityInContext:localContext];
                        
                        newVisit.latitude=@(location.coordinate.latitude);
                        newVisit.longitude=@(location.coordinate.longitude);
                        newVisit.desc=places.firstObject.name;
                        newVisit.icon_path=@"cafe";
                        
                        newVisit.is_visible=@0;
                        if(isCurrentAddress){
                            newVisit.is_checked=@1;
                            newVisit.is_autocheckin=@0;
                        } else {
                            newVisit.is_autocheckin=@1;
                        }
                        newVisit.datetime = currentDate;
                        newVisit.is_timeline=@1;
                        
                        
                        TLPlaceData *firstData=[placesSet firstObject];
                        newVisit.selected_place_id=firstData.id;
                        newVisit.address=firstData.address;
                        newVisit.placesList=placesSet;
                        //NSLog(@"%ld", newVisit.id.integerValue - 1);
//                        [newVisit moveTaskToPastByTimeWithMaxInd:newVisit.id.integerValue - 1];
                        [newVisit moveTaskToPastByTimeWithMaxInd:newVisit.id.integerValue - 1 inContext:localContext];
                        //[newVisit moveTaskToRecentPastInContext:localContext];
                        
                        if (photoImage != nil ) {
                            TLUserTaskPhoto *photo=[TLUserTaskPhoto MR_createEntityInContext:localContext];
                            photo.image=photoImage;
                            NSOrderedSet *set = [[NSOrderedSet alloc] initWithArray:@[photo]];
                            newVisit.taskPhoto=set;
                        }
                        
                        [self addCurrentRouteIfPossibleLeadingToTask:newVisit inContext:localContext];
                    }
                    
                } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:kGeoManagerDidSuccessfullyChecked object:nil];
                    if(completionBlock){
                        completionBlock();
                    }
                    UILocalNotification *notification=[[UILocalNotification alloc] init];
                    notification.alertBody=[NSString stringWithFormat:@"Вы посетили %@",places.firstObject.name];
                    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
                    
                }];
                
            } else {
                
                if(failureBlock){
                    failureBlock();
                }
               
                if(userTask || isCurrentAddress) {
                  return;
                }
                //APPLICATION IS IN BACKGROUND
                
                [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
                    
                    TLDelayedCheckin *delayed=[TLDelayedCheckin MR_createEntityInContext:localContext];
                    delayed.time=[NSDate date];
                    delayed.latitude=@(location.coordinate.latitude);
                    delayed.longitude=@(location.coordinate.longitude);
                    
                    delayed.isDefined=@1;
                    
                    
                    NSMutableOrderedSet *placesSet=[[NSMutableOrderedSet alloc] init];
                    
                    for(TLCommonPlace *place in places){
                        TLPlaceData *placeData=[TLPlaceData createPlaceDataWithCommonPlace:place inContext:localContext];
                        [placesSet addObject:placeData];
                        
                    }
                    
                    delayed.placesList=placesSet;
                    [self addCurrentRouteIfPossibleLeadingToDelayedTask:delayed inContext:localContext];
                    
                }];
                
                UILocalNotification *notification=[[UILocalNotification alloc] init];
                notification.alertBody=[NSString stringWithFormat:@"Похоже, что вы посетили %@. Проверьте в приложении! ;)",places.firstObject.name];
                [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
            }
        } else {
            //places count ==0;
            NSLog(@"0 places don't add checkin");
            
            
            return;
        }
    } failure:^(NSError *error) {
        
        if(failureBlock){
            failureBlock();
        }
        if(userTask || isCurrentAddress){
            return;
        }
        
        // ERROR (network or other error)
        UILocalNotification *notification=[[UILocalNotification alloc] init];
        notification.alertBody=[NSString stringWithFormat:@"Посещено новое место, но название заведения не было загружено. Загрузка будет отложена до появления интернета"];
        
        NSLog(@"----------\n\n internet error making delayed checkin");
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        
        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
            
            TLDelayedCheckin *delayed=[TLDelayedCheckin MR_createEntityInContext:localContext];
            delayed.time=[NSDate date];
            delayed.latitude=@(location.coordinate.latitude);
            delayed.longitude=@(location.coordinate.longitude);
            
            [self addCurrentRouteIfPossibleLeadingToDelayedTask:delayed inContext:localContext];
            
        }];
    }];
    
}

- (void) addCurrentRouteIfPossibleLeadingToTask:(TLUserTask*)userTask inContext:(NSManagedObjectContext*) context{
    if(self.isRouteTrackingEnabled){
        if(!self.isCurrentRouteTrackingCancelled){
            TLUserTask *previousVisit=[TLUserTask userTaskWithId:@(userTask.id.intValue-1) inContext:context];
            NSMutableArray<CLLocation*> *route=[self getCurrentRouteOrInitialize];
            if(previousVisit && route){
                NSMutableOrderedSet *locationsSet=[[NSMutableOrderedSet alloc]init];
                for(CLLocation *location in route){
                    TLLocation *cdLocation=[TLLocation MR_createEntityInContext:context];
                    cdLocation.horizontalAccuracy=@(location.horizontalAccuracy);
                    cdLocation.latitude=@(location.coordinate.latitude);
                    cdLocation.longitude=@(location.coordinate.longitude);
                    cdLocation.timestamp=location.timestamp;
                    
                    [locationsSet addObject:cdLocation];
                }
                previousVisit.routeToNextTask=locationsSet;
            }
            self.currentRoute=nil;
            [self saveCurrentRouteAndSettings];
        } else {
            self.isCurrentRouteTrackingCancelled=NO;
            [self privateSetupForBackgroundTracking];
        }
    }
}

- (void) addCurrentRouteIfPossibleLeadingToDelayedTask:(TLDelayedCheckin *)userTask inContext:(NSManagedObjectContext *)context{
    if(self.isRouteTrackingEnabled){
        if(!self.isCurrentRouteTrackingCancelled){
            NSMutableArray<CLLocation*> *route=[self getCurrentRouteOrInitialize];
            if(route){
                NSMutableOrderedSet *locationsSet=[[NSMutableOrderedSet alloc]init];
                for(CLLocation *location in route){
                    TLLocation *cdLocation=[TLLocation MR_createEntityInContext:context];
                    cdLocation.latitude=@(location.coordinate.latitude);
                    cdLocation.longitude=@(location.coordinate.longitude);
                    cdLocation.timestamp=location.timestamp;
                    
                    [locationsSet addObject:cdLocation];
                }
                userTask.route=locationsSet;
            }
            self.currentRoute=nil;
            [self saveCurrentRouteAndSettings];
        } else {
            self.isCurrentRouteTrackingCancelled=NO;
            [self privateSetupForBackgroundTracking];
        }
    }
}

- (void) getAdressesForAllTasks{
    NSPredicate *predicate=[NSPredicate predicateWithFormat:@"address = nil"];
    __block NSArray<TLUserTask*> *tasks=[TLUserTask MR_findAllSortedBy:@"id" ascending:YES withPredicate:predicate];
    if(tasks.count==0) return;
    
    __block NSMutableArray *addresses=[[NSMutableArray alloc] init];
    
    __block int i=0;
    
    __block void (^completionHandler)(NSString *address)=NULL;
    completionHandler=[^(NSString *address){
        
        [addresses addObject:address];
        
        if(i<tasks.count-1){
            i++;
            
            [[TLPlacesAPIManager sharedManager] getGoogleAddressForLocation:[[CLLocation alloc] initWithLatitude:tasks[i].locationCoordinate.latitude longitude:tasks[i].locationCoordinate.longitude] withCompletion:completionHandler failure:nil];
        } else{
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                for(int k=0;k<tasks.count;k++){
                    TLUserTask *taskInCont=[TLUserTask userTaskWithId:tasks[k].id inContext:localContext];
                    taskInCont.address=addresses[k];
                }
            }];
        }
        
    }copy];
    
    [[TLPlacesAPIManager sharedManager] getGoogleAddressForLocation:[[CLLocation alloc] initWithLatitude:tasks[i].locationCoordinate.latitude longitude:tasks[i].locationCoordinate.longitude] withCompletion:completionHandler failure:nil];
}

#pragma mark - CLLocationManager's Delegate


#pragma mark delegates methods

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
//    NSLog(@"location update");

    // Step 1: Process last location to identify adress
    CLLocation* location = [locations lastObject];
    
    NSDate* eventDate = location.timestamp;
    BOOL shouldUpdateAdress=NO;
    self.lastLocation=location;
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kGeoManagerUpdatedCurrentLocation object:nil];
    
    if(firstLocationUpdate){ //delay before sending adress request for testing
        firstLocationUpdate=NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:kGeoManagerUpdatedLocationFirstTimeForCurrentLaunch object:nil];
    }

    
    if(!adressUpdateLastDate){
        shouldUpdateAdress=YES;
    } else {
        NSTimeInterval timeSinceLastUpdate=[eventDate timeIntervalSinceDate:adressUpdateLastDate];
        if(timeSinceLastUpdate>20.0 && [UIApplication sharedApplication].applicationState==UIApplicationStateActive){
            shouldUpdateAdress=YES;
        }
    }
    
    if(shouldUpdateAdress){
        self.lastLocation=location;
        [[TLPlacesAPIManager sharedManager] getGoogleAddressForLocation:location withCompletion:^(NSString *address) {
            adressUpdateLastDate=[NSDate date];
            self.currentAddress=address;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kGeoManagerUpdatedCurrentAddress object:nil];
            
            NSLog(@"Current address:%@",self.currentAddress);
            
        } failure:nil];
    }
    
    // Step 2: writing route if it's enabled
    if(self.isRouteTrackingEnabled && !self.isCurrentRouteTrackingCancelled){
        
        NSMutableArray<CLLocation*> *filteredLocations=[NSMutableArray array];
        
        for(int i=0;i<locations.count;i++){
            CLLocation *location=locations[i];
            
            // Filter out points by invalid accuracy
            if(location.horizontalAccuracy<0) continue;
            if(location.horizontalAccuracy>50) continue;
            
            // Filter out points by invalid accuracy
            #if !TARGET_IPHONE_SIMULATOR
            if (location.verticalAccuracy < 0) continue;
            #endif
            
            if(self.lastKnownValidLocation){
                // Filter out points that are out of order
                NSTimeInterval secondsSinceLastPoint = [location.timestamp timeIntervalSinceDate:self.lastKnownValidLocation.timestamp];
                if (secondsSinceLastPoint < 0) continue;
                
                if ((self.lastKnownValidLocation.coordinate.latitude == location.coordinate.latitude) && (self.lastKnownValidLocation.coordinate.longitude == location.coordinate.longitude)) continue;
                
                CLLocationDistance dist = [location distanceFromLocation:self.lastKnownValidLocation];
                
                
                //check if traverlled distance is lower than accuracy
                if( dist <= location.horizontalAccuracy) continue;
            }

            
            [filteredLocations addObject:location];
            self.lastKnownValidLocation=location;
        }
        
        [self addLocationsToCurrentRoute:filteredLocations];
        [[NSNotificationCenter defaultCenter] postNotificationName:kGeoManagerUpdatedCurrentRoute object:nil];
        
        CLLocationAccuracy avAc=0;
        CLLocationAccuracy avVac=0;
        int count=0;
        for(CLLocation *loc in locations){
            avAc+=loc.horizontalAccuracy;
            avVac+=loc.verticalAccuracy;
            count++;
        }
        avAc=avAc/count;
        avVac=avVac/count;
        [self postLoggingLocalNotificationWithText:[NSString stringWithFormat:@"updates c:%d avAc:%lf avVac:%lf filtC:%ld allows backgrupd %@ deferupds avilbl %@",count,avAc,avVac , filteredLocations.count,  self.locationManager.allowsBackgroundLocationUpdates? @"YES" : @"NO",  [CLLocationManager deferredLocationUpdatesAvailable]? @"YES" : @"NO"]];
        
//        NSLog(@"%@",[CLLocationManager deferredLocationUpdatesAvailable] ? @"YES" : @"NO");
//        if(!isDeferringUpdates){
//            isDeferringUpdates=YES;
//            [_locationManager allowDeferredLocationUpdatesUntilTraveled:500 timeout:2*60];
//        }
    }

}




- (void) locationManager:(CLLocationManager *)manager didVisit:(CLVisit *)visit{
    
    if([visit.departureDate isEqualToDate:[NSDate distantFuture]]){
        
        //makecheckin
        CLLocation *location=[[CLLocation alloc]  initWithLatitude:visit.coordinate.latitude longitude:visit.coordinate.longitude];
        TLPlacesAPIType placesAPIType=[(NSNumber*)[[NSUserDefaults standardUserDefaults] objectForKey:kPlacesAPIType] integerValue];
        [self makeCheckinWithLocation:location withAPI:placesAPIType withTask:nil completionBlock:^{
        } failureBlock:nil withDate:0]; // !!!
    } else {
        //departure visit
    }
}

- (void) locationManager:(CLLocationManager *)manager didFinishDeferredUpdatesWithError:(NSError *)error{
    isDeferringUpdates=NO;
    if(!error){
        [self postLoggingLocalNotificationWithText:[NSString stringWithFormat:@"deffered finish"]];
     } else {
         [self postLoggingLocalNotificationWithText:[NSString stringWithFormat:@"deffered error:%@",error.localizedDescription]];
     }
}

-(void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kGeoManagerChangeLocationManagerAuthorizationStatus object:nil userInfo:@{
                                                                                                                                         @"status":@(status)
                                                                                                                                         }];
    
//    if(status==kCLAuthorizationStatusAuthorizedAlways){
//        self.mapView.myLocationEnabled=YES;
//    }
}

- (void) locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
    
    NSInteger taskId=[region.identifier integerValue];
    TLUserTask *task=[TLUserTask userTaskWithId:@(taskId)];
    
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        TLUserTask *taskInC=[TLUserTask userTaskWithId:@(taskId) inContext:localContext];
        [taskInC moveTaskToRecentPastInContext:localContext];
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
    
    UILocalNotification *notification=[[UILocalNotification alloc] init];
    notification.alertBody=[NSString stringWithFormat:@"Moved near task:%@",task.desc];
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    
    
    [_locationManager stopMonitoringForRegion:region];
    
}





@end

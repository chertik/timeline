//
//  TLListCollectionVC.m
//  TimeLine
//
//  Created by Evgenii Oborin on 23.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLListCollectionVC.h"
#import "TLUserTask.h"
#import "UIImage+TLIcon.h"
#import "TLListCollectionCell.h"
#import "TLListSectionHeader.h"
#import "UICollectionView+NSFetchedResultsController.h"
#import "TLPlaceData+CoreDataClass.h"

#define kAutocheckinMarkRed @"AutocheckinMarkRed"
#define kAutocheckinMarkGray @"AutocheckinMarkGray"
#define kManualcheckinMark @"ManualcheckinMarkPink"

@interface TLListCollectionVC () <NSFetchedResultsControllerDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSFetchedResultsController *fetchedTimeline;

@property (strong,nonatomic) NSBlockOperation *blockOperation;
@property (nonatomic) BOOL shouldReloadCollectionView;

@end

@implementation TLListCollectionVC

static NSString * const reuseIdentifier = @"TLListCollectionReuseID";
static NSString * const sectionHeaderReuseIdentifier = @"TLListSectionHeaderReuseID";

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.backgroundColor=[UIColor clearColor];
    self.collectionView.backgroundColor=[UIColor clearColor];
    self.shouldReloadCollectionView=NO;
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerNib:[UINib nibWithNibName:@"TLListCollectionCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"TLListSectionHeader" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:sectionHeaderReuseIdentifier];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark custom setups

- (NSFetchedResultsController *)fetchedTimeline {
    if (_fetchedTimeline != nil) {
        return _fetchedTimeline;
    }
    _fetchedTimeline = [TLUserTask MR_fetchAllGroupedBy:@"dayName" withPredicate:nil sortedBy:@"id" ascending:NO delegate:self];
//    _fetchedTimeline = [TLUserTask MR_fetchAllSortedBy:@"id" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    return _fetchedTimeline;
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.fetchedTimeline sections].count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[[self.fetchedTimeline sections] objectAtIndex:section] numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TLListCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    TLUserTask *userTask = [_fetchedTimeline objectAtIndexPath:indexPath];
    
  
    cell.descriptionLabel.text=userTask.desc;

    TLPlaceData *placeData=[TLPlaceData placeDataWithId:userTask.selected_place_id];
    if(placeData){
        cell.adressLabel.text=placeData.address;
    } else {
        cell.adressLabel.text=userTask.address;
    }
    [cell setPastAppearance:userTask.is_past.boolValue];
    [userTask iconForSize:CGSizeMake(41, 41) callback:^(UIImage * _Nonnull image) {
        [cell.iconView setImage:image];
    }];
    
    cell.checkinMark.hidden=NO;
    if(userTask.is_checked.boolValue){
        if(userTask.is_autocheckin.boolValue){
            [cell.checkinMark setImage:[UIImage imageNamed:kAutocheckinMarkRed]];
        } else {
            [cell.checkinMark setImage:[UIImage imageNamed:kManualcheckinMark]];
        }
    } else if(userTask.is_autocheckin.boolValue){
        [cell.checkinMark setImage:[UIImage imageNamed:kAutocheckinMarkGray]];
    } else {
        cell.checkinMark.hidden=YES;
    }
    
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedTimeline sections][indexPath.section];
    /*TLUserTask *item = sectionInfo.objects[0];
    NSString *dayName = item.dayName;*/
    TLListSectionHeader *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                withReuseIdentifier:sectionHeaderReuseIdentifier forIndexPath:indexPath];
    headerView.label.text = [sectionInfo name];
    return headerView;
}

#pragma mark <UICollectionViewDelegate>

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    TLUserTask *userTask=[self.fetchedTimeline objectAtIndexPath:indexPath];
    
    [self.delegate pushDetailViewWithTask:userTask];
    
}



/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

#pragma mark NSFetchedResultDelegate
/*
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{

}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
    [self.collectionView addChangeForSection:sectionInfo atIndex:sectionIndex forChangeType:type];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    [self.collectionView addChangeForObjectAtIndexPath:indexPath forChangeType:type newIndexPath:newIndexPath];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
     [self.collectionView commitChanges];
    
}*/

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
//    self.shouldReloadCollectionView = NO;
//    self.blockOperation = [[NSBlockOperation alloc] init];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    
//    __weak UICollectionView *collectionView = self.collectionView;
//    switch (type) {
//        case NSFetchedResultsChangeInsert: {
//            [self.blockOperation addExecutionBlock:^{
//                [collectionView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
//            }];
//            break;
//        }
//            
//        case NSFetchedResultsChangeDelete: {
//            [self.blockOperation addExecutionBlock:^{
//                [collectionView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
//            }];
//            break;
//        }
//            
//        case NSFetchedResultsChangeUpdate: {
//            [self.blockOperation addExecutionBlock:^{
//                [collectionView reloadSections:[NSIndexSet indexSetWithIndex:sectionIndex]];
//            }];
//            break;
//        }
//            
//        default:
//            break;
//    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    
//    __weak UICollectionView *collectionView = self.collectionView;
//    switch (type) {
//        case NSFetchedResultsChangeInsert: {
//            if ([self.collectionView numberOfSections] > 0) {
//                if ([self.collectionView numberOfItemsInSection:indexPath.section] == 0) {
//                    self.shouldReloadCollectionView = YES;
//                } else {
//                    [self.blockOperation addExecutionBlock:^{
//                        [collectionView insertItemsAtIndexPaths:@[newIndexPath]];
//                    }];
//                }
//            } else {
//                self.shouldReloadCollectionView = YES;
//            }
//            break;
//        }
//            
//        case NSFetchedResultsChangeDelete: {
//            if ([self.collectionView numberOfItemsInSection:indexPath.section] == 1) {
//                self.shouldReloadCollectionView = YES;
//            } else {
//                [self.blockOperation addExecutionBlock:^{
//                    [collectionView deleteItemsAtIndexPaths:@[indexPath]];
//                }];
//            }
//            break;
//        }
//            
//        case NSFetchedResultsChangeUpdate: {
//            [self.blockOperation addExecutionBlock:^{
//                [collectionView reloadItemsAtIndexPaths:@[indexPath]];
//            }];
//            break;
//        }
//            
//        case NSFetchedResultsChangeMove: {
//            [self.blockOperation addExecutionBlock:^{
//                [collectionView moveItemAtIndexPath:indexPath toIndexPath:newIndexPath];
//            }];
//            break;
//        }
//            
//        default:
//            break;
//    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
//    if (self.shouldReloadCollectionView) {
//        [self.collectionView reloadData];
//    } else {
//        [self.collectionView performBatchUpdates:^{
//            [self.blockOperation start];
//        } completion:nil];
//    }
    [self.collectionView reloadData];
}




#pragma mark <FlowLayoytDelegate>

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    CGSize bounds = self.view.superview.bounds.size;
    if(bounds.width==0){
        return CGSizeMake(10, 10);
    }
    CGSize cellSize=CGSizeMake(bounds.width-40, 52);
    return cellSize;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    
    CGSize bounds = self.view.superview.bounds.size;
    if(bounds.width==0){
        return CGSizeMake(10, 10);
    }
    CGSize cellSize=CGSizeMake(bounds.width-40, 27);
    return cellSize;
}

- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 20, 0, 20);
}

@end

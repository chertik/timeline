//
//  Constants.h
//  TimeLine
//
//  Created by Evgenii Oborin on 26.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//------------API's----------------

//GOOGLE API

//#define kGoogleMapsKey @"AIzaSyDdEwHmo6vYicI10DoJkOhNoV9grGofKVI"
#define kGoogleMapsKey @"AIzaSyCw8GAf-r4zGByg5WWubxm19SVKqVb08ss"
#define kGooglePlacesWebKey @"AIzaSyBcxMNAVptElPjh4OkTzuNOCNOFGFuBtns"

//Segues ID's
#define segueToFirstViewController @"segueAfterLaunchScreen"
#define segueToPlacesListController @"TLPlacesListSegue"
#define segueToAddToMapController @"TLAddToMapControllerSegue"


//Notifications names
#define kTLPlacesListChangedPlaceNotification @"TLPlacesListChangedPlaceNotification"
#define kTLTasksChangedInDB @"kTLTasksChangedInDB"
#define kTLAddedCheckins @"kTLAddedCheckins"



//FOURSQUARE API

#define kFS_CLIENTID @"1O3TGY0LASJB0CP0TGGPQVHLP2SYJSNYVVUBM4L4U1XKWYGR"
#define kFS_CLIENTSECRET @"AGTYLB5RPGK1AKDBW54VYJYCYMKG1VJ2V5QH3KY222F2SRJ4"

//NSUserDefaultsKeys
#define kSettingsRouteTrackingEnabled @"kSettingsRouteTrackingEnabled"
#define kSettingsLoggingEnabled @"kSettingsLoggingEnabled"
#define kLastVisitDate @"kLastVisitDate"
#define kLastVisitLat @"kLastVisitLat"
#define kLastVisitLong @"kLastVisitLong"
#define kPlacesAPIType @"kPlacesAPIType"

#define kDefaultsLastPositionLatitude @"kDefaultsLastPositionLatitude"
#define kDefaultsLastPositionLongitude @"kDefaultsLastPositionLongitude"

#define kUserdefaultsFilterSettingsAccuracyThreshold @"kUserdefaultsFilterSettingsAccuracyThreshold"
#define kUserdefaultsFilterSettingsDistanceThreshold @"kUserdefaultsFilterSettingsDistanceThreshold"
#define kUserdefaultsFilterSettingsSpideringfilterDistanceFromVisit @"kUserdefaultsFilterSettingsSpideringfilterDistanceFromVisit"
#define kUserdefaultsFilterSettingsLowPassInnertiaCoeefincy @"kUserdefaultsFilterSettingsLowPassInnertiaCoeefincy"

#define kUserdefaultsPassedAuthorization @"kUserdefaultsPassedAuthorization"

//-----------COLORS, VARIABLES-----------

#define dThemeColorGray [UIColor colorWithRed:106/255.0 green:111/255.0 blue:114/255.0 alpha:1]

#define dThemeColorRed [TLThemeManager mainAccentColor]


#endif /* Constants_h */

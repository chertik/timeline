//
//  TLThemeManager.h
//  TimeLine
//
//  Created by Evgenii Oborin on 13.04.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

#define sSelectedThemeKey @"sSelectedThemeKey"

typedef enum : NSUInteger {
    TLThemeOcher
} TLTheme;

@interface TLThemeManager : NSObject

+ (UIColor*) mainAccentColor;
+ (UIColor*) lighterGrayColor;
+ (UIColor*) gradientIntermediateColor;

+ (UIColor*) statusBarBackgroundColor;


+ (TLTheme) currentTheme;
+ (void) applyTheme:(TLTheme) theme;

@end

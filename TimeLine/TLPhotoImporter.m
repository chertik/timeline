//
//  TLPhotoImporter.m
//  TimeLine7
//
//  Created by EVGENY Oborin on 11.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLPhotoImporter.h"
#import "TLGeoManager.h"
#import <Photos/Photos.h>
#import "Constants.h"

#define MAX_ASSETS_INTASK 7

@interface TLPhotoImporter()

@property (atomic) BOOL isImportInProgress;

@end

@implementation TLPhotoImporter

+ (instancetype) sharedManager{
    static TLPhotoImporter *manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager=[[TLPhotoImporter alloc]init];
    });
    return manager;
}

- (void) checkForNewPhotosAndImport {
    //determine what api type is currently used to define places in location
    NSLog(@"trying to update photos");
    if (self.isImportInProgress) {
        return;
    }
    
    self.isImportInProgress = YES;
    
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status == PHAuthorizationStatusAuthorized) {
            
            NSArray<PHAsset*> *filteredAssets = [self getNewPhotosAssetsArrayWithLocation];
            
            if (filteredAssets.count == 0) {
                self.isImportInProgress = NO;
                return;
            }
            
            NSArray<NSArray<PHAsset*>*> *groupedAssets = [self groupAssets:filteredAssets];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"dd.MM.YYYY";
            
            if (groupedAssets.count > 0) {
                
                PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
                requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
                requestOptions.synchronous = YES;
                dispatch_semaphore_t aSemaphore = dispatch_semaphore_create(1);
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                    
                    for (NSArray<PHAsset*> *assetGroup in groupedAssets) {
                        
                        dispatch_semaphore_wait(aSemaphore, DISPATCH_TIME_FOREVER);
                        
                        PHAsset *firstAsset = assetGroup.firstObject;
                        
                        NSDate *shootDate = firstAsset.creationDate;
                        int elapsedTime = [shootDate timeIntervalSince1970];
                        
                        CLLocation *location = firstAsset.location;
                        
                        CGSize size = CGSizeMake(50, 50);
                        
                        [[PHImageManager defaultManager] requestImageForAsset:firstAsset targetSize:size contentMode:PHImageContentModeAspectFill options:requestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                            
                            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                                
                                [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                                    NSDate *currentDate;
                                    if (elapsedTime != 0) {
                                        currentDate = [NSDate dateWithTimeIntervalSince1970:elapsedTime];
                                    } else { currentDate = [NSDate date]; }
                                    
                                    TLUserTask *newVisit;
                                    
                                    newVisit=[TLUserTask MR_createEntityInContext:localContext];
                                    
                                    newVisit.latitude=@(location.coordinate.latitude);
                                    newVisit.longitude=@(location.coordinate.longitude);
                                    newVisit.desc=[dateFormatter stringFromDate:currentDate];
                                    newVisit.icon_path=@"cafe";
                                    
                                    newVisit.is_visible=@0;
                                    newVisit.is_autocheckin=@1;
                                    
                                    newVisit.datetime = currentDate;
                                    newVisit.is_timeline=@1;
                                    
                                    //                        newVisit.selected_place_id=firstData.id;
                                    newVisit.address=[NSString stringWithFormat:@"Фото из бибилотеки пользователя %@",[dateFormatter stringFromDate:currentDate]];
                                    //                        newVisit.placesList=placesSet;
                                    //NSLog(@"%ld", newVisit.id.integerValue - 1);
                                    //                        [newVisit moveTaskToPastByTimeWithMaxInd:newVisit.id.integerValue - 1];
                                    [newVisit moveTaskToPastByTimeWithMaxInd:newVisit.id.integerValue - 1 inContext:localContext];
                                    //[newVisit moveTaskToRecentPastInContext:localContext];
                                    
                                    if (result != nil ) {
                                        NSMutableOrderedSet<TLUserTaskPhoto*> *photoSet = [[NSMutableOrderedSet alloc] init];
                                        for (PHAsset *asset in assetGroup) {
                                            TLUserTaskPhoto *photo=[TLUserTaskPhoto MR_createEntityInContext:localContext];
                                            photo.image = result;
                                            photo.asset = asset.localIdentifier;
                                            [photoSet addObject:photo];
                                        }
                                        
                                        newVisit.taskPhoto=[NSOrderedSet orderedSetWithOrderedSet:photoSet];
                                    }
                                    
                                } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                                    
                                    NSDate *maxDate = firstAsset.creationDate;
                                    for (PHAsset *itAsset in assetGroup) {
                                        if (itAsset.creationDate > maxDate) {
                                            maxDate = itAsset.creationDate;
                                        }
                                    }
                                    
                                    [[NSUserDefaults standardUserDefaults] setObject:maxDate forKey:kDefaultsKeyLastImportedPhotoDate];
                                    dispatch_semaphore_signal(aSemaphore);
                                }];
                            });
                            
                        }];
                        
                    }//end of for loop
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:kTLTasksChangedInDB object:nil];
                        self.isImportInProgress = NO;
                    });
                      
                });
                
                
            } else {
                self.isImportInProgress = NO;
            }
        }
    }];
}

#pragma mark - Utilites

- (NSArray<NSArray<PHAsset*>*>*) groupAssets:(NSArray<PHAsset*>*) assets {
    NSMutableArray *array = [[NSMutableArray alloc ] init];
    
    NSMutableArray *currentGroup = [[NSMutableArray alloc] init];
    int i = 0;
    while (i < assets.count) {
        PHAsset *asset = assets[i];
        if (currentGroup.count == 0) {
            [currentGroup addObject:asset];
            i++;
            continue;
        } else {
            if (currentGroup.count < MAX_ASSETS_INTASK && [self asset:asset belongsToOneGroupWithAsset:[currentGroup firstObject]]) {
                [currentGroup addObject:asset];
                i++;
                continue;
            } else {
                [array addObject:currentGroup];
                currentGroup = [[NSMutableArray alloc] init];
                continue;
            }
         }
    }
    
    if (![array containsObject:currentGroup]) {
        [array addObject:currentGroup];
    }
    
    return array;
}

- (BOOL) asset: (PHAsset*) asset1 belongsToOneGroupWithAsset:(PHAsset*) asset2 {
    NSTimeInterval timeInterval = 1 * 60 * 60; //1 hour
    CLLocationDistance distance = 200; //200 meters
    if (fabs([asset1.creationDate timeIntervalSinceDate:asset2.creationDate]) < timeInterval && ([asset1.location distanceFromLocation:asset2.location] < distance)) {
        return YES;
    } else {
        return NO;
    }
}

- (NSArray<PHAsset*>*) getNewPhotosAssetsArrayWithLocation {
    
    //Check date of last imported photo to import only new photo
    NSDate *lastImportedPhotoDate = [[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsKeyLastImportedPhotoDate];
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    
    //if date is not nil, set predicate to get only new photos
    if (lastImportedPhotoDate != nil) {
        fetchOptions.predicate = [NSPredicate predicateWithFormat:@"creationDate > %@",lastImportedPhotoDate];
    }
    NSSortDescriptor *creationDateSortDescriptior = [NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES];
    fetchOptions.sortDescriptors = @[creationDateSortDescriptior];
    
//    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    
    //getting array of assets with locations
    NSMutableArray<PHAsset*> *allAssets = [NSMutableArray array];
    
    PHFetchResult * moments = [PHAssetCollection fetchMomentsWithOptions:nil];
    for (PHAssetCollection * moment in moments) {
        PHFetchResult * assetsFetchResults = [PHAsset fetchAssetsInAssetCollection:moment options:fetchOptions];
        for (PHAsset * asset in assetsFetchResults) {
            [allAssets addObject:asset];
        }
    }
    
    NSArray<PHAsset*> *filteredAssets = [self getAssetsWithLocationFromFetchResults:allAssets];
    NSArray<PHAsset*> *sortedAssets = [filteredAssets sortedArrayUsingDescriptors:@[creationDateSortDescriptior]];
    
    return sortedAssets;
}

- (NSArray<PHAsset*>*) getAssetsWithLocationFromFetchResults:(NSArray*)fetchResults{
    NSMutableArray *assetsArray = [NSMutableArray new];
    for (PHAsset* asset in fetchResults) {
        if (asset.location != NULL) {
            [assetsArray addObject:asset];
        }
    }
//    [fetchResults enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL * _Nonnull stop) {
//        if (asset.location != NULL) {
//            [assetsArray addObject:asset];
//        }
//    }];
    return assetsArray;
}

@end

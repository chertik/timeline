//
//  TLIconButton.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 11/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "ASShapeButton.h"

typedef NS_ENUM(NSInteger, TLIconButtonStyle){
    TLIconButtonStyleColor,
    TLIconButtonStyleGray,
    TLIconButtonStyleWhite,
    TLIconButtonStylePink
};

@interface TLIconButton : ASShapeButton

@property (nonatomic, readonly) TLIconButtonStyle style;

- (void)setStyle:(TLIconButtonStyle)style;

@end

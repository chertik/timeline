//
//  TLAuthorizationVC.m
//  TimeLine
//
//  Created by Evgenii Oborin on 24.05.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLAuthorizationVC.h"
#import "Constants.h"
#import "UIAuthButton.h"
#import "TLThemeManager.h"
#import <VK_ios_sdk/VKSdk.h>
#import "TLBackendManager.h"
//#import <FBSDKCoreKit/FBSDKCoreKit.h>
//#import "FBSDKLoginKit.framework/FBSDKLoginKit"

@import FBSDKCoreKit;
@import FBSDKLoginKit;
@import PhoneNumberKit;
@import SVProgressHUD;

@interface TLAuthorizationVC () <UITextFieldDelegate, VKSdkUIDelegate,VKSdkDelegate,UIWebViewDelegate>

// OUTLETS ----------------------------------------------------------------------------------------------------------
@property (weak, nonatomic) IBOutlet UIAuthButton *phoneButton;
@property (weak, nonatomic) IBOutlet UIAuthButton *vkButton;
@property (weak, nonatomic) IBOutlet UIAuthButton *middleButton;
@property (weak, nonatomic) IBOutlet UIAuthButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIAuthButton *instagramButton;

@property (weak, nonatomic) IBOutlet PhoneNumberTextField *phoneNumberField;
@property (weak, nonatomic) IBOutlet UIView *phoneNumberLine;
@property (weak, nonatomic) IBOutlet UIView *phoneContainer;
@property (weak, nonatomic) IBOutlet UIImageView *phoneSendButton;


@property (strong,nonatomic) NSArray<UIAuthButton*> *buttons;

@property (strong,nonatomic) UIViewController *modalWebViewController;

// PROPERTIES ----------------------------------------------------------------------------------------------------------

@property (strong,nonatomic) NSArray<NSString*> *notActiveNames;
@property (strong,nonatomic) NSArray<NSString*> *activeNames;

@property (nonatomic) BOOL isShowingPhoneField;
@property (nonatomic) BOOL isValidNumber;

@property (strong,nonatomic) VKSdk *vksdkInstance;

@property (strong,nonatomic) NSString *instToken;

@end

@implementation TLAuthorizationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupViews];
    
    
    //- Gesture Recognizers
    
    //Dismiss gr
    UITapGestureRecognizer *dismissKeyboardGR=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:dismissKeyboardGR];
    
    //Send phone button gr
    UITapGestureRecognizer *sendPhoneGR=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendPhoneButtonTap:)];
    [self.phoneSendButton addGestureRecognizer:sendPhoneGR];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initialization

- (void) setupViews{
    _buttons=@[
               _phoneButton,
               _vkButton,
               _middleButton,
               _facebookButton,
               _instagramButton
               ];
    
    self.notActiveNames=@[@"i-ph",@"i-vk",@"CurAddrIcAltrntv2",@"i-in",@"i-fb"];
    self.activeNames=@[@"f-ph",@"f-vk",@"CurAddrIcAltrntv2",@"f-in",@"f-fb"];
    
    for(UIAuthButton *button in _buttons){
        UITapGestureRecognizer *tapGR=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bottomButtonTapHandle:)];
        [button addGestureRecognizer:tapGR];
        [self makeUIAuthButton:button active:NO];
    }
    
    [self makeUIAuthButton:_middleButton active:YES ];
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Телефон" attributes:@{
                                                                                                 NSForegroundColorAttributeName : [UIColor darkGrayColor] }];
    self.phoneNumberField.attributedPlaceholder = str;
    self.phoneNumberField.withPrefix=YES;
    self.phoneNumberField.defaultRegion=@"RU";
    
//    PhoneNumberKit *kit=[[PhoneNumberKit alloc] init];
    
    self.phoneContainer.alpha=0;
    
    UIAuthButton *centerButton=self.buttons[2];
    CGFloat enlargeScale=1.18421053;
    centerButton.borderView.contentScaleFactor=[[UIScreen mainScreen] scale]*enlargeScale;
    CGAffineTransform scTr=CGAffineTransformMakeScale(enlargeScale, enlargeScale);
    CGAffineTransform trans=CGAffineTransformTranslate(scTr, 0, -2);
    centerButton.borderView.transform=trans;
    
    

    
}

#pragma mark - Actions


- (void) bottomButtonTapHandle:(UITapGestureRecognizer*) gr{
    
    NSInteger indexOfButton=[self.buttons indexOfObject:(UIAuthButton*)gr.view];
    
    switch (indexOfButton) {
        case 0:
            if(self.isShowingPhoneField){
                [self hidePhoneField];
                [self makeUIAuthButton:(UIAuthButton*)gr.view active:NO];
            } else {
                [self showPhoneField];
                [self makeUIAuthButton:(UIAuthButton*)gr.view active:YES];
            }
            
            self.isShowingPhoneField=!self.isShowingPhoneField;
            break;
        case 1:
            [self startVkAuthorization];
            break;
        case 2:
            [self dismissAuthView];
            break;
        case 3:
            [self startInstAuthorization];
            break;
        case 4:
            [self startFbAuthorization];
            break;
        default:
            break;
    }
    
    
}

- (void) dismissKeyboard:(UITapGestureRecognizer*)gr{
    [self.view endEditing:YES];
}

- (void) sendPhoneButtonTap:(UITapGestureRecognizer*) gr{
    if(!self.isValidNumber) {
        [SVProgressHUD showErrorWithStatus:@"Введен некорректный номер телефона"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Проверка номера"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD showSuccessWithStatus:@"Телефон подключен"];
        
        [self.view endEditing:YES];
        
        self.isShowingPhoneField=NO;
        [self hidePhoneField];
        self.buttons[0].checkMark.hidden=NO;
    });
    
}

- (IBAction)fieldValueChanged:(id)sender {
    self.isValidNumber=self.phoneNumberField.isValidNumber;
    [self setPhoneSendButtonActive:self.phoneNumberField.isValidNumber];
}


#pragma mark - Utilites

- (void) showPhoneField{
    self.phoneContainer.transform=CGAffineTransformMakeScale(0.6, 0.6);
    self.phoneContainer.transform=CGAffineTransformTranslate(self.phoneContainer.transform, 0, 30);
    
    [UIView animateWithDuration:0.15 animations:^{
        self.phoneContainer.transform=CGAffineTransformIdentity;
        self.phoneContainer.alpha=1;
    }];
}

- (void) hidePhoneField{

    [UIView animateWithDuration:0.15 animations:^{
        self.phoneContainer.transform=CGAffineTransformMakeScale(0.6, 0.6);
        self.phoneContainer.transform=CGAffineTransformTranslate(self.phoneContainer.transform, 0, 30);
        self.phoneContainer.alpha=0;
    }];
}

- (void) setPhoneSendButtonActive:(BOOL) active{
    if(active){
        [self.phoneSendButton setImage:[UIImage imageNamed:@"AutocheckinMarkRed"]];
    } else {
        [self.phoneSendButton setImage:[UIImage imageNamed:@"AutocheckinMarkGray"]];
    }
}

- (void) makeUIAuthButton:(UIAuthButton*)button active:(BOOL) active{
    [self makeUIAuthButton:button active:active andHaveMark:NO];
}

- (void) makeUIAuthButton:(UIAuthButton*)button active:(BOOL) active andHaveMark:(BOOL)haveMark{

    NSInteger buttonInd=[self.buttons indexOfObject:button];
    button.borderView.layer.borderColor= active? dThemeColorRed.CGColor : dThemeColorGray.CGColor;
    
    NSString *iconName= active? self.activeNames[buttonInd] : self.notActiveNames[buttonInd];
    
    [button.iconView setImage:[UIImage imageNamed:iconName]];
    
    button.checkMark.hidden=!haveMark;
}

- (void) dismissAuthView{
    [[NSNotificationCenter defaultCenter] postNotificationName:kUserdefaultsPassedAuthorization object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Authorization

#pragma mark Inst

- (void) startInstAuthorization{
    UIViewController *modelWebViewController=[[UIViewController alloc]init];
    
    UIWebView *webView=[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    modelWebViewController.view=webView;
    
    webView.delegate=self;

    UINavigationController *navVC=[[UINavigationController alloc] initWithRootViewController:modelWebViewController];
    UIBarButtonItem *dismiss=[[UIBarButtonItem alloc] initWithTitle:@"Отмена" style:UIBarButtonItemStylePlain target:self action:@selector(dismissModalWebView:)];
    modelWebViewController.navigationItem.leftBarButtonItem=dismiss;
    
    [self presentViewController:navVC animated:YES completion:nil];
    
    self.modalWebViewController=navVC;
    
    NSString *stringurl=@"https://instagram.com/oauth/authorize/?client_id=5479e235cf0341adaf762112ffc01fb1&redirect_uri=https://lifein.com/auth&response_type=token";
    NSURL *url=[NSURL URLWithString:stringurl];
    
    NSURLRequest *request=[NSURLRequest requestWithURL:url];
    
    [webView loadRequest:request];
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSError *error;
    if([self validAccessTokenFromURL:request.URL appRedirectPath:@"https://lifein.com/auth" error:&error]){
        [self dismissModalWebView:self];
        [[TLBackendManager sharedManager] loginInBackgroundWithVkId:self.instToken block:^(NSError *error) {
            if(error){
                NSLog(@"%@",error);
            } else{
                [self makeUIAuthButton:self.buttons[3] active:YES andHaveMark:YES];
                self.buttons[3].userInteractionEnabled = false;
            }
        }];
    }
    
    return YES;
}


- (BOOL)validAccessTokenFromURL:(NSURL *)url
                appRedirectPath:(NSString *)appRedirectPath
                          error:(NSError *__autoreleasing *)error
{
    NSURL *appRedirectURL = [NSURL URLWithString:appRedirectPath];
    
    BOOL identicalURLSchemes = [appRedirectURL.scheme isEqual:url.scheme];
    BOOL identicalURLHosts = [appRedirectURL.host isEqual:url.host];
    // For app:// base URL, the host is nil.
    BOOL isAppURL = (BOOL)(appRedirectURL.host == nil);
    if (!identicalURLSchemes || (!isAppURL && !identicalURLHosts)) {
        return NO;
    }
    
    NSString *formattedURL = nil;
    // For app:// base url the fragment is nil
    if (url.fragment) {
        formattedURL = url.fragment;
    } else {
        formattedURL = url.resourceSpecifier;
    }
    
    NSString *token = [self queryStringParametersFromString:formattedURL][@"access_token"];
    BOOL success = token.length;
    if (success) {
        self.instToken = token;
    }
    else {
        NSString *localizedDescription = NSLocalizedString(@"Authorization not granted.", @"Error notification to indicate Instagram OAuth token was not provided.");
        NSLog(@"%@",localizedDescription);
    }
    return success;
}

- (NSDictionary *)queryStringParametersFromString:(NSString*)string {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [[string componentsSeparatedByString:@"&"] enumerateObjectsUsingBlock:^(NSString * param, NSUInteger idx, BOOL *stop) {
        NSArray *pairs = [param componentsSeparatedByString:@"="];
        if ([pairs count] != 2) return;
        
        NSString *key = [pairs[0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *value = [pairs[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        // Must manually clean when passed string is url.resourceSpecifier
        if ([[key substringToIndex:1] isEqualToString: @"#"]) {
            key = [key substringFromIndex:1];
        }
        
        [dict setObject:value forKey:key];
    }];
    return dict;
}




- (void) dismissModalWebView:(id)sender{
    [self.modalWebViewController dismissViewControllerAnimated:YES completion:nil];
    self.modalWebViewController=nil;
}

#pragma mark FB

- (void) startFbAuthorization{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             [[TLBackendManager sharedManager] loginInBackgroundWithFbId:result.token.userID block:^(NSError *error) {
                 if(error){
                     NSLog(@"%@",error);
                 } else{
                     [self makeUIAuthButton:self.buttons[4] active:YES andHaveMark:YES];
                     self.buttons[4].userInteractionEnabled = false;
                 }
             }];
         }
     }];
}

#pragma mark VK

- (void) startVkAuthorization{
    VKSdk *sdkInstance = [VKSdk initializeWithAppId:@"6064500"];
    
    [sdkInstance registerDelegate:self];
    [sdkInstance setUiDelegate:self];
    
    self.vksdkInstance=sdkInstance;
    
    [VKSdk authorize:nil];
}

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result{
    if (result.token) {
        // Пользователь успешно авторизован
        
        [[TLBackendManager sharedManager] loginInBackgroundWithVkId:result.token.userId block:^(NSError *error) {
            if(error){
                NSLog(@"%@",error);
            } else{
                [self makeUIAuthButton:self.buttons[1] active:YES andHaveMark:YES];
                self.buttons[1].userInteractionEnabled = false;
            }
        }];
    } else if (result.error) {
        // Пользователь отменил авторизацию или произошла ошибка
    }
}

- (void) vkSdkShouldPresentViewController:(UIViewController *)controller{
    [self presentViewController:controller animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  TLTaskIconManager.m
//  TimeLine7
//
//  Created by EVGENY Oborin on 13.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLTaskIconManager.h"
#import "UIImage+TLIcon.h"

@interface TLTaskIconManager()

@property (strong, nonatomic) NSMutableDictionary<NSString*,UIImage*> *cachedImages;

@end

@implementation TLTaskIconManager

+ (instancetype) sharedManager{
    static TLTaskIconManager *manager=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager=[[TLTaskIconManager alloc]init];
    });
    return manager;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        _cachedImages = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (UIImage*) sharedImageForIconPath:(NSString*)iconPath size:(CGSize)size square:(BOOL)square border:(BOOL) border shadow:(BOOL)shadow isPhoto:(BOOL)isPhoto isPink:(BOOL)isPink {
    
    NSString *cacheKey = [self cacheKeyForIconPath:iconPath size:size square:square border:border shadow:shadow isPhoto:isPhoto isPink:isPink];
    UIImage *sharedImage = self.cachedImages[cacheKey];
    if (sharedImage != nil) {
        return sharedImage;
    } else {
        UIImage *image = [UIImage imageWithColorIcon:[UIImage imageNamed:iconPath] size:size square:square border:border shadow:shadow isPhoto:isPhoto isPink:isPink];
        self.cachedImages[cacheKey] = image;
        return image;
    }
}

- (NSString*) cacheKeyForIconPath:(NSString*)iconPath size:(CGSize)size square:(BOOL)square border:(BOOL) border shadow:(BOOL)shadow isPhoto:(BOOL)isPhoto isPink:(BOOL)isPink {
    return [NSString stringWithFormat:@"%@ %@ %d %d %d %d %d",iconPath,NSStringFromCGSize(size),square,border,shadow,isPhoto,isPink];
}

@end

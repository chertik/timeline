//
//  TLVenue.m
//  TimeLine
//
//  Created by Evgenii Oborin on 18.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLVenue.h"

@implementation TLVenue

- (NSString*) getShortAddress{
    if(self.formattedAddress){
        if(self.formattedAddress[0]){
            return self.formattedAddress[0];
        }
    }
    return nil;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"name:%@, dist:%f m. rating:%f shortAdr:%@",self.name,self.distance.floatValue,self.rating.floatValue,[self getShortAddress]];
}

@end

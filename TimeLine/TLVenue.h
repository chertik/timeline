//
//  TLVenue.h
//  TimeLine
//
//  Created by Evgenii Oborin on 18.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLVenue : NSObject

@property (strong,nonatomic) NSNumber *lat;
@property (strong,nonatomic) NSNumber *lon;

@property (strong,nonatomic) NSString *placeID;

@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSNumber *rating;
@property (strong,nonatomic) NSArray *formattedAddress; //Array of (as I've seen) 2 elements. Using first for adress inside city
@property (strong,nonatomic) NSNumber *distance;

- (NSString*) getShortAddress;

@end

//
//  UIButton+hitTestInsets.h
//  TimeLine
//
//  Created by Evgenii Oborin on 23.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (hitTestInsets)

@property(nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;


@end

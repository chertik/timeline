//
//  TLPhotoMarker.m
//  TimeLine
//
//  Created by Evgenii Oborin on 02.11.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLPhotoMarker.h"

@implementation TLPhotoMarker

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor clearColor];
        self.opaque=NO;
        UIImage *bgImage=[UIImage imageNamed:@"Map marker"];
        UIImageView *imageViewBG=[[UIImageView alloc] initWithImage:bgImage];
        [self addSubview:imageViewBG];
    }
    return self;
}

- (void) setPhoto:(UIImage *)photo{
    UIImageView *photoImageView=[[UIImageView alloc] initWithImage:photo];
    
    [photoImageView setContentMode:UIViewContentModeScaleAspectFill];
    
    photoImageView.frame=CGRectMake(6.32, 6.89, 25, 25);
    photoImageView.layer.cornerRadius=12.5;
    photoImageView.layer.masksToBounds=YES;
    
    [self addSubview:photoImageView];
}

@end

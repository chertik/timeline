//
//  TLView_GetsTouchesOutBounds.m
//  TimeLine
//
//  Created by Evgenii Oborin on 07.11.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLView_GetsTouchesOutBounds.h"

@implementation TLView_GetsTouchesOutBounds

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (!self.clipsToBounds && !self.hidden && self.alpha > 0) {
        for (UIView *subview in self.subviews.reverseObjectEnumerator) {
            CGPoint subPoint = [subview convertPoint:point fromView:self];
            UIView *result = [subview hitTest:subPoint withEvent:event];
            if (result != nil) {
                return result;
            }
        }
    }
    
    return nil;
}

@end

//
//  TLTaskPhotoList.m
//  TimeLine7
//
//  Created by EVGENY Oborin on 14.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLTaskPhotoList.h"
#import <Photos/Photos.h>

@interface TLTaskPhotoList() <UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) NSLayoutConstraint *containerWidthConstraint;
@property (strong, nonatomic) NSMutableArray *imageViews;

@property (nonatomic) NSUInteger currentImagesCount;

@end

@implementation TLTaskPhotoList

- (instancetype) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupViews];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupViews];
    }
    
    return self;
}

- (void) setupViews {
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    _scrollView.pagingEnabled = YES;
    _scrollView.delegate = self; 
    [self addSubview:_scrollView];
    
    [_scrollView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
    [_scrollView.leadingAnchor constraintEqualToAnchor:self.leadingAnchor].active = YES;
    [_scrollView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
    [_scrollView.trailingAnchor constraintEqualToAnchor:self.trailingAnchor].active = YES;
    
    _containerView = [[UIView alloc] initWithFrame:CGRectZero];
    _containerView.translatesAutoresizingMaskIntoConstraints = NO;
    [_scrollView addSubview:_containerView];
    
    [_containerView.heightAnchor constraintEqualToAnchor:self.heightAnchor multiplier:1].active = YES;
    
    _imageViews = [[NSMutableArray alloc] init];
    
    self.containerWidthConstraint = [_containerView.widthAnchor constraintEqualToConstant:self.bounds.size.width];
    self.containerWidthConstraint.active = YES;
    
    [self resizeContent];
}

- (void) setImages: (NSOrderedSet<TLUserTaskPhoto*>*) images {
    
    self.currentImagesCount = images.count;
    
    CGFloat width = self.bounds.size.width;
    
    if (images.count > self.imageViews.count) {
        NSInteger existingCount = self.imageViews.count;
        NSInteger diff = images.count - existingCount;
        
        for (NSInteger i = 0 ; i<diff; i++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
            imageView.translatesAutoresizingMaskIntoConstraints = NO;
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            
            [self.imageViews addObject:imageView];
            [self.containerView addSubview:imageView];
            
            [imageView.topAnchor constraintEqualToAnchor:_containerView.topAnchor].active = YES;
            [imageView.bottomAnchor constraintEqualToAnchor:_containerView.bottomAnchor].active = YES;
            [imageView.leadingAnchor constraintEqualToAnchor:_containerView.leadingAnchor constant:(i+existingCount)*width].active = YES;
            [imageView.widthAnchor constraintEqualToConstant:width].active = YES;
        }
    }
    
    for (NSInteger i = 0; i < images.count; i++){
        UIImageView *imageView = self.imageViews[i];
        TLUserTaskPhoto *taskPhoto = images[i];
        imageView.image = nil;
        CGSize size = self.bounds.size;
        
        if (taskPhoto.asset) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSString *identifier = (NSString*) taskPhoto.asset;
                PHFetchResult *fetchResult = [PHAsset fetchAssetsWithLocalIdentifiers:@[identifier] options:nil];
                PHAsset *asset = [fetchResult firstObject];
                if (asset) {
                    PHImageRequestOptions *requestOptions = [[PHImageRequestOptions alloc] init];
                    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
                    requestOptions.synchronous = NO;
                    [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:size contentMode:PHImageContentModeAspectFill options:requestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
//                            CGRect imageViewFrame = imageView.frame;
                            imageView.image = result;
//                            imageViewFrame = imageView.frame;
                        });
                    }];
                }
            });
        } else if (taskPhoto.hiresImage) {
            imageView.image = (UIImage*) taskPhoto.hiresImage;
        } else if (taskPhoto.image) {
            imageView.image = (UIImage*) taskPhoto.image;
        } else {
            imageView.image = nil;
        }
        
    }
    
    [self resizeContent];
    
    for (NSInteger i = images.count; i < self.imageViews.count; i++) {
        UIImageView *imageView = self.imageViews[i];
        imageView.image = nil;
    }
    
    [self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    
}

- (void) resizeContent {
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    self.containerWidthConstraint.constant = (CGFloat) MAX(1, self.currentImagesCount) * width;
    self.scrollView.contentSize = CGSizeMake(width * (CGFloat) MAX(1, self.currentImagesCount), height);
    
//    for (int i = 0; i < self.imageViews.count; i++) {
//        UIImageView *imageView = self.imageViews[i];
//        imageView.frame = CGRectMake(i * width, 0, width, height);
//    }
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    [self resizeContent];
}

- (UIView*) viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.containerView;
}

@end

//
//  UIImage+TLIcon.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 11/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TLIcon)

+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border  isPink:(BOOL) isPink;
+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border isPhoto:(BOOL) isPhoto isPink:(BOOL) isPink;
+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border shadow:(BOOL)shadow  isPink:(BOOL) isPink;
+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border shadow:(BOOL)shadow isPhoto:(BOOL) isPhoto  isPink:(BOOL) isPink;
+ (UIImage *)imageWithGrayIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border;
+ (UIImage *)imageWithGrayIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border isPhoto:(BOOL) isPhoto;

+ (UIImage *)imageWithWhiteIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border;
+ (UIImage *)imageWithWhiteIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border isPhoto:(BOOL) isPhoto;

+ (UIImage *)imageWithView:(UIView *)view size:(CGSize)size;

@end

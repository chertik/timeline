//
//  UIView+Shadow.h

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIView (Shadow)

- (void)makeInsetShadow;
- (void)makeInsetShadowWithRadius:(float)radius alpha:(float)alpha;
- (void)makeTopInsetShadowWithRadius:(CGFloat)radius color:(UIColor*)color alpha:(CGFloat)alpha;
- (void)makeInsetShadowWithRadius:(float)radius color:(UIColor *)color directions:(NSArray *)directions;

- (void)removeInsetShadow;

@end

//
//  MainViewController.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//
@import CoreLocation;
@import GoogleMaps;
@import GooglePlaces;

#import "MainViewController.h"
#import "TLTimelineVC.h"
#import "UIView+Graphics.h"
#import "UITableViewCell+Additional.h"
#import "UIImage+TLIcon.h"
#import "NSFetchedResultsController+Additional.h"
//#import "RestKit/RestKit.h"
#import "TLCommonPlace.h"
#import "TLPlacesAPIManager.h"
#import "TLGeoManager.h"
#import "TLDelayedCheckin.h"
#import "Reachability.h"
#import "TLPlacesListVC.h"
#import "TLPlaceData+CoreDataClass.h"
#import "TLAddToMapList.h"
#import "DatabaseAPI.h"
#import "TLThemeManager.h"
#import "UIButton+verticalLayout.h"
#import "TLAuthorizationVC.h"
#import "TLBackendManager.h"
#import "TLTaskPhotoList.h"


#define kCellIdentifier @"cellIdentifier"
#define kCellWidth 150

#define cTLPanelHeightTableView 430
#define cTLPanelHeightDetailView 362
#define cTLPanelHeightCompact 120

@import SVProgressHUD;
@import RestKit;
@import SDWebImage;

@interface MainViewController () <TLTimelineVCDelegate,TLPlacesListVCDelegate,TLAddToMapListDelegate,NSURLConnectionDelegate>

// OUTLETS ------------------------------------------------------------------------------------------------------------------------------
@property (nonatomic,strong) TLTimelineVC *timeLineVC;

@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *tlPanelSwipeUpGR;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *tlPanelSwipeDownGR;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *tlPanelPanGR;

@property (weak, nonatomic) IBOutlet UIView *addToMapContainerView;
@property (weak,nonatomic) TLAddToMapList *addToMapController;

@property (strong, nonatomic) UIImage *photoHolder;


@property (weak, nonatomic) IBOutlet UIView *taskPhotoViewContainer;
@property (weak, nonatomic) IBOutlet TLTaskPhotoList *taskPhotoListView;


// Filter controls outlets
@property (weak, nonatomic) IBOutlet UIView *filterContainer;
@property (weak, nonatomic) IBOutlet UISlider *accuracySlider;
@property (weak, nonatomic) IBOutlet UILabel *accuracyValue;
@property (weak, nonatomic) IBOutlet UISlider *distanceSlider;
@property (weak, nonatomic) IBOutlet UILabel *distanceValue;
@property (weak, nonatomic) IBOutlet UISlider *radiusSlider;
@property (weak, nonatomic) IBOutlet UILabel *radiusValue;
@property (weak, nonatomic) IBOutlet UISlider *lowPassSlider;
@property (weak, nonatomic) IBOutlet UILabel *lowPassValue;


// Top Bar Buttons


// PROPERTIES ----------------------------------------------------------------------------------------------------------
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tlPanelHeightConstr;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tlPanelBottomConstr;

// - Properties for delayed checkin functionality
@property (nonatomic,strong) Reachability* internetReachability;
@property (nonatomic,strong) NSInvocationOperation *placesFetchOperation;
@property (strong,nonatomic) TLUserTask *taskToOpenPlacesListWith;

@property (strong,nonatomic) NSMutableArray<GMSPolyline*> *polylines;

// - Filter settings
@property (nonatomic) double filterAccuracyThreshold;
@property (nonatomic) double filterDistanceThreshold;
@property (nonatomic) double filterVisitDiscardRadius;
@property (nonatomic) double filterLowPassCoeffincy;

@property (nonatomic) NSRange currentDisplayTasksRange;

// Overlay for authorization VC

@property (weak,nonatomic) UIView* authorizationWhiteOverlay;

// Recieved data setting
@property (nonatomic) NSMutableData* mutableData;

@end



@implementation MainViewController {
    CLLocationCoordinate2D startDragMarkerPosition;
    
    
    NSIndexPath *indexPathDragCell;
    
    AnimationState stateTopBar;
    AnimationState stateToolBar;
    AnimationState stateSearchView;
    TLPlacesAPIType placesAPIType;
    
    TLPanelPresentationState tlPresentationState;
    NSArray<NSNumber*> *panelPresentationConstMap;
    
    CGFloat keyboardHeight;
}

#pragma mark - Initialization and lyfecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.polylines=[[NSMutableArray alloc] init];
    timelineData = @[@2];
    
    keyboardHeight=0;
    _mutableData = [[NSMutableData alloc] init];
    
    placesAPIType=[[[NSUserDefaults standardUserDefaults] objectForKey:kPlacesAPIType] integerValue];
    
    tlPresentationState=TLPanelPresentationStateCompact;
    panelPresentationConstMap=@[@(cTLPanelHeightCompact-kCompactViewHeightDif),
                                @(cTLPanelHeightCompact),
                                @(cTLPanelHeightDetailView),
                                @(cTLPanelHeightTableView)
                                ];
    
    [self setupViewController];
    
    [self.menuButton centerVerticallyWithPadding:4];
    [self.eventsButton centerVerticallyWithPadding:2];
    [self.friendsButton centerVerticallyWithPadding:2];
    
    [self.menuButton layoutSubviews];
    [self.eventsButton layoutSubviews];
    [self.friendsButton layoutSubviews];
    
    self.placesFetchOperation=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(fetchInfoForDelayed) object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    //Registering for refreshing map data
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleRefreshNotifications:) name:kGeoManagerDidSuccessfullyChecked object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleRefreshNotifications:) name:kTLTasksChangedInDB object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleRefreshCurrentRoute:) name:kGeoManagerUpdatedCurrentRoute object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFirstLocationRefreshNotification:) name:kGeoManagerUpdatedLocationFirstTimeForCurrentLaunch object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDidBecomeActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    //Registering for authorization notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidAuthorized:) name:kUserdefaultsPassedAuthorization object:nil];
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    
    [self loadFilterSettings];
    [self setupFilterViews];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.topBar drawHalfEllipseWithShapeOffsetX:25 heightShapeCap:30 backgroundColor:[UIColor whiteColor] opacity:1 flippedOver:NO];
    NSLog(@"View will appear");
    
    if(![[TLBackendManager sharedManager] isLoggedIn]){
        
        UIView *whiteOverlayView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        whiteOverlayView.backgroundColor=[UIColor colorWithWhite:1 alpha:0.4];
        self.authorizationWhiteOverlay=whiteOverlayView;
        
        [self.view insertSubview:whiteOverlayView aboveSubview:self.mapView];
        
        TLAuthorizationVC *authVC=[[self storyboard] instantiateViewControllerWithIdentifier:@"AuthVCSBID"];
        authVC.modalPresentationStyle=UIModalPresentationOverCurrentContext;
        authVC.modalTransitionStyle=UIModalTransitionStyleCrossDissolve;
        [self presentViewController:authVC animated:YES completion:nil];
    
    } else {
        
        // NSLog(@"token: %@", [[TLBackendManager sharedManager] tokenKey]);
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isExistVkData"]) {
            if ([[TLBackendManager sharedManager] vkUserID]) {
                [self getHistoryFromVK];
            }
        }
    }
}

- (void) userDidAuthorized:(NSNotification*)note{
    [self.authorizationWhiteOverlay removeFromSuperview];
    self.authorizationWhiteOverlay=nil;
    
    [self loadDataToMap];
    
    if ([[TLBackendManager sharedManager] vkUserID]) {
        [self getHistoryFromVK];
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//    [self.toolBar drawHalfEllipseWithShapeOffsetX:5 heightShapeCap:100 backgroundColor:[UIColor blackColor] opacity:0.3f flippedOver:YES];

    
//    [UIView animateWithDuration:0.5f animations:^{
//            [self.topBar drawHalfEllipseWithBackgroundColor:[UIColor whiteColor] opacity:1.0f];
//    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.layoutVSOfTopBar.priority=UILayoutPriorityDefaultHigh;
        [UIView animateWithDuration:0.7 animations:^{
            [self.view layoutIfNeeded];
        }];
    });
    
    [[TLGeoManager sharedManager] getAdressesForAllTasks];
    [self loadData];
    [self updateCheckins];
    
}

- (void) handleDidBecomeActive:(NSNotification*)note{
    [self updateCheckins];
}

#pragma mark - Setups

- (void)setupViewController {
    
    [self.mapView setDelegate:self];
    [self.mapView.settings setTiltGestures:NO];
    [self.mapView.settings setRotateGestures:NO];
    
    
    [self.searchView setCornerRadius:3.0f];
    
    [self.searchTextField setDelegate:self];
//    
//    [self.menuButton setImageSize:CGSizeMake(12, 10)];
//    [self.eventsButton setImageSize:CGSizeMake(25, 25)];
//    [self.friendsButton setImageSize:CGSizeMake(20, 20)];
    
    [self.searchOnMapButton setShadowWithColor:[UIColor blackColor] radius:8 opacity:0.7f];
    
//    UISwipeGestureRecognizer *swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpOnTopBar:)];
//    UISwipeGestureRecognizer *swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDownOnTopBar:)];
//    
//    swipeUp.direction = UISwipeGestureRecognizerDirectionUp;
//    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
//    
//    [self.topBar addGestureRecognizer:swipeUp];
//    [self.topBar addGestureRecognizer:swipeDown];
    
    
    self.timeLineVC=[[TLTimelineVC alloc] init];
    self.timeLineVC.delegate=self;;
    [self addChildViewController:self.timeLineVC];
    [self.timeLineVC.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.bottomBar insertSubview:self.timeLineVC.view atIndex:0];
    
    [[self.timeLineVC.view.topAnchor constraintEqualToAnchor:self.bottomBar.topAnchor] setActive:YES];
    [[self.timeLineVC.view.leftAnchor constraintEqualToAnchor:self.bottomBar.leftAnchor] setActive:YES];
    [[self.timeLineVC.view.rightAnchor constraintEqualToAnchor:self.bottomBar.rightAnchor] setActive:YES];
    [[self.timeLineVC.view.heightAnchor constraintEqualToConstant:430] setActive:YES];
    
    [self.timeLineVC didMoveToParentViewController:self];
}


- (void)loadData {
    [self fetchData];
    
    if([[TLBackendManager sharedManager] isLoggedIn]){
        [self loadDataToMap];
    }
    
}

- (void)fetchData {
    [self.fetchedTimeline reloadDataWithCacheName:@"TimelineTasksCache"];
}

- (void) setupFilterViews{
    self.accuracyValue.text=[NSString stringWithFormat:@"%f",self.filterAccuracyThreshold];
    self.distanceValue.text=[NSString stringWithFormat:@"%f",self.filterDistanceThreshold];
    self.radiusValue.text=[NSString stringWithFormat:@"%f",self.filterVisitDiscardRadius];
    self.lowPassValue.text=[NSString stringWithFormat:@"%f",self.filterLowPassCoeffincy];
    
    self.accuracySlider.value=self.filterAccuracyThreshold;
    self.distanceSlider.value=self.filterDistanceThreshold;
    self.radiusSlider.value=self.filterVisitDiscardRadius;
    self.lowPassSlider.value=self.filterLowPassCoeffincy;
    
}



#pragma mark - Utilites

- (void) refreshTimeline{
    
    [self fetchData];
    [self loadDataToMap];
}

- (BOOL) isPhoto:(NSString*) iconPath{
    if([iconPath containsString:@"photoic"])
        return YES;
    else return NO;
}

- (NSFetchedResultsController *)fetchedTimeline {
    if (_fetchedTimeline != nil) {
        return _fetchedTimeline;
    }
    
    _fetchedTimeline = [TLUserTask MR_fetchAllSortedBy:@"id" ascending:YES withPredicate:nil groupBy:nil delegate:self];
    return _fetchedTimeline;
}

- (void) addUserTaskWithLattitude:(CLLocationDegrees)lat longitude:(CLLocationDegrees)lon name:(NSString*)name toPast:(BOOL)toPast addToMap:(BOOL)addToMap{
}

#pragma mark - Map

- (void)loadDataToMap {
    
    [self.mapView clear];
    [self.polylines removeAllObjects];
    
    NSArray *array = [TLUserTask MR_findAll];

    for (TLUserTask *userTask in array) {
        
        [self.mapView addMarkerWithCoordinate:CLLocationCoordinate2DMake(userTask.latitude.floatValue, userTask.longitude.floatValue)
                                        image:[userTask mapMarker]
                                        title:userTask.desc
                                       zindex:userTask.id.integerValue
                                  anchorPoint:[userTask mapMarkerAnchorPoint]];
        
        //        if (userTask.route_id.integerValue > 0 && userTask.route_id > userTask.id) {
        //            TLUserTask *taskB = [TLUserTask userTaskWithId:userTask.route_id];
        //
        //            [self.mapView addRouteFrom:userTask.locationCoordinate to:taskB.locationCoordinate withZIndex:userTask.id.integerValue];
        //        }
        
        if(userTask.routeToNextTask && userTask.routeToNextTask.count>0){
            [self addPolylinesForRoute:userTask.routeToNextTask withzInd:userTask.id.intValue];
        }
    }
    
    [self refreshMapDataCurrentRouteOnly];
}

- (void) refreshMapDataCurrentRouteOnly{
    
    
    NSMutableArray *polylinesToRemove=[NSMutableArray array];
    for(GMSPolyline *polyline in self.polylines){
        if(polyline.zIndex==-1){
            [polylinesToRemove addObject:polyline];
        }
    }
    for(GMSPolyline *polyline in polylinesToRemove){
        polyline.map=nil;
        [self.polylines removeObjectsInArray:polylinesToRemove];
    }
    
    GMSPolyline *polyline = [[GMSPolyline alloc] init];
    GMSMutablePath *path = [GMSMutablePath path];
    
    NSArray<CLLocation*> *points = [[TLGeoManager sharedManager] getRoute];
    NSInteger numberOfSteps = points.count;
    
    for (NSInteger index = 0; index < numberOfSteps; index++) {
        CLLocation *location = [points objectAtIndex:index];
        CLLocationCoordinate2D coordinate = location.coordinate;
        [path addCoordinate:coordinate];
    }
    
    polyline.path = path;
    polyline.strokeColor = [UIColor whiteColor];
    polyline.strokeWidth = 7.0f;
    polyline.map = self.mapView;
    polyline.geodesic=YES;
    polyline.zIndex = (int) -1;
    [self.polylines addObject:polyline];
    
    // Copy the previous polyline, change its color, and mark it as geodesic.
    polyline = [polyline copy];
    polyline.strokeColor = [TLThemeManager mainAccentColor];
    polyline.geodesic = YES;
    polyline.strokeWidth = 4.0f;
    polyline.map = self.mapView;
    polyline.zIndex = (int) -1;
    [self.polylines addObject:polyline];
    
}

- (void) redisplayRoutesForTasksRange:(NSRange)range{
    for(GMSPolyline *polyLine in self.polylines){
        polyLine.map=nil;
    }
    [self.polylines removeAllObjects];
    
    for(NSInteger idx=range.location; idx<NSMaxRange(range); idx++ ){
        TLUserTask *userTask=[TLUserTask userTaskWithId:@(idx)];
        if(!userTask) continue;
        if(!userTask.routeToNextTask) continue;
        [self addPolylinesForRoute:userTask.routeToNextTask withzInd:idx];
    }
}



- (void) addPolylinesForRoute:(NSOrderedSet<TLLocation*>*)route withzInd:(int)zInd{
    GMSPolyline *polyline = [[GMSPolyline alloc] init];
    GMSMutablePath *path = [GMSMutablePath path];
    
    TLUserTask *task=[TLUserTask userTaskWithId:@(zInd)];
    [path addCoordinate:task.locationCoordinate];
    
    NSLog(@"%f %f",task.locationCoordinate.longitude,task.locationCoordinate.latitude);
    NSLog(@"%@",task.desc);
    
    TLUserTask *nextTask=[TLUserTask userTaskWithId:@(zInd+1)];
    
    //- Filter points
    
    NSArray<CLLocation*> *filteredArray=[self filteredRoute:route withStartTask:task andEndTask:nextTask];
    for(CLLocation *location in filteredArray){
        [path addCoordinate:location.coordinate];
    }
    
    if(nextTask){
        NSLog(@"%@",nextTask.desc);
        [path addCoordinate:nextTask.locationCoordinate];
        NSLog(@"%f %f",nextTask.locationCoordinate.longitude,nextTask.locationCoordinate.latitude);
    }
    
    polyline.path = path;
    polyline.strokeColor = [UIColor whiteColor];
    polyline.strokeWidth = 7.0f;
    polyline.map = self.mapView;
    polyline.geodesic=YES;
    polyline.zIndex = zInd;
    
    [self.polylines addObject:polyline];
    
    // Copy the previous polyline, change its color, and mark it as geodesic.
    polyline = [polyline copy];
    polyline.strokeColor = [TLThemeManager lighterGrayColor];
    polyline.geodesic = YES;
    polyline.strokeWidth = 4.0f;
    polyline.map = self.mapView;
    polyline.zIndex = zInd;
    [self.polylines addObject:polyline];
}

- (NSArray<CLLocation*>*) filteredRoute:(NSOrderedSet<TLLocation*>*)route withStartTask:(TLUserTask*) startTask andEndTask:(TLUserTask*)endTask{
    
    CLLocation *startLocation=[[CLLocation alloc]initWithLatitude:startTask.latitude.doubleValue longitude:startTask.longitude.doubleValue];
    
    CLLocation *endLocation=nil;
    if(endTask){
        endLocation=[[CLLocation alloc] initWithLatitude:endTask.latitude.doubleValue longitude:endTask.longitude.doubleValue];
    }
    
    NSMutableArray<CLLocation*> *filteredRoute=[[NSMutableArray alloc]init];
    
    
    
    for (NSInteger index = 0; index < route.count; index++) {
        TLLocation *thisLocation=route[index];
        
        CLLocationCoordinate2D thisCoordinate;
        thisCoordinate.longitude=[thisLocation.longitude doubleValue];
        thisCoordinate.latitude=[thisLocation.latitude doubleValue];
        
        //- Accuracy filter
        if([thisLocation.horizontalAccuracy doubleValue]>self.filterAccuracyThreshold) continue;
        
//        NSLog(@"%f %f time %@",thisCoordinate.longitude,thisCoordinate.latitude,thisLocation.timestamp);
        
        
        //- Spidering Filter (filter points around start and end visti within given radius)
        CLLocation *thisCLLocation=[[CLLocation alloc]initWithLatitude:thisCoordinate.latitude longitude:thisCoordinate.longitude];
        
        CLLocationDistance distanceToStartLoc=[thisCLLocation distanceFromLocation:startLocation];
        if(distanceToStartLoc<self.filterVisitDiscardRadius) continue;
        
        if(endTask){
            CLLocationDistance distanceToEndLoc=[thisCLLocation distanceFromLocation:endLocation];
            if(distanceToEndLoc<self.filterVisitDiscardRadius) continue;
        }
        
        //- Distance Filter
        CLLocation *prevCLLocation;
        if(index!=0 && filteredRoute.count!=0){
            prevCLLocation=filteredRoute[filteredRoute.count-1];
        } else{
            prevCLLocation=startLocation;
        }
        
        CLLocationDistance distanceToPrevLoc=[thisCLLocation distanceFromLocation:prevCLLocation];
        if(distanceToPrevLoc<self.filterDistanceThreshold) continue;
        

//        NSLog(@"distance from prev:%f",distanceToPrevLoc);

        
        //- Low-pass filter
        
        CLLocation *smoothedLocation;
        if(index!=0){
            CLLocationDegrees smoothedLat=[self lowPassedDegree:thisLocation.latitude.doubleValue withPreviousDegree:prevCLLocation.coordinate.latitude];
            CLLocationDegrees smoothedLon=[self lowPassedDegree:thisLocation.longitude.doubleValue withPreviousDegree:prevCLLocation.coordinate.longitude];
            
            smoothedLocation=[[CLLocation alloc] initWithLatitude:smoothedLat longitude:smoothedLon];
        } else{
            smoothedLocation=thisCLLocation;
        }
        
        
        [filteredRoute addObject:smoothedLocation];
    }
    
    return filteredRoute;

}

- (CLLocationDegrees) lowPassedDegree:(CLLocationDegrees) degree withPreviousDegree:(CLLocationDegrees) prevDegree{
    return degree*self.filterLowPassCoeffincy +prevDegree*(1-self.filterLowPassCoeffincy);
}

- (void) loadFilterSettings{
    self.filterAccuracyThreshold=[[NSUserDefaults standardUserDefaults] doubleForKey:kUserdefaultsFilterSettingsAccuracyThreshold];
    self.filterDistanceThreshold=[[NSUserDefaults standardUserDefaults] doubleForKey:kUserdefaultsFilterSettingsDistanceThreshold] ;
    self.filterVisitDiscardRadius=[[NSUserDefaults standardUserDefaults] doubleForKey:kUserdefaultsFilterSettingsSpideringfilterDistanceFromVisit];
    self.filterLowPassCoeffincy=[[NSUserDefaults standardUserDefaults] doubleForKey:kUserdefaultsFilterSettingsLowPassInnertiaCoeefincy];
    
}

- (void) saveFilterSettings{
    [[NSUserDefaults standardUserDefaults] setDouble:self.filterAccuracyThreshold forKey:kUserdefaultsFilterSettingsAccuracyThreshold];
    [[NSUserDefaults standardUserDefaults] setDouble:self.filterDistanceThreshold forKey:kUserdefaultsFilterSettingsDistanceThreshold];
    [[NSUserDefaults standardUserDefaults] setDouble:self.filterVisitDiscardRadius forKey:kUserdefaultsFilterSettingsSpideringfilterDistanceFromVisit];
    [[NSUserDefaults standardUserDefaults] setDouble:self.filterLowPassCoeffincy forKey:kUserdefaultsFilterSettingsLowPassInnertiaCoeefincy];
}


#pragma mark - GSMapView`s Delegate

- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    [[NSUserDefaults standardUserDefaults] setObject:@(position.target.latitude) forKey:kDefaultsLastPositionLatitude];
    [[NSUserDefaults standardUserDefaults] setObject:@(position.target.longitude) forKey:kDefaultsLastPositionLongitude];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    if (stateSearchView == AnimationStateComplete)
        [self actionSearchButtonTap:_searchOnMapButton];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
//    [[TLGeoManager sharedManager] makeCheckinWithLocation:[[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude] withAPI:placesAPIType withDate:0];
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker {
    startDragMarkerPosition = marker.position;
}

- (void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker {
    CLLocationCoordinate2D coorMap = mapView.camera.target;
    CLLocationCoordinate2D offsetCoorMarker = marker.position;
    CGFloat multi = 0.1f;
    
    
    offsetCoorMarker.latitude -= coorMap.latitude;
    offsetCoorMarker.longitude -= coorMap.longitude;
    
    coorMap.latitude += offsetCoorMarker.latitude * multi;
    coorMap.longitude += offsetCoorMarker.longitude * multi;
    
    [mapView animateToLocation:coorMap];
    marker.opacity = 0.85f;
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker {
    __block NSInteger zindexFirst = marker.zIndex;
    marker.opacity = 1;
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        TLUserTask *userTask = [TLUserTask userTaskWithId:@(marker.zIndex) inContext:localContext];
        
        userTask.latitude = @(marker.position.latitude);
        userTask.longitude = @(marker.position.longitude);
        
        zindexFirst = MIN(zindexFirst, userTask.route_id.integerValue);
    }];
    
    TLUserTask *userTask = [TLUserTask userTaskWithId:@(zindexFirst)];
    TLUserTask *taskB = [TLUserTask userTaskWithId:userTask.route_id];
    
    [self.mapView removePolylineWithZIndex:zindexFirst];
    [self.mapView addRouteFrom:userTask.locationCoordinate to:taskB.locationCoordinate withZIndex:userTask.id.integerValue];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    /*TLUserTask *userTask = [[_fetchedTimeline fetchedObjects] objectAtIndex:marker.zIndex];
     
     [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * localContext) {
     TLUserTask *ut = [TLUserTask MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"id = %@", userTask.id]
     inContext:localContext];
     
     ut.route_id = @1;
     }];*/
    
    return YES;
}




#pragma mark - Animation Methods

#pragma mark TLPanel

- (void) animateAppearanceOfBars{
    self.layoutVSOfTopBar.priority=UILayoutPriorityDefaultLow;
    self.tlPanelBottomConstr.constant=0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) animateDisppearanceOfBars{
    
    self.layoutVSOfTopBar.priority=UILayoutPriorityDefaultHigh;
    self.tlPanelBottomConstr.constant=-self.tlPanelHeightConstr.constant;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - VC's Delegates methods

- (void) zoomToCoordinate:(CLLocationCoordinate2D)coordinate{
    [self.mapView animateToLocation:coordinate];
}

- (void) setDetailPhoto:(NSOrderedSet<TLUserTaskPhoto*> *)photo{
    
    if(!photo || photo.count == 0){
        [UIView animateWithDuration:0.4 animations:^{
            self.taskPhotoListView.alpha=0.0;
        }
         completion:^(BOOL finished) {
             self.taskPhotoViewContainer.hidden=YES;
             [self.taskPhotoListView setImages:nil];
         }];
    } else {
        
        [self.taskPhotoListView setImages:photo];
        
        self.taskPhotoViewContainer.hidden=NO;
        [UIView animateWithDuration:0.4 animations:^{
            self.taskPhotoListView.alpha=1;
        }];
        
        
        
//        [UIView transitionWithView:self.taskPhotoViewContainer
//                          duration:0.4f
//                           options:UIViewAnimationOptionTransitionCrossDissolve
//                        animations:^{
//                            [self.taskPhotoListView setImages:photo];
//                        } completion:nil];
    }
    
}

- (void) changePanelPresentationState:(TLPanelPresentationState) state animated:(BOOL)animated{
    [self changePanelPresentationState:state animated:animated withoutLayingOut:NO];
}


- (void) changePanelPresentationState:(TLPanelPresentationState) state animated:(BOOL)animated withoutLayingOut:(BOOL)withoutLayingOut{
    if(state!=TLPanelPresentationStateDetail){
        [self setDetailPhoto:nil];
    }
    
    if(!withoutLayingOut){
        
        if(state==TLPanelPresentationStateTable && self.timeLineVC.contentState==TLPanelPresentationStateDetail){
            state=TLPanelPresentationStateDetail;
        }
        
        if(state==TLPanelPresentationStateDetail && self.timeLineVC.contentState==TLPanelPresentationStateTable) {
            state=TLPanelPresentationStateTable;
        }
        
        if(state==TLPanelPresentationStateDetail){
            self.tlPanelHeightConstr.constant=self.timeLineVC.detailPanelHeight;
        } else{
            self.tlPanelHeightConstr.constant=panelPresentationConstMap[state].floatValue;
        }
        
//        [self.view layoutIfNeeded];
   
        if(animated){
            [UIView animateWithDuration:0.4 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }

    
    if(tlPresentationState==state) return;
    tlPresentationState=state;
//    self.timeLineVC.presentationState=state;

}


//TLDetailView keyboard management
- (void) keyBoardWillShowWithHeight:(CGFloat)height{

    if(![[NSUserDefaults standardUserDefaults] boolForKey:kUserdefaultsPassedAuthorization]){
        return;
    }
    
    keyboardHeight=height;
    self.tlPanelHeightConstr.constant+=height;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void) keyBoardWillBeHidden{
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:kUserdefaultsPassedAuthorization]){
        return;
    }
    
    self.tlPanelHeightConstr.constant-=keyboardHeight;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

//- (void) switchToState:(TLPanelPresentationState)state{
//    [self changePanelPresentationState:state animated:YES];
//}
//
//- (void) switchToStateWithoutLayingOut:(TLPanelPresentationState) state{
//    if(tlPresentationState!=TLPanelPresentationStateTable && tlPresentationState!=TLPanelPresentationStateDetail){
//        return;
//    }
//    [self changePanelPresentationState:state animated:YES];
//}

- (void) panelChangedContentStateTo:(TLPanelPresentationState)state{
    if(tlPresentationState!=TLPanelPresentationStateTable && tlPresentationState!=TLPanelPresentationStateDetail && tlPresentationState!=TLPanelPresentationStateCompact){
        return;
    }
    [self changePanelPresentationState:state animated:YES];
}

- (void) showPlacesListForTask:(TLUserTask *)task{
    
//    if(self.taskPhotoImageView.image){
//        self.photoHolder=self.taskPhotoImageView.image;
//        
//        [self setDetailPhoto:nil];
//    }
    
    self.taskToOpenPlacesListWith=task;
    [self animateDisppearanceOfBars];
    [self performSegueWithIdentifier:segueToPlacesListController sender:self];
    
}

- (void) addMapToTask:(TLUserTask *)task{
    [UIView animateWithDuration:0.2 animations:^{
        self.addToMapContainerView.hidden=NO;

    }];
    [self.addToMapController addTask:task];
}

- (void) removeLastTask{
    if([self.addToMapController removerRecentTask]){
        [UIView animateWithDuration:0.2 animations:^{
            self.addToMapContainerView.hidden=YES;
            
        }];
    }
}

// TLAddToMapList confirmedAllTasks and want to dissapear
- (void) didClearedAllTasks{
    
    [UIView animateWithDuration:0.2 animations:^{
        self.addToMapContainerView.hidden=YES;
        
    }];
}

- (void) changedVisibleTasksToRange:(NSRange)range{
    self.currentDisplayTasksRange=range;
    [self redisplayRoutesForTasksRange:range];
}

//TLAddToMapList wants map to update it's markers after confirming adding to map
- (void) refreshMapData{
    [self loadDataToMap];
}

- (void) handleRefreshNotifications:(NSNotification*) note{
    [self refreshMapData];
}

- (void) handleRefreshCurrentRoute:(NSNotification*) note{
    [self refreshMapDataCurrentRouteOnly];
}

- (void) handleFirstLocationRefreshNotification:(NSNotification*)note{
    [self.mapView animateToLocation:[[TLGeoManager sharedManager] getCurrentLocation].coordinate];
}


#pragma mark - Delayed checkins methods

- (void) updateCheckins{
    
    NSArray *allDelayed=[TLDelayedCheckin MR_findAll];
    if(allDelayed.count!=0){
        
        NSArray *allNotDefined=[TLDelayedCheckin notDefinedArray];
        if(allNotDefined.count!=0){
            //есть notdefined
            
            if([self.internetReachability currentReachabilityStatus]!=NotReachable){
                
                if(!self.placesFetchOperation.isExecuting){
                    NSOperationQueue *queue=[[NSOperationQueue alloc] init];
                    self.placesFetchOperation=[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(fetchInfoForDelayed) object:nil];
                    [queue addOperation:self.placesFetchOperation];
                }
            }
            
        } else {
            //нет notdefined
            
            [self showCardsForDefined];
        }
        
        
    }
}

- (void) fetchInfoForDelayed{
    
    NSArray<TLDelayedCheckin*> *allNotDefined1=[TLDelayedCheckin notDefinedArray];
    if(allNotDefined1.count!=0){
        
        __block int i=0;
        
        __block NSMutableArray<NSNumber*> *orderingValues=[[NSMutableArray alloc] init];
        __block NSMutableArray<NSArray*> *placesArrays=[[NSMutableArray alloc] init];
        
        for(TLDelayedCheckin *ch in allNotDefined1)
            NSLog(@"%@",ch.time);
        
        //Recursive completion handler for fetching places one by one and only after finishing all, showing cards
        __block void (^completionHandler)(NSArray<TLCommonPlace *> *places);
        completionHandler=[^(NSArray<TLCommonPlace *> *places){
            
            NSArray<TLDelayedCheckin*> *allNotDefined=[TLDelayedCheckin notDefinedArray];
            
            TLDelayedCheckin *checkin=allNotDefined[i];
            NSLog(@"%@",checkin.time);
            //writing data and marking as defined
            
            [orderingValues addObject:allNotDefined[i].orderValue];
//            if(!places){
//                [placesArrays addObject:[NSNull null]];
//            } else {
                [placesArrays addObject:places];
//            }
            
            
            if(i<allNotDefined.count-1){
                
                //if we haven't reached the end fetching next place
                i++;
                [[TLPlacesAPIManager sharedManager] getPlacesForCoordinates:allNotDefined[i].locationCoordinate withAPI:placesAPIType withCompletion:completionHandler failure:^(NSError *error) {
                        NSLog(@"Google Places error:%@",error);
                }];
                //[self getGooglePlacesForCoordinates:allNotDefined[i].locationCoordinate withCompletion:completionHandler];
            } else {
                
                //reached the end

                [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                    for(int k=0;k<orderingValues.count;k++){
                        TLDelayedCheckin *checkin=[TLDelayedCheckin checkinWithOrderValue:orderingValues[k] inContext:localContext];
                        
                        
                            NSArray *placesArray=placesArrays[k];
                            
                            NSMutableOrderedSet *placesSet=[[NSMutableOrderedSet alloc] init];
                            

                            
                            if(placesArray.count!=0){
                                for(TLCommonPlace *place in placesArray){
                                    TLPlaceData *placeData=[TLPlaceData createPlaceDataWithCommonPlace:place inContext:localContext];
                                    [placesSet addObject:placeData];
                                }
                                
                                checkin.placesList=placesSet;
                            checkin.isDefined=@1;
                            
                        } else {
                            
                            [checkin MR_deleteEntityInContext:localContext];
                            
                        }
                        
                    }
                } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                    __weak typeof(self) weakSelf = self;
                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                        [weakSelf showCardsForDefined];
                    }];
                }];
                

                
            }
        } copy];
        
        [[TLPlacesAPIManager sharedManager] getPlacesForCoordinates:allNotDefined1[i].locationCoordinate
                                                            withAPI:placesAPIType
                                                     withCompletion:completionHandler
                                                            failure:^(NSError *error) {
                                                                NSLog(@"Google Places error:%@",error);
                                                            }];

    }
    
}

- (void) showCardsForDefined{
    
    if([UIApplication sharedApplication].applicationState==UIApplicationStateActive)
    {
        NSArray<TLDelayedCheckin*> *definedArrayCh=[TLDelayedCheckin definedArray];
        
        if(definedArrayCh.count!=0){
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                NSArray<TLDelayedCheckin*> *definedArray=[TLDelayedCheckin definedArrayInContext:localContext];
                
                NSInteger max=[(NSNumber*)[TLUserTask maxValueWithKey:@"id"] integerValue];
                
                if(definedArray.count==0){
                    [SVProgressHUD showErrorWithStatus:@"Error no defined checkins inside Magical Record save block"];
                }
                
                for(int i=0;i<definedArray.count;i++){
                    
                    TLUserTask *newVisit=[TLUserTask MR_createEntityInContext:localContext];
                    
                    newVisit.id=@(max+1+i);
                    
                    newVisit.latitude=definedArray[i].latitude;
                    newVisit.longitude=definedArray[i].longitude;
                    
                    TLPlaceData *firstPlaceData=[definedArray[i].placesList firstObject];
                    newVisit.desc=firstPlaceData.name;
                    newVisit.address=firstPlaceData.address;
                    newVisit.selected_place_id=firstPlaceData.id;
                    newVisit.placesList=definedArray[i].placesList;
                    
                    newVisit.icon_path=@"cafe";
                    newVisit.is_visible=@0;
                    newVisit.is_timeline=@1;
                    newVisit.is_autocheckin=@1;
                    newVisit.datetime=definedArray[i].time;
                    
                    [newVisit moveTaskToPastByTimeWithMaxInd:max+i inContext:localContext];
                    
                    if(definedArray[i].route){
                        TLUserTask *prevTask=[TLUserTask userTaskWithId:@(newVisit.id.intValue-1) inContext:localContext];
                        if(prevTask){
                            prevTask.routeToNextTask=definedArray[i].route;
                        }
                    }
                    
                    [definedArray[i] MR_deleteEntityInContext:localContext];
                }
                
            }
             completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                 if(error){
                     [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Error saving context:%@",error.localizedDescription]];

                 } else {
//                     [SVProgressHUD showSuccessWithStatus:@"Новые чекины успешно добавлены"];
                     [[NSNotificationCenter defaultCenter] postNotificationName:kTLAddedCheckins object:nil];
                     [self refreshMapData];
                 }
             }];

            [self refreshTimeline];
            
            
        }
    }
}

#pragma mark - Internet Connection

- (void) reachabilityChanged:(NSNotification *)note{
    
    [self updateCheckins];
}

#pragma mark - NSURLConnectionDelegate
-(void) connection:(NSURLConnection *) connection didReceiveResponse:(NSURLResponse *)response
{
    [_mutableData setLength:0];
}

-(void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_mutableData appendData:data];
}

-(void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error: %@", error.localizedDescription);
    return;
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:_mutableData options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"%@", responseDic);
    if ([NSJSONSerialization isValidJSONObject:responseDic])
    {
        NSMutableDictionary *data = [responseDic objectForKey:@"data"];
        NSMutableArray *history = [data objectForKey:@"history"];
        
        //Костыль для последовательной обработки данных, рекурсивный completion блок, т.к. мы не можем запустить синхронно makeCheckin, для этого нужен другой метод.
        __block int i=0;
        __block void (^recursiveCompletionBlock)()=^{
            NSDictionary *currentHistory = history[i];
            
            NSString *time = currentHistory[@"post_date"];
            int elapsedTime = [time intValue];
            
            NSDictionary *coordinates = currentHistory[@"post_coords"];
            double x = [[coordinates objectForKey:@"x"] doubleValue];
            double y = [[coordinates objectForKey:@"y"] doubleValue];
            
            NSString *photoURL = currentHistory[@"photo_src_big"];
            
            NSNumber *likesCount = currentHistory[@"post_likes_count"];
            NSNumber *commentsCount = currentHistory[@"post_comments_count"];
            
//            NSString *postText = currentHistory[@"post_text"];
            
            NSURL *url = [NSURL URLWithString:photoURL];
            [[SDWebImageManager sharedManager] loadImageWithURL:url options:0 progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
                
                
                if(i<history.count-1){ //Увеличиваем i и запускаем блок по новой, как только чекин отметится
                    i++;
                    [[TLGeoManager sharedManager] makeCheckinWithLocation:[[CLLocation alloc] initWithLatitude:x longitude:y] withAPI:placesAPIType completionBlock:recursiveCompletionBlock failureBlock:^{
                        
                    } withDate:elapsedTime photo:image];
                } else { //Достигли конца, апдейтим
                    
                    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isExistVkData"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self updateCheckins];
                }
            }];
        };
        
        //Запускаем наш блок
        recursiveCompletionBlock();
    }
    
}

#pragma mark - Helper functions
- (void) getHistoryFromVK {
    
    NSString *tokenKey = [[TLBackendManager sharedManager] vkToken];
    if (tokenKey == NULL) {
        return;
    }
    NSLog(@"token key: %@", tokenKey);
    NSString *post = [NSString stringWithFormat:@"token_key=%@", tokenKey];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"http://lifein.western-studio.ru/user-get-history"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wdeprecated-declarations"
    #pragma clang diagnostic ignored "-Wunused-value"
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    #pragma clang diagnostic pop
    
}

#pragma mark - UITextField`s Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self actionSearchButtonTap:_searchOnMapButton];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.searchTextField) {
        [textField resignFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - IBAction's
- (IBAction)logoTap:(id)sender {

    UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"Тестовые функции" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    BOOL routeTrackingEnabled=[[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsRouteTrackingEnabled] boolValue];

    
    UIAlertAction *actionTracking=[UIAlertAction actionWithTitle:routeTrackingEnabled? @"Отключить трекинг" : @"Включить трекинг" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if(routeTrackingEnabled){
            [[TLGeoManager sharedManager] stopRouteTracking];
        } else {
            [[TLGeoManager sharedManager] enableRouteTracking];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@(!routeTrackingEnabled) forKey:kSettingsRouteTrackingEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }];
    
    UIAlertAction *actionFilters=[UIAlertAction actionWithTitle:self.filterContainer.alpha==0? @"Показать настройки фильтров" : @"Скрыть настройки фильтров" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        self.filterContainer.alpha= (1-self.filterContainer.alpha);
        
    }];
    
    BOOL wakeHack=[[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsAppWakingHackIsEnabled] boolValue];
    UIAlertAction *actionHack=[UIAlertAction actionWithTitle:wakeHack? @"Отключить \"хак\" для возобновления трекинга" : @"Включить \"хак\" для возобновления трекинга" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@(!wakeHack) forKey:kDefaultsAppWakingHackIsEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }];
    
    BOOL loggingEnabled=[[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsLoggingEnabled] boolValue];
    UIAlertAction *actionLogging=[UIAlertAction actionWithTitle:loggingEnabled? @"Отключить логи" : @"Включить логи" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@(!loggingEnabled) forKey:kSettingsLoggingEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }];
    
    UIAlertAction *actionLogout=[UIAlertAction actionWithTitle:@"Выйти из пользователя перед следующим запуском"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[TLBackendManager sharedManager]logoutInBackgroundWithBlock:^(NSError *error) {
            
        }];
        
    }];

    
    UIAlertAction *action=[UIAlertAction actionWithTitle:@"Очистить базу" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [[DatabaseAPI sharedAPI] cleanUpDatabase];
        [[TLGeoManager sharedManager] cancelCurrentRouteTracking];
        [[NSNotificationCenter defaultCenter] postNotificationName:kTLTasksChangedInDB object:nil];
        
    }];
    
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:actionTracking];
    [alert addAction:actionLogging];
    [alert addAction:actionHack];
    [alert addAction:actionFilters];
    [alert addAction:action];
    [alert addAction:actionLogout];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)actionSearchButtonTap:(id)sender {
    if (stateToolBar == AnimationStateActive)
        return;
    
    if (stateSearchView == AnimationStateStop) {
        stateSearchView = AnimationStateActive;
        _searchView.alpha = 0;
        _searchView.hidden = NO;
        

        
        [UIView animateWithDuration:0.4f animations:^{
            [self.view layoutIfNeeded];
            
            _searchView.alpha = 1.0f;
        } completion:^(BOOL finished) {
            stateSearchView = AnimationStateComplete;
        }];
        
        [_searchTextField becomeFirstResponder];
    }
    else if (stateSearchView == AnimationStateComplete) {
       
        [UIView animateWithDuration:0.4f animations:^{
            [self.view layoutIfNeeded];
            
            _searchView.alpha = 0.0f;
        } completion:^(BOOL finished) {
            _searchView.hidden = YES;
            stateSearchView = AnimationStateStop;
        }];
        
        [_searchTextField resignFirstResponder];
    }
}

- (IBAction)actionOpenTopBar:(id)sender {
    if (stateTopBar == AnimationStateActive)
        return;
    
    CGFloat offsetTopBar_x = _topBar.frame.size.height - _topBar.heightEllipseCap - 2 * statusBarHeight - 10;
    CGFloat offsetToolBar_x = _toolBar.heightEllipseCap + 15;
    UIViewAnimationOptions viewAnimationOptions = UIViewAnimationOptionCurveEaseInOut;
    
    if (stateTopBar == AnimationStateStop) {
        stateTopBar = AnimationStateActive;
        
        NSTimeInterval interval = 0.45f;
        
        _layoutVSOfTopBar.priority = UILayoutPriorityDefaultHigh;
        //_layoutVSOfToolBar.priority = UILayoutPriorityDefaultHigh;
        
        [UIView animateWithDuration:interval
                              delay:0.0f
                            options:viewAnimationOptions
                         animations:^{
                             /*CGPoint centerTopBar = _topBar.center;
                              CGPoint centerToolBar = _toolBar.center;
                              
                              centerTopBar.y -= offsetTopBar_x;
                              centerToolBar.y -= offsetToolBar_x;
                              
                              _topBar.center = centerTopBar;
                              _toolBar.center = centerToolBar;*/
                             
                             //_logoImageView.alpha = 0.0f;
                             
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             stateTopBar = AnimationStateComplete;
                         }];
    }
    else if (stateTopBar == AnimationStateComplete) {
        stateTopBar = AnimationStateActive;
        
        NSTimeInterval interval = 0.45f;
        
        _layoutVSOfTopBar.priority = UILayoutPriorityDefaultLow;
        //_layoutVSOfToolBar.priority = UILayoutPriorityDefaultLow;
        
        [UIView animateWithDuration:interval
                              delay:0.0f
                            options:viewAnimationOptions
                         animations:^{
                             /*CGPoint centerTopBar = _topBar.center;
                              CGPoint centerToolBar = _toolBar.center;
                              
                              centerTopBar.y += offsetTopBar_x;
                              centerToolBar.y += offsetToolBar_x;
                              
                              _topBar.center = centerTopBar;
                              _toolBar.center = centerToolBar;*/
                             
                             //_logoImageView.alpha = 1.0f;
                             
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             stateTopBar = AnimationStateStop;
                         }];
        
        //[self.topBar slideBottomWithDuration:interval Offset:CGPointMake(0, - self.topBar.heightEllipseCap) completion:^{}];
        //[self.toolBar slideBottomWithDuration:interval Offset:CGPointMake(0, - self.toolBar.frame.size.height) completion:^{ stateToolBar = AnimationStateStop; }];
    }
}
//
//- (IBAction)actionOpenToolBar:(id)sender {
//    if (stateToolBar == AnimationStateActive)
//        return;
//    
//    CGFloat offsetTopBar_x = _topBar.frame.size.height - _topBar.heightEllipseCap - 2 * statusBarHeight - 10;
//    CGFloat offsetToolBar_x = _toolBar.heightEllipseCap + 15;
//    UIViewAnimationOptions viewAnimationOptions = UIViewAnimationOptionCurveEaseInOut;
//    
//    if (stateToolBar == AnimationStateStop) {
//        stateToolBar = AnimationStateActive;
//        
//        NSTimeInterval interval = 0.45f;
//        
//        //_layoutVSOfTopBar.priority = UILayoutPriorityDefaultHigh;
//       
//        [UIView animateWithDuration:interval
//                              delay:0.0f
//                            options:viewAnimationOptions
//                         animations:^{
//                             /*CGPoint centerTopBar = _topBar.center;
//                             CGPoint centerToolBar = _toolBar.center;
//                             
//                             centerTopBar.y -= offsetTopBar_x;
//                             centerToolBar.y -= offsetToolBar_x;
//                             
//                             _topBar.center = centerTopBar;
//                             _toolBar.center = centerToolBar;*/
//                             
//                             //_logoImageView.alpha = 0.0f;
//                             
//                             [self.view layoutIfNeeded];
//                         }
//                         completion:^(BOOL finished) {
//                             stateToolBar = AnimationStateComplete;
//                         }];
//    }
//    else if (stateToolBar == AnimationStateComplete) {
//        stateToolBar = AnimationStateActive;
//        
//        NSTimeInterval interval = 0.45f;
//        
//        //_layoutVSOfTopBar.priority = UILayoutPriorityDefaultLow;
//      [UIView animateWithDuration:interval
//                              delay:0.0f
//                            options:viewAnimationOptions
//                         animations:^{
//                             /*CGPoint centerTopBar = _topBar.center;
//                             CGPoint centerToolBar = _toolBar.center;
//                             
//                             centerTopBar.y += offsetTopBar_x;
//                             centerToolBar.y += offsetToolBar_x;
//                             
//                             _topBar.center = centerTopBar;
//                             _toolBar.center = centerToolBar;*/
//                             
//                             //_logoImageView.alpha = 1.0f;
//                             
//                             [self.view layoutIfNeeded];
//                         }
//                         completion:^(BOOL finished) {
//                             stateToolBar = AnimationStateStop;
//                         }];
//        
//        //[self.topBar slideBottomWithDuration:interval Offset:CGPointMake(0, - self.topBar.heightEllipseCap) completion:^{}];
//        //[self.toolBar slideBottomWithDuration:interval Offset:CGPointMake(0, - self.toolBar.frame.size.height) completion:^{ stateToolBar = AnimationStateStop; }];
//    }
//}

- (IBAction)swipeUpOnTopBar:(UISwipeGestureRecognizer *)recognizer {
//    if (stateTopBar == AnimationStateStop) {
//        [self actionOpenTopBar:nil];
//    }
    self.layoutVSOfTopBar.priority=UILayoutPriorityDefaultHigh;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}
//
- (IBAction)swipeDownOnTopBar:(UISwipeGestureRecognizer *)recognizer {
//    if (stateTopBar == AnimationStateComplete) {
//        [self actionOpenTopBar:nil];
//    }
    self.layoutVSOfTopBar.priority=UILayoutPriorityDefaultLow;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (stateSearchView == AnimationStateComplete)
        [self actionSearchButtonTap:_searchOnMapButton];
}


- (NSTimeInterval) durationForFinishState:(TLPanelPresentationState)state andVelocity:(CGFloat)velocity{
    
    CGFloat vertical = self.bottomBar.frame.origin.y - ([UIScreen mainScreen].bounds.size.height- panelPresentationConstMap[state].floatValue);
    CGFloat duration = MIN(ABS(vertical / velocity), 0.3);
    return duration;
}

- (IBAction)handlePanOnTLPanel:(UIPanGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
  
        // If it is over, we check the velocity of the drag
        // to see if we want to finish dragging it up or down
        CGPoint origin = [gestureRecognizer velocityInView:self.view];
        CGFloat velocity = origin.y;
        
        TLPanelPresentationState finishState=TLPanelPresentationStateTable;
        // If the y value is negative, we are moving up
        if (velocity < 0) {
            
            if(tlPresentationState==TLPanelPresentationStateDetail) return;
            
            for(NSInteger i=0 ; i<4 ; i++){
                CGFloat maxyforstate=[UIScreen mainScreen].bounds.size.height - panelPresentationConstMap[i].floatValue;
                if(self.bottomBar.frame.origin.y>maxyforstate){
                    finishState=i;
                    break;
                }
            }

            
            [self.timeLineVC exitEditingMode];
        }
        else {
            // Otherwise, moving down
            
            for(NSInteger i=3 ; i>=0 ; i--){
                CGFloat maxyforstate=[UIScreen mainScreen].bounds.size.height - panelPresentationConstMap[i].floatValue;
                if(self.bottomBar.frame.origin.y<=maxyforstate){
                    
                    finishState=i;
                    break;
                }
            }
            
            if(finishState==TLPanelPresentationStateDetail && self.timeLineVC.contentState==TLPanelPresentationStateTable){
                finishState=TLPanelPresentationStateCompact;
            }
            
        }
        
        NSTimeInterval duration=[self durationForFinishState:finishState andVelocity:velocity];
        
        [self.timeLineVC finishInteractiveHidingAndHide:(finishState==TLPanelPresentationStateExtraCompact) withDuration:duration];

        [UIView animateWithDuration:duration
                              delay:0.0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             [self changePanelPresentationState:finishState animated:NO];
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
        
        
    }
    else if (gestureRecognizer.state==UIGestureRecognizerStateChanged) {
        // Keep track of where we are
        CGPoint origin = [gestureRecognizer locationInView:self.view];
        
        // As long as we aren't going above the top of the view, have it follow the drag
        if (CGRectContainsPoint(self.view.frame, origin)) {
            // Only allow dragging to a certain point. Don't let drag further down.
            CGPoint translatedPoint = [gestureRecognizer translationInView:self.view];
            
            
            //-If we pulling higher in detail state
            if(tlPresentationState==TLPanelPresentationStateDetail && translatedPoint.y<0 ) {
                self.tlPanelHeightConstr.constant=self.timeLineVC.detailPanelHeight;
                return;
            }
            
            
            //-If we pulling higher in table state
//            CGPoint location=[gestureRecognizer locationOfTouch:0 inView:self.view];
            if(tlPresentationState==TLPanelPresentationStateTable && translatedPoint.y<0 ) {
                self.tlPanelHeightConstr.constant=panelPresentationConstMap[TLPanelPresentationStateTable].floatValue;
                return;
            }
            
            //Pulling lower then in compact view
            CGFloat constForCurrentState=panelPresentationConstMap[tlPresentationState].floatValue;
            if(tlPresentationState==TLPanelPresentationStateDetail){
                constForCurrentState=self.timeLineVC.detailPanelHeight;
            }
            
            if(translatedPoint.y>(constForCurrentState-panelPresentationConstMap[TLPanelPresentationStateCompact].floatValue)){
                CGFloat percent=(translatedPoint.y-(constForCurrentState-panelPresentationConstMap[TLPanelPresentationStateCompact].floatValue))/kCompactViewHeightDif;

                if(percent>1){
                    percent=1;
                }
                
                
                [self.timeLineVC setInteractiveHidingPercent:percent];
                
                self.tlPanelHeightConstr.constant=panelPresentationConstMap[TLPanelPresentationStateCompact].floatValue-percent*kCompactViewHeightDif;

                
                return;
            } else{
                [self.timeLineVC setInteractiveHidingPercent:0];
            }
            
            if(tlPresentationState==TLPanelPresentationStateCompact || tlPresentationState==TLPanelPresentationStateExtraCompact){
                CGFloat allowedTranslation;
                if(self.timeLineVC.contentState==TLPanelPresentationStateTable){
                    allowedTranslation=panelPresentationConstMap[TLPanelPresentationStateTable].floatValue-panelPresentationConstMap[tlPresentationState].floatValue;
                } else{
                    allowedTranslation=self.timeLineVC.detailPanelHeight- panelPresentationConstMap[tlPresentationState].floatValue;
                }
                
                if(translatedPoint.y<-allowedTranslation){
                    
                    if(self.timeLineVC.contentState==TLPanelPresentationStateTable){
                        self.tlPanelHeightConstr.constant=panelPresentationConstMap[TLPanelPresentationStateTable].floatValue;
                    } else{
                        self.tlPanelHeightConstr.constant=self.timeLineVC.detailPanelHeight;
                    }
                    
                    return;
                }
            }
                
            self.tlPanelHeightConstr.constant=panelPresentationConstMap[tlPresentationState].floatValue-translatedPoint.y;
        }
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        // Now, we are dragging
        
        [self setDetailPhoto:nil];
        if(tlPresentationState==TLPanelPresentationStateCompact){
//            [self.timeLineVC changeToListView];
        }
        
        [self.timeLineVC startInteractiveHiding];
    }
}

#pragma mark Filter Actions

- (IBAction)accuracySliderValueChanged:(id)sender {
    UISlider *slider=(UISlider*)sender;
    self.filterAccuracyThreshold=slider.value;
    self.accuracyValue.text=[NSString stringWithFormat:@"%f",self.filterAccuracyThreshold];
    [self redisplayRoutesForTasksRange:self.currentDisplayTasksRange];
    [self saveFilterSettings];
}

- (IBAction)distanceSliderValueChanged:(id)sender {
    UISlider *slider=(UISlider*)sender;
    self.filterDistanceThreshold=slider.value;
    self.distanceValue.text=[NSString stringWithFormat:@"%f",self.filterDistanceThreshold];
    [self redisplayRoutesForTasksRange:self.currentDisplayTasksRange];
    [self saveFilterSettings];
}

- (IBAction)spiderRadiusSliderValueChanged:(id)sender {
    UISlider *slider=(UISlider*)sender;
    self.filterVisitDiscardRadius=slider.value;
    self.radiusValue.text=[NSString stringWithFormat:@"%f",self.filterVisitDiscardRadius];
    [self redisplayRoutesForTasksRange:self.currentDisplayTasksRange];
    [self saveFilterSettings];
}

- (IBAction)lowPassSliderValueChanged:(id)sender {
    UISlider *slider=(UISlider*)sender;
    self.filterLowPassCoeffincy=slider.value;
    self.lowPassValue.text=[NSString stringWithFormat:@"%f",self.filterLowPassCoeffincy];
    [self redisplayRoutesForTasksRange:self.currentDisplayTasksRange];
    [self saveFilterSettings];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqual:segueToPlacesListController]){
        TLPlacesListVC *placesListVC=[segue destinationViewController];
        placesListVC.delegate=self;
        [placesListVC setCurrentTask:self.taskToOpenPlacesListWith];
        
    } else if ([segue.identifier isEqual:segueToAddToMapController]){
        
        self.addToMapController=(TLAddToMapList*)segue.destinationViewController;
        self.addToMapController.delegate=self;
    }
}

- (IBAction) prepareForUnwind:(UIStoryboardSegue *)segue{
    
    [self animateAppearanceOfBars];
    
//    if(self.photoHolder){
//        [self setDetailPhoto:self.photoHolder];
//        self.photoHolder=nil;
//    }
    
}

@end

//
//  TLDelayedCheckin.m
//  TimeLine
//
//  Created by Evgenii Oborin on 21.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLDelayedCheckin.h"

@implementation TLDelayedCheckin

+ (instancetype)MR_createEntityInContext:(NSManagedObjectContext *)context {
    TLDelayedCheckin *checkin = [super MR_createEntityInContext:context];
    

    checkin.orderValue = @([[self maxValueWithKey:@"orderValue"] integerValue] + 1);    
    checkin.time=[NSDate date];
    return checkin;
}

- (CLLocationCoordinate2D)locationCoordinate {
    return CLLocationCoordinate2DMake(self.latitude.floatValue, self.longitude.floatValue);
}

+ (id)maxValueWithKey:(NSString *)key {
    TLDelayedCheckin *dc = [TLDelayedCheckin MR_findFirstOrderedByAttribute:key ascending:NO];
    
    return (dc != nil) ? [dc valueForKey:key] : @0;
}

@end

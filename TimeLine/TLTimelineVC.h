
//  TLTimelineVC.h
//  TimeLine
//
//  Created by Evgenii Oborin on 28.09.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>


#define kCompactViewHeightDif 47.0
#define kCompactViewCellHeightDif 47.0
#define kCompactViewCollectionViewTopConstraintDif 0

@class GMSMapView;
@class TLUserTaskPhoto;

typedef enum : NSUInteger {
    TLPanelPresentationStateExtraCompact,
    TLPanelPresentationStateCompact,
    TLPanelPresentationStateDetail,
    TLPanelPresentationStateTable,
} TLPanelPresentationState;


@protocol TLTimelineVCDelegate <NSObject>

//- (void) switchToState:(TLPanelPresentationState) state;
//- (void) switchToStateWithoutLayingOut:(TLPanelPresentationState) state;

- (void) panelChangedContentStateTo:(TLPanelPresentationState) state;

- (void) showPlacesListForTask:(TLUserTask*) task;
- (void) addMapToTask:(TLUserTask*) task;
- (void) removeLastTask;

- (void) keyBoardWillShowWithHeight:(CGFloat) height;
- (void) keyBoardWillBeHidden;

- (void) setDetailPhoto:(NSOrderedSet<TLUserTaskPhoto*>*) photo;

- (void) zoomToCoordinate:(CLLocationCoordinate2D) coordinate;

- (void) changedVisibleTasksToRange:(NSRange)range;

@end

@interface TLTimelineVC : UIViewController

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *stripView;

@property (weak,nonatomic) id<TLTimelineVCDelegate> delegate;
@property (weak,nonatomic) GMSMapView* mapView;

@property (nonatomic) TLPanelPresentationState contentState;
@property (nonatomic) CGFloat detailPanelHeight;

- (void) changeToListView;
- (void) exitEditingMode;

//For extra-compact View
- (void) startInteractiveHiding;
- (void) setInteractiveHidingPercent:(CGFloat) percent;
- (void) finishInteractiveHidingAndHide:(BOOL)hide withDuration:(CGFloat)duration;

@end

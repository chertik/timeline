//
//  TLTaskPhotoList.h
//  TimeLine7
//
//  Created by EVGENY Oborin on 14.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLUserTaskPhoto;

@interface TLTaskPhotoList : UIView

- (void) setImages: (NSOrderedSet<TLUserTaskPhoto*>*) images;

@end

//
//  TLLocation+CoreDataProperties.h
//  TimeLine
//
//  Created by Evgenii Oborin on 03.04.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLLocation+CoreDataClass.h"

@class TLDelayedCheckin;

NS_ASSUME_NONNULL_BEGIN

@interface TLLocation (CoreDataProperties)

+ (NSFetchRequest<TLLocation *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *latitude;
@property (nullable, nonatomic, copy) NSNumber *longitude;
@property (nullable, nonatomic, copy) NSDate *timestamp;
@property (nullable, nonatomic, copy) NSNumber *horizontalAccuracy;
@property (nullable, nonatomic, retain) TLDelayedCheckin *delayedCheckin;
@property (nullable, nonatomic, retain) TLUserTask *userTask;

@end

NS_ASSUME_NONNULL_END

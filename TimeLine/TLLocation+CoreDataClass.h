//
//  TLLocation+CoreDataClass.h
//  TimeLine
//
//  Created by Evgenii Oborin on 13.03.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TLUserTask;

NS_ASSUME_NONNULL_BEGIN

@interface TLLocation : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TLLocation+CoreDataProperties.h"

//
//  ASImageButtonView.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 05/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LayoutMode) {
    LayoutModeNone,
    LayoutModeFullImage,
    LayoutModeImageAboveTitle
};

typedef enum LayoutMode LayoutMode;

@interface ASImageButtonView : UIButton

@property LayoutMode layoutMode;
@property (nonatomic, assign) CGSize imageSize;
@property (nonatomic, assign) UIEdgeInsets contentOffset;

@end
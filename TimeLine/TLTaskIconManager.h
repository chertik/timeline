//
//  TLTaskIconManager.h
//  TimeLine7
//
//  Created by EVGENY Oborin on 13.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLTaskIconManager : NSObject

+ (instancetype) sharedManager;

- (UIImage*) sharedImageForIconPath:(NSString*)iconPath size:(CGSize)size square:(BOOL)square border:(BOOL) border shadow:(BOOL)shadow isPhoto:(BOOL)isPhoto isPink:(BOOL)isPink;

@end

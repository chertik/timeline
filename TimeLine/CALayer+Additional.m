//
//  CALayer+Additional.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 10/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "CALayer+Additional.h"

@implementation CALayer (Additional)

+ (instancetype)squareLayerWithBorderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color cornerRadius:(CGFloat)radius {
    CALayer *layer = [[CALayer alloc]init];
    
    layer.borderWidth = borderWidth;
    layer.borderColor = color.CGColor;
    layer.cornerRadius = radius;
    
    return layer;
}

+ (instancetype)rhombusLayerWithBorderWidth:(CGFloat)borderWidth borderColor:(UIColor *)color cornerRadius:(CGFloat)radius {
    CALayer *layer = [self squareLayerWithBorderWidth:borderWidth borderColor:color cornerRadius:radius];
//    CGAffineTransform rotateTransform = CGAffineTransformMakeRotation(M_PI / 4.0);
    //[layer setAffineTransform:rotateTransform];
    
    return layer;
}

@end

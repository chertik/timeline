//
//  UIButton+verticalLayout.h
//  TimeLine
//
//  Created by Evgenii Oborin on 13.04.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (verticalLayout)

- (void)centerVerticallyWithPadding:(float)padding;
- (void)centerVertically;



@end

//
//  NSFetchedResultsController+Additional.m
//  CurrencyItsEasy
//
//  Created by Alexey on 3/8/16.
//  Copyright © 2016 inspirelab. All rights reserved.
//

#import "NSFetchedResultsController+Additional.h"

@implementation NSFetchedResultsController (Additional)

- (BOOL)reloadDataWithCacheName:(NSString *)cacheName {
    BOOL success = YES;
    
    [NSFetchedResultsController deleteCacheWithName:cacheName];
    
    NSError *errorFetch;
    if (![self performFetch:&errorFetch]) {
        NSLog(@"Unresolved error %@, %@", errorFetch, [errorFetch userInfo]);
        success = NO;
    }
    
    return success;
}

@end

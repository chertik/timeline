//
//  TLUserTaskPhoto+CoreDataProperties.h
//  TimeLine7
//
//  Created by EVGENY Oborin on 13.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//
//

#import "TLUserTaskPhoto+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@class TLUserTask;

@interface TLUserTaskPhoto (CoreDataProperties)

+ (NSFetchRequest<TLUserTaskPhoto *> *)fetchRequest;

@property (nullable, nonatomic, retain) NSObject *image;
@property (nullable, nonatomic, retain) NSObject *asset;
@property (nullable, nonatomic, retain) NSObject *hiresImage;
@property (nullable, nonatomic, retain) TLUserTask *userTask;

@end

NS_ASSUME_NONNULL_END

//
//  NSLayoutConstraint+fillInView.m
//  TimeLine
//
//  Created by Evgenii Oborin on 23.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "NSLayoutConstraint+fillInView.h"

@implementation NSLayoutConstraint (fillInView)

- (void) fillView:(UIView*) view inVIew:(UIView*)fill{
    [view.topAnchor constraintEqualToAnchor:fill.topAnchor].active=YES;
    [view.leftAnchor constraintEqualToAnchor:fill.leftAnchor].active=YES;
    [view.rightAnchor constraintEqualToAnchor:fill.rightAnchor].active=YES;
    [view.bottomAnchor constraintEqualToAnchor:fill.bottomAnchor].active=YES;
    
}

@end

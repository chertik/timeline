//
//  TLPhotoImporter.h
//  TimeLine7
//
//  Created by EVGENY Oborin on 11.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kDefaultsKeyLastImportedPhotoDate @"kDefaultsKeyLastImportedPhotoDate"

@interface TLPhotoImporter : NSObject

+ (instancetype) sharedManager;

- (void) checkForNewPhotosAndImport;

@end

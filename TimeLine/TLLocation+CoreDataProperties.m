//
//  TLLocation+CoreDataProperties.m
//  TimeLine
//
//  Created by Evgenii Oborin on 03.04.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLLocation+CoreDataProperties.h"

@implementation TLLocation (CoreDataProperties)

+ (NSFetchRequest<TLLocation *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TLLocation"];
}

@dynamic latitude;
@dynamic longitude;
@dynamic timestamp;
@dynamic horizontalAccuracy;
@dynamic delayedCheckin;
@dynamic userTask;

@end

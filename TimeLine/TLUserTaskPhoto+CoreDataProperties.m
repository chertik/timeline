//
//  TLUserTaskPhoto+CoreDataProperties.m
//  TimeLine7
//
//  Created by EVGENY Oborin on 13.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//
//

#import "TLUserTaskPhoto+CoreDataProperties.h"

@implementation TLUserTaskPhoto (CoreDataProperties)

+ (NSFetchRequest<TLUserTaskPhoto *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TLUserTaskPhoto"];
}

@dynamic image;
@dynamic asset;
@dynamic hiresImage;
@dynamic userTask;

@end

//
//  UIAuthButton.m
//  TimeLine
//
//  Created by Evgenii Oborin on 29.05.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "UIAuthButton.h"
#import "Constants.h"


@implementation UIAuthButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self loadFromNib];
        //        [self setInitialValues];
        [self setupViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadFromNib];
        
        [self setupViews];
    }
    return self;
}

- (void) loadFromNib{
    UIView *view=[[[NSBundle bundleForClass:[self class]] loadNibNamed:@"UIAuthButton" owner:self options:nil] firstObject];
    [self.widthAnchor constraintEqualToConstant:58].active=YES;
    [self.heightAnchor constraintEqualToConstant:78].active=YES;
    view.frame=self.bounds;

    [self addSubview:view];
    
    
    
}

- (void) setupViews{
    self.borderView.layer.cornerRadius=4;
    self.borderView.layer.borderWidth=2;
    self.borderView.layer.borderColor=dThemeColorGray.CGColor;
}






@end

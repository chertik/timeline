//
//  TLPlaceData+CoreDataProperties.h
//  TimeLine
//
//  Created by Evgenii Oborin on 27.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLPlaceData+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TLPlaceData (CoreDataProperties)

+ (NSFetchRequest<TLPlaceData *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *id;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSNumber *distance;
@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSNumber *rating;

@end

NS_ASSUME_NONNULL_END

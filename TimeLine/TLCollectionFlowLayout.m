//
//  TLCollectionFlowLayout.m
//  TimeLine
//
//  Created by Evgenii Oborin on 12.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLCollectionFlowLayout.h"

@implementation TLCollectionFlowLayout

- (UICollectionViewLayoutAttributes*) layoutAttributesForInteractivelyMovingItemAtIndexPath:(NSIndexPath *)indexPath withTargetPosition:(CGPoint)position
{
    UICollectionViewLayoutAttributes *attributes = [super layoutAttributesForInteractivelyMovingItemAtIndexPath:indexPath withTargetPosition:position];
    
    attributes.alpha=0.7;
//    attributes.transform=CGAffineTransformMakeScale(1.3, 1.3);
    
    return attributes;
}

@end

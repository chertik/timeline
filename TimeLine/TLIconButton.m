//
//  TLIconButton.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 11/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLIconButton.h"
#import "TLThemeManager.h"

#define kGrayColor [UIColor colorWithRed:83/255.0f green:87/255.0f blue:89/255.0f alpha:1]
#define kWhiteColor [UIColor whiteColor]

@implementation TLIconButton

@synthesize style = _style;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setStyle:TLIconButtonStyleGray];
        [self setShape:ASShapeSquare];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setStyle:TLIconButtonStyleGray];
        [self setShape:ASShapeSquare];
    }
    
    return self;
}

- (void)changeToNeedColor {
    switch (_style) {
        case TLIconButtonStyleColor: {
            if(self.isPhoto){
                self.imageView.tintColor = nil;
            } else
                self.imageView.tintColor=kGrayColor;
            break;
        }

        case TLIconButtonStyleGray: {
            [self setColor:kGrayColor];
            
            if (self.type == ASShapeButtonTypeBorder)
                self.imageView.tintColor = kGrayColor;
            else
                self.imageView.tintColor = kWhiteColor;
            
            break;
        }
            
        case TLIconButtonStyleWhite: {
            [self setColor:kWhiteColor];
            
            if (self.type == ASShapeButtonTypeBorder)
                self.imageView.tintColor = kWhiteColor;
            else
                self.imageView.tintColor = kGrayColor;
            
            break;
        }
        case TLIconButtonStylePink :{
            if(self.isPhoto){
                self.imageView.tintColor = nil;
            } else
                self.imageView.tintColor=dThemeColorRed;
            break;
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - Setters

- (void)setStyle:(TLIconButtonStyle)style {
    _style = style;
    
    [self changeToNeedColor];
}

- (void)setShape:(ASShape)shape {
    [super setShape:shape];
    
    [self changeToNeedColor];
}

- (void)setType:(ASShapeButtonType)type {
    [super setType:type];
    
    [self changeToNeedColor];
}

@end

//
//  TLUserTask+CoreDataProperties.m
//  TimeLine7
//
//  Created by EVGENY Oborin on 13.11.2017.
//  Copyright © 2017 InspireLab. All rights reserved.
//
//

#import "TLUserTask+CoreDataProperties.h"

@implementation TLUserTask (CoreDataProperties)

+ (NSFetchRequest<TLUserTask *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TLUserTask"];
}

@dynamic address;
@dynamic datetime;
@dynamic dayName;
@dynamic desc;
@dynamic icon_path;
@dynamic id;
@dynamic is_autocheckin;
@dynamic is_checked;
@dynamic is_current_location_holder;
@dynamic is_past;
@dynamic is_timeline;
@dynamic is_visible;
@dynamic latitude;
@dynamic longitude;
@dynamic photo_path;
@dynamic polyline_id;
@dynamic route_id;
@dynamic selected_place_id;
@dynamic z_index;
@dynamic placesList;
@dynamic routeToNextTask;
@dynamic taskPhoto;

@end

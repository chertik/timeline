//
//  UIAuthButton.h
//  TimeLine
//
//  Created by Evgenii Oborin on 29.05.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAuthButton : UIView

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIImageView *checkMark;
@property (weak, nonatomic) IBOutlet UIView *borderView;


@end

//
//  TLPlacesAPIType.h
//  TimeLine
//
//  Created by Evgenii Oborin on 28.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#ifndef TLPlacesAPIType_h
#define TLPlacesAPIType_h

typedef enum : NSUInteger {
    TLPlacesAPITypeGoogle,
    TLPlacesAPITypeFoursquare
} TLPlacesAPIType;

#endif /* TLPlacesAPIType_h */

//
//  TLListSectionHeader.h
//  TimeLine
//
//  Created by Evgenii Oborin on 24.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLListSectionHeader : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *label;


@end

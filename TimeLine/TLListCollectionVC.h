//
//  TLListCollectionVC.h
//  TimeLine
//
//  Created by Evgenii Oborin on 23.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLUserTask;

@protocol TLListCollectionVCDelegate <NSObject>

- (void) pushDetailViewWithTask:(TLUserTask*) task;

@end

@interface TLListCollectionVC : UICollectionViewController

@property (weak,nonatomic) id<TLListCollectionVCDelegate> delegate;

@end

//
//  TLDelayedCheckin.m
//  TimeLine
//
//  Created by Evgenii Oborin on 22.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLDelayedCheckin.h"

@implementation TLDelayedCheckin

+ (instancetype)MR_createEntityInContext:(NSManagedObjectContext *)context {
    TLDelayedCheckin *checkin = [super MR_createEntityInContext:context];
    
    
    checkin.orderValue = @([[self maxValueWithKey:@"orderValue"] integerValue] + 1);
    checkin.time=[NSDate date];
    checkin.isDefined=@0;
    return checkin;
}

- (CLLocationCoordinate2D)locationCoordinate {
    return CLLocationCoordinate2DMake(self.latitude.floatValue, self.longitude.floatValue);
}

+ (id)maxValueWithKey:(NSString *)key {
    TLDelayedCheckin *dc = [TLDelayedCheckin MR_findFirstOrderedByAttribute:key ascending:NO];
    
    return (dc != nil) ? [dc valueForKey:key] : @0;
}

+ (NSArray*) definedArrayInContext:(NSManagedObjectContext*) context{
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"isDefined = %@",@1];
    
    return [TLDelayedCheckin MR_findAllSortedBy:@"orderValue"
                                      ascending:YES
                                  withPredicate:pred inContext:context];
    
}

+ (NSArray*) definedArray{
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"isDefined = %@",@1];
    
    return [TLDelayedCheckin MR_findAllSortedBy:@"orderValue"
                         ascending:YES
                     withPredicate:pred];
    
}

+ (NSArray*) notDefinedArray{
    NSPredicate *pred=[NSPredicate predicateWithFormat:@"isDefined = %@",@0];
    
    return [TLDelayedCheckin MR_findAllSortedBy:@"orderValue"
                                ascending:YES
                            withPredicate:pred];
    
}

+ (TLDelayedCheckin*) checkinWithOrderValue:(NSNumber*)value inContext:(NSManagedObjectContext *)context{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"orderValue = %@",value];
    
    return [TLDelayedCheckin MR_findFirstWithPredicate:predicate inContext:context];
}



@end

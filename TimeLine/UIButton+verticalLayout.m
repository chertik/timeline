//
//  UIButton+verticalLayout.m
//  TimeLine
//
//  Created by Evgenii Oborin on 13.04.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "UIButton+verticalLayout.h"

@implementation UIButton (verticalLayout)

- (void)centerVerticallyWithPadding:(float)padding
{
    CGSize imageSize = self.imageView.frame.size;
    CGSize titleSize = self.titleLabel.frame.size;
    CGFloat totalHeight = (imageSize.height + titleSize.height + padding);
    self.imageEdgeInsets = UIEdgeInsetsMake(0,
                                            0.0f,
                                            0.0f,
                                            - titleSize.width);
    self.titleEdgeInsets = UIEdgeInsetsMake((totalHeight - imageSize.height),
                                            - imageSize.width,
                                            - (totalHeight - titleSize.height),
                                            0.0f);
}

- (void)centerVertically
{  
    const CGFloat kDefaultPadding = 6.0f;
    [self centerVerticallyWithPadding:kDefaultPadding];  
}
@end

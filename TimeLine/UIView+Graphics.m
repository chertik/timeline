//
//  UIView+Graphics.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "UIView+Graphics.h"
#import <objc/runtime.h>

#define defaultHeightEllipseCap 45

static void *kHeightEllipsCapKey;

@implementation UIView (Graphics)

- (void)setOrigin:(CGPoint)point {
    CGRect frame = self.frame;
    
    frame.origin = point;
    
    self.frame = frame;
}

- (void)setOrigin:(CGPoint)point animated:(BOOL)animated {
    if (animated)
        [UIView animateWithDuration:0.25f animations:^{
            [self setOrigin:point];
        }];
    else
        [self setOrigin:point];
}

- (void)setAlpha:(CGFloat)alpha animated:(BOOL)animated {
    if (animated)
        [UIView animateWithDuration:0.4f animations:^{
            [self setAlpha:alpha];
        }];
    else
        [self setAlpha:alpha];
}

- (void)setCornerRadius:(CGFloat)radius {
    //self.layer.masksToBounds = YES;
    self.layer.cornerRadius = radius;
}

#pragma mark - Getters/Setters

- (CGFloat)heightEllipseCap {
    NSNumber *number_heightEllipseCap = objc_getAssociatedObject(self, &kHeightEllipsCapKey);
    
    if (number_heightEllipseCap == nil) {
        number_heightEllipseCap = @(defaultHeightEllipseCap);
        
        objc_setAssociatedObject(self, &kHeightEllipsCapKey, number_heightEllipseCap, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return number_heightEllipseCap.floatValue;
}

- (void)setHeightEllipseCap:(CGFloat)heightEllipseCap {
    objc_setAssociatedObject(self, &kHeightEllipsCapKey, @(heightEllipseCap), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Draw Ellipse View

- (void)drawHalfEllipseWithBackgroundColor:(UIColor *)color opacity:(CGFloat)opacity {
    [self drawHalfEllipseWithBackgroundColor:color opacity:opacity flippedOver:NO];
}

- (void)drawHalfFlippedEllipseWithBackgroundColor:(UIColor *)color opacity:(CGFloat)opacity {
    [self drawHalfEllipseWithBackgroundColor:color opacity:opacity flippedOver:YES];
}

- (void)drawHalfEllipseWithBackgroundColor:(UIColor *)color opacity:(CGFloat)opacity flippedOver:(BOOL)flipped {
    [self drawHalfEllipseWithShapeOffsetX:10 backgroundColor:color opacity:opacity flippedOver:flipped];
}

- (void)drawHalfEllipseWithShapeOffsetX:(CGFloat)shapeOffset_x backgroundColor:(UIColor *)color opacity:(CGFloat)opacity flippedOver:(BOOL)flipped {
    [self drawHalfEllipseWithShapeOffsetX:shapeOffset_x heightShapeCap:self.heightEllipseCap backgroundColor:color opacity:opacity flippedOver:flipped];
}

- (void)drawHalfEllipseWithShapeOffsetX:(CGFloat)shapeOffset_x heightShapeCap:(CGFloat)heightShapeCap backgroundColor:(UIColor *)color opacity:(CGFloat)opacity flippedOver:(BOOL)flipped {
    CGRect frame = self.frame;
    
    
     
    CALayer *layerTop = [CALayer layer];
    CGRect layerTopFrame = CGRectMake(0, 0, frame.size.width, frame.size.height - heightShapeCap);
    
    layerTop.frame = layerTopFrame;
    layerTop.backgroundColor = color.CGColor;
    layerTop.opacity = opacity;
    
    CGRect shapeFrame = CGRectMake(-shapeOffset_x, frame.size.height - heightShapeCap, frame.size.width + 2 * shapeOffset_x, heightShapeCap);
    CGRect shapeDrawingFrame = CGRectMake(0, - heightShapeCap, frame.size.width + 2 * shapeOffset_x, 2 * heightShapeCap);
    
    //default flipped is NO
    if (flipped) {
        shapeFrame.origin.y = 0;
        shapeDrawingFrame.origin.y = 0;
        
        layerTopFrame.origin.y = shapeFrame.origin.y + shapeFrame.size.height;
        
        layerTop.frame = layerTopFrame;
    }
    
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:shapeDrawingFrame];
    CAShapeLayer *shape = [CAShapeLayer layer];
    CGPathRef editedPath = path.CGPath;
    
    shape.frame = shapeFrame;
    shape.path = editedPath;
    shape.fillColor = color.CGColor;
    shape.opacity = opacity;
    shape.masksToBounds = YES;
    //shape.shadowColor = [UIColor blackColor].CGColor;
    //shape.shadowOpacity = 0.45f;
    //shape.shadowRadius = 8;
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self.layer insertSublayer:layerTop atIndex:0];
    [self.layer insertSublayer:shape atIndex:0];
    
    [self setShadowWithColor:[UIColor blackColor] radius:8 opacity:0.45f];
}

- (void)setShadowWithColor:(UIColor *)color radius:(CGFloat)radius opacity:(CGFloat)opacity {
    self.layer.shadowColor = color.CGColor;
    self.layer.shadowRadius = radius;
    self.layer.shadowOpacity = opacity;
    self.layer.shadowOffset = CGSizeMake(0,0);
}

#pragma mark - Animation

- (void)slideTopWithDuration:(NSTimeInterval)interval Offset:(CGPoint)offset completion:(void (^)())completionBlock {
    [UIView animateWithDuration:interval
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGPoint newCenter = self.center;
                         
                         newCenter.x += offset.x;
                         newCenter.y += offset.y;
                         
                         [self setCenter:newCenter];
                     }
                     completion:^(BOOL finished) {
                         if (completionBlock != nil)
                             completionBlock();
                     }];
}

- (void)slideBottomWithDuration:(NSTimeInterval)interval Offset:(CGPoint)offset completion:(void (^)())completionBlock {
    [UIView animateWithDuration:interval
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGPoint newCenter = self.center;
                         
                         newCenter.x -= offset.x;
                         newCenter.y -= offset.y;
                         
                         [self setCenter:newCenter];
                     }
                     completion:^(BOOL finished) {
                         if (completionBlock != nil)
                             completionBlock();
                     }];
}

@end

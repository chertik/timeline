//
//  MainViewController.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASMapView.h"
#import "ASImageButtonView.h"


typedef NS_ENUM(NSInteger, AnimationState) {
    AnimationStateStop,
    AnimationStateActive,
    AnimationStateComplete
};

typedef enum AnimationState AnimationState;

@interface MainViewController : UIViewController < UIScrollViewDelegate, GMSMapViewDelegate, NSFetchedResultsControllerDelegate, UITextFieldDelegate,CLLocationManagerDelegate> {
    
    NSArray *timelineData;
}

@property (strong, nonatomic) NSFetchedResultsController *fetchedTimeline;

@property (weak, nonatomic) IBOutlet UIView *topBar;
@property (weak, nonatomic) IBOutlet UIView *bottomBar;
@property (weak, nonatomic) IBOutlet UIView *toolBar;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (weak, nonatomic) IBOutlet ASMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *searchOnMapButton;

@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UIButton *eventsButton;
@property (weak, nonatomic) IBOutlet UIButton *friendsButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutVSOfTopBar;

//- (IBAction)actionSearchButtonTap:(id)sender;
//- (IBAction)actionOpenToolBar:(id)sender;

@end

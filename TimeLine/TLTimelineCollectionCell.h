//
//  TLTimelineCollectionCell.h
//  TimeLine
//
//  Created by Evgenii Oborin on 29.09.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLTimelineCollectionCell;

@protocol TLTimelineCollectionCellDelegate <NSObject>

- (void) didTappedDeleteAtCell:(TLTimelineCollectionCell*) cell;

@end

@interface TLTimelineCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *borderView;
@property (weak, nonatomic) IBOutlet UIImageView *colorDot;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *markView;

@property (weak, nonatomic) IBOutlet UIView *iconContainerView;


@property (weak, nonatomic) IBOutlet UIButton *deleteCellButton;

@property (weak,nonatomic) id<TLTimelineCollectionCellDelegate> delegate;

- (void) startWiggling;
- (void) stopWiggling;
- (void) stopWigglingAnimated;

- (void) showDeleteButtonAnimated:(BOOL) animated;
- (void) hideDeleteButtonAnimated:(BOOL) animated;

@end

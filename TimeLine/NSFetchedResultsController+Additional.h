//
//  NSFetchedResultsController+Additional.h
//  CurrencyItsEasy
//
//  Created by Alexey on 3/8/16.
//  Copyright © 2016 inspirelab. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSFetchedResultsController (Additional)

- (BOOL)reloadDataWithCacheName:(NSString *)cacheName;

@end

//
//  ASImageButtonView.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 05/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "ASImageButtonView.h"

#define defaultContentOffset UIEdgeInsetsMake(0, 0, 0, 0)

@implementation ASImageButtonView

@synthesize layoutMode = _layoutMode;
@synthesize imageSize = _imageSize;
@synthesize contentOffset = _contentOffset;

#pragma mark - Getters/Setters Parameters

- (LayoutMode)layoutMode {
    return _layoutMode;
}

- (void)setLayoutMode:(LayoutMode)layoutMode {
    _layoutMode = layoutMode;
}

- (CGSize)imageSize {
    return _imageSize;
}

- (void)setImageSize:(CGSize)imageSize {
    _imageSize = imageSize;
}

- (UIEdgeInsets)contentOffset {
    return _contentOffset;
}

- (void)setContentOffset:(UIEdgeInsets)contentOffset {
    _contentOffset = contentOffset;
}

#pragma mark - UIView methods

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.contentOffset = defaultContentOffset;
        
        self.imageEdgeInsets = UIEdgeInsetsZero;
        self.titleEdgeInsets = UIEdgeInsetsZero;
        self.contentEdgeInsets = UIEdgeInsetsZero;
        
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        //Debug
        //[self setBackgroundColor:[UIColor yellowColor]];
        //[self.imageView setBackgroundColor:[UIColor redColor]];
        //[self.titleLabel setBackgroundColor:[UIColor blueColor]];
        
        self.layoutMode = LayoutModeImageAboveTitle;
        self.imageSize = CGSizeZero;
    }
    
    return self;
}

- (void)setNeedsLayout {
    [super setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imageEdgeInsets = UIEdgeInsetsZero;
    self.titleEdgeInsets = UIEdgeInsetsZero;
    self.contentEdgeInsets = UIEdgeInsetsZero;
    
    CGRect frame = self.bounds;
    UIEdgeInsets contentOffset = self.contentOffset;
    
    frame.origin.x += contentOffset.left;
    frame.origin.y += contentOffset.top;
    frame.size.width -= contentOffset.left + contentOffset.right;
    frame.size.height -= contentOffset.top + contentOffset.bottom;
    
    //This is full image button
    if (self.currentTitle.length == 0) {
        if (self.currentImage) {
            CGRect imageFrame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height);
            
            [self.imageView setFrame:imageFrame];
        }
    }
    else if (self.currentTitle.length > 0 && self.currentImage) {
        //This is image/text button
        if (self.layoutMode == LayoutModeImageAboveTitle) {
            [self.titleLabel sizeToFit];
            
            CGRect titleFrame = self.titleLabel.frame;
            
            titleFrame.origin.x = frame.origin.x + (frame.size.width - titleFrame.size.width) / 2;
            titleFrame.origin.y = frame.size.height - titleFrame.size.height;
            
            [self.titleLabel setFrame:titleFrame];
            
            CGRect imageFrame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - titleFrame.size.height);
            
            if (!CGSizeEqualToSize(_imageSize, CGSizeZero)) {
                CGPoint imageCenter = CGPointMake(imageFrame.origin.x + (imageFrame.size.width / 2), imageFrame.origin.y + (imageFrame.size.height / 2));
                
                imageFrame.size = _imageSize;
                
                [self.imageView setFrame:imageFrame];
                [self.imageView setCenter:imageCenter];
            }
            else {
                [self.imageView setFrame:imageFrame];
            }
        }
    }
}

@end

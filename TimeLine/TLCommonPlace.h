//
//  TLCommonPlace.h
//  TimeLine
//
//  Created by Evgenii Oborin on 25.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TLVenue.h"
#import "TLGPlace.h"

@interface TLCommonPlace : NSObject

@property (strong,nonatomic) NSString *name;

@property (strong,nonatomic) NSString *address;
@property (strong,nonatomic) NSNumber *rating;
@property (strong,nonatomic) NSNumber *distance;

+ (instancetype) commonPlaceWithGooglePlace:(TLGPlace*) place;
+ (instancetype) commonPlaceWithFoursquarePlace:(TLVenue*) venue;

@end

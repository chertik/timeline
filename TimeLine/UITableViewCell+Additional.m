//
//  UITableViewCell+Additional.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 08/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "UITableViewCell+Additional.h"

@implementation UITableViewCell (Additional)

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier width:(CGFloat)width {
    if (self = [self initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        CGRect frame = self.frame;
        
        frame.size.width = width;
        
        self.frame = frame;
    }
    
    return self;
}

@end

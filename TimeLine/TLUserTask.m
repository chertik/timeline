//
//  TLUserTask.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 10/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLUserTask.h"
#import "UIImage+vImageScaling.h"
#import "LoadImage.h"
#import "TLPlaceData+CoreDataClass.h"
#import "UIImage+TLIcon.h"
#import "TLPhotoMarker.h"
#import "TLTaskIconManager.h"

@implementation TLUserTask


+ (instancetype)MR_createEntityInContext:(NSManagedObjectContext *)context {
    TLUserTask *userTask = [super MR_createEntityInContext:context];
    
    userTask.id = @([[self maxValueWithKey:@"id"] integerValue] + 1);
    
    userTask.route_id = @0;
    userTask.is_visible = @1;
    userTask.is_timeline = @0;
    userTask.is_past = @0;
    userTask.is_current_location_holder=@1;
    
    return userTask;
}


#pragma mark - Public methods

//- (void) moveToId:(NSInteger) id {
//    
//    if(id==self.id.integerValue) return;
//    
//    
//    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
//     
//        TLUserTask *selfTaskInCont=[TLUserTask userTaskWithId:self.id inContext:localContext];
//        
//        if(id<self.id.integerValue){
//        
//            for(NSInteger i=self.id.integerValue-1 ; i>=id; i--){
//                TLUserTask *ut=[TLUserTask userTaskWithId:@(i) inContext:localContext];
//                ut.id=@(i+1);
//            }
//        } else {
//            for (NSInteger i=self.id.integerValue+1;i<=id;i++){
//                TLUserTask *ut=[TLUserTask userTaskWithId:@(i) inContext:localContext];
//                ut.id=@(i-1);
//
//            }
//        }
//        
//        selfTaskInCont.id=@(id);
//        
//        TLUserTask *nextTask=[TLUserTask userTaskWithId:@(id+1) inContext:localContext];
//        TLUserTask *prevTask=[TLUserTask userTaskWithId:@(id-1) inContext:localContext];
//        
//        if(!prevTask){
//            selfTaskInCont.datetime=[nextTask.datetime dateByAddingTimeInterval:-5*60];
//
//        } else  if(!nextTask){
//            selfTaskInCont.datetime=[prevTask.datetime dateByAddingTimeInterval:5*60];
//
//        } else {
//            NSTimeInterval interval=-[prevTask.datetime timeIntervalSinceDate:nextTask.datetime];
//            selfTaskInCont.datetime=[prevTask.datetime dateByAddingTimeInterval:interval/2];
//        }
//       
//    }];
//
//}
- (void) moveToId:(NSInteger) id {
    
    if(id==self.id.integerValue) return;
    
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
        
        TLUserTask *selfTaskInCont=[TLUserTask userTaskWithId:self.id inContext:localContext];
        
        [selfTaskInCont moveToId:id inContext:localContext changeTime:YES];
        
    }];
    
}

- (void) moveToId:(NSInteger) id inContext:(NSManagedObjectContext *)context changeTime:(BOOL)changeTime{
    
    if(id==self.id.integerValue) return;
    
    if(id<self.id.integerValue){
        
        for(NSInteger i=self.id.integerValue-1 ; i>=id; i--){
            TLUserTask *ut=[TLUserTask userTaskWithId:@(i) inContext:context];
            ut.id=@(i+1);
        }
    } else {
        for (NSInteger i=self.id.integerValue+1;i<=id;i++){
            TLUserTask *ut=[TLUserTask userTaskWithId:@(i) inContext:context];
            ut.id=@(i-1);
        }
    }
    
    self.id=@(id);
    
    if(changeTime){
        TLUserTask *nextTask=[TLUserTask userTaskWithId:@(id+1) inContext:context];
        TLUserTask *prevTask=[TLUserTask userTaskWithId:@(id-1) inContext:context];
        
        if(!prevTask){
            self.datetime=[nextTask.datetime dateByAddingTimeInterval:-5*60];
            return;
        }
        if(!nextTask){
            self.datetime=[prevTask.datetime dateByAddingTimeInterval:5*60];
            return;
        }
        
        NSTimeInterval interval=-[prevTask.datetime timeIntervalSinceDate:nextTask.datetime];
        NSDate *resultDate=[prevTask.datetime dateByAddingTimeInterval:interval/2];
        
        //for past task cannot set date that is bigger than now
        if(self.is_past.boolValue){
            if([resultDate timeIntervalSinceNow]>=0){
                resultDate=[NSDate dateWithTimeIntervalSinceNow:-5*60];
            }
        } else {
            if([resultDate timeIntervalSinceNow]<=0){
                resultDate=[NSDate dateWithTimeIntervalSinceNow:5*60];
            }
        }
        
        self.datetime=resultDate;
    }

}

- (void) moveToId:(NSInteger) id inContext:(NSManagedObjectContext *)context {

    [self moveToId:id inContext:context changeTime:YES];
}

- (void) deleteTaskWithCompletion:(void(^)(void))completionBlock{

    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        TLUserTask *selfInCont=[TLUserTask userTaskWithId:self.id inContext:localContext];
        
        TLUserTask *prevTask=[TLUserTask userTaskWithId:@(self.id.intValue-1) inContext:localContext];
        if(prevTask) {
            for(TLLocation *loc in prevTask.routeToNextTask){
                [loc MR_deleteEntityInContext:localContext];
            }
            prevTask.routeToNextTask=nil;
        }
        
        if(selfInCont.routeToNextTask){
            for(TLLocation *loc in selfInCont.routeToNextTask){
                [loc MR_deleteEntityInContext:localContext];
            }
            selfInCont.routeToNextTask=nil;
        }
        
        NSInteger maxId=[[TLUserTask maxValueWithKey:@"id" inContext:localContext] integerValue];
        
        for(NSInteger i=self.id.integerValue+1;i<=maxId;i++){
            TLUserTask *ut=[TLUserTask userTaskWithId:@(i) inContext:localContext];
            ut.id=@(i-1);
        }
        
        [selfInCont MR_deleteEntityInContext:localContext];
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        if(completionBlock)
            completionBlock();
    }];
}


- (CLLocationCoordinate2D)locationCoordinate {
    return CLLocationCoordinate2DMake(self.latitude.floatValue, self.longitude.floatValue);
}

+ (TLUserTask *)userTaskWithId:(NSNumber *)id {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", id];
    
    return [TLUserTask MR_findFirstWithPredicate:predicate];
}

+ (TLUserTask *)userTaskWithId:(NSNumber *)id inContext:(NSManagedObjectContext *)context {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", id];
    
    return [TLUserTask MR_findFirstWithPredicate:predicate inContext:context];
}

+ (id)maxValueWithKey:(NSString *)key {
    TLUserTask *ut = [TLUserTask MR_findFirstOrderedByAttribute:key ascending:NO];
    
    return (ut != nil) ? [ut valueForKey:key] : @0;
}

+ (id)maxValueWithKey:(NSString *)key inContext:(NSManagedObjectContext*) context{
    TLUserTask *ut = [TLUserTask MR_findFirstOrderedByAttribute:key ascending:NO inContext:context];
    
    return (ut != nil) ? [ut valueForKey:key] : @0;
}

- (NSInteger) moveTaskToRecentPastInContext:(NSManagedObjectContext*) context{
    
    TLUserTask *lastPast=[TLUserTask MR_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"is_past = %@", @1] sortedBy:@"id" ascending:NO inContext:context]; 
    
    NSInteger intFirstNonPastID=[lastPast.id integerValue]+1;
    
    if(intFirstNonPastID<=self.id.integerValue){
        for(NSInteger i=self.id.integerValue-1;i>=intFirstNonPastID;i--){
            TLUserTask *ut=[TLUserTask userTaskWithId:@(i) inContext:context];
            ut.id=@(i+1);
        }
        self.id=@(intFirstNonPastID);
    } else {
        for (NSInteger i=self.id.integerValue+1;i<intFirstNonPastID;i++){
            TLUserTask *ut=[TLUserTask userTaskWithId:@(i) inContext:context];
            ut.id=@(i-1);
            
        }
        self.id=@(intFirstNonPastID-1);
    }
    

    self.is_past=@1;
    
    return intFirstNonPastID;
}

- (NSInteger) moveTaskToPastByTimeWithMaxInd:(NSInteger) mIndex{
    __block NSInteger newId;
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
        TLUserTask *taskInContext=[TLUserTask userTaskWithId:self.id inContext:localContext];
        newId=[taskInContext moveTaskToPastByTimeWithMaxInd:mIndex inContext:localContext];
    }];
    return newId;
}

- (NSInteger) moveTaskToPastByTimeWithMaxInd:(NSInteger) mIndex inContext:(NSManagedObjectContext*) context{
    
    NSArray<TLUserTask*> *tasksArray=[TLUserTask MR_findAllSortedBy:@"id" ascending:YES inContext:context]; // id
    
    for (int i = 0; i < tasksArray.count; i++) {
        NSLog(@"%@", tasksArray[i].datetime);
    }
    
    if(mIndex==0){
        self.is_past=@1;
        [self moveToId:1 inContext:context changeTime:NO];
        return 0;
    }
    //binary search
    
    NSInteger max=mIndex;
    NSInteger min=1;
    
    NSInteger ind=min;
    
    while(max>min){
        ind=(max-min)/2+min;
        
        TLUserTask *curTask=tasksArray[ind-1];
        if([self.datetime timeIntervalSinceDate:curTask.datetime]>=0){
            min=ind+1;
        } else{
            max=ind-1;
        }
    }
    
 
    if([self.datetime timeIntervalSinceDate:tasksArray[min-1].datetime]>=0){
        ind=min+1;
    } else {
        ind=min;
    }   
 
    
    self.is_past=@1;
    
    [self moveToId:ind inContext:context changeTime:NO];
    
    return ind;
}

- (NSInteger) moveTaskToFuture{

    __block NSInteger newId;
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
        TLUserTask *taskInCont=[TLUserTask userTaskWithId:self.id inContext:localContext];
        newId=[taskInCont moveTaskToFutureInContext:localContext];
    }];
    
    return newId;
    
}

- (NSInteger) moveTaskToFutureInContext:(NSManagedObjectContext*) context{

    
    NSInteger maxId=[(NSNumber*)[TLUserTask maxValueWithKey:@"id"] integerValue];
    
    [self moveToId:maxId inContext:context];
    
    self.is_past=@0;
    
    if([self.datetime timeIntervalSinceNow]<10*60){
        self.datetime=[[NSDate date] dateByAddingTimeInterval:24*60*60];
    }
    
    return maxId;
    
}

- (void) iconForSize:(CGSize)size callback:(void(^)(UIImage *))callback{
    if(self.photo_path){
        UIImage *image=[UIImage imageNamed:self.photo_path];
        CGFloat scale=[UIScreen mainScreen].scale;
        
        callback([image vImageScaledImageWithSize:CGSizeMake(size.width*scale, size.height*scale)]);
    } else {
        NSString *fileName=[self.icon_path stringByAppendingString:self.is_past.boolValue? @"Gray": @"Red"];
        NSString *sizeName;
        if(size.width==38){
            sizeName=@"Compact";
        } else if(size.width==41){
            
            sizeName=@"Table";
        } else if (size.width==60){
            sizeName=@"Detail";
        } else {
            [NSException raise:@"Wrong size for taskIcon" format:@"size %@ error",NSStringFromCGSize(size)];
            
        }
        
        NSString *fullName=[fileName stringByAppendingString:sizeName];
        UIImage *image=[UIImage imageNamed:fullName];
        callback(image);
    }
}

- (NSString*) dayName{
    return [self relativeDateStringFromDate:self.datetime];
}

- (NSString*) relativeDateStringFromDate:(NSDate*) date{
    
    if(!date) {
        NSLog(@"no date");
        return nil;
    }
    
    NSCalendarUnit units = NSCalendarUnitDay;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    NSInteger days=components.day;
    NSDateFormatter* cellDateFormatter=[[NSDateFormatter alloc] init];
    [cellDateFormatter setDateFormat:@"dd MMMM yyyyг."];
    cellDateFormatter.locale=[[NSLocale alloc]initWithLocaleIdentifier:@"RU"];
    switch (days) {
        case 0:
            return @"Сегодня";
            break;
            
        case 1:
            return @"Вчера";
            break;
            
        default:
            return [cellDateFormatter stringFromDate:date];
            break;
    }
    
}

+ (BOOL)date:(NSDate*)date isBetweenDate:(NSDate*)beginDate andDate:(NSDate*)endDate
{
    if ([date compare:beginDate] == NSOrderedAscending)
        return NO;
    
    if ([date compare:endDate] == NSOrderedDescending)
        return NO;
    
    return YES;
}

- (void) changePlaceTo:(TLPlaceData *)placeData inContext:(NSManagedObjectContext*) context{

    self.selected_place_id=placeData.id;
    self.address=placeData.address;
    self.desc=placeData.name;
    
}

- (void) changePlaceTo:(TLPlaceData *)placeData{
    
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
        
        TLUserTask *selfTaskInCont=[TLUserTask userTaskWithId:self.id inContext:localContext];
        
        selfTaskInCont.selected_place_id=placeData.id;
        selfTaskInCont.address=placeData.address;
        selfTaskInCont.desc=placeData.name;
        
    }];
    
}

- (UIImage*) iconMarker{
    return [[TLTaskIconManager sharedManager] sharedImageForIconPath:self.icon_path size:CGSizeMake(31, 31) square:NO border:NO shadow:NO isPhoto:NO isPink:!self.is_past.boolValue];
}

- (UIImage*) photoMarker{
    CGFloat width=39,height=49;
    TLPhotoMarker *marker=[[TLPhotoMarker alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [marker setPhoto:[UIImage imageNamed:self.photo_path]];

    
    UIImage *image=[UIImage imageWithView:marker size:CGSizeMake(width, height)];
    
    return image;
}

- (UIImage*) mapMarker{
    if(self.photo_path){
        return [self photoMarker];
    } else {
        return [self iconMarker];
    }
}

- (CGPoint) mapMarkerAnchorPoint{
    if(self.photo_path) {
        return CGPointMake(0.5, 0.5);
    } else {
        return CGPointMake(0.48, 0.92);
    }
}


//- (NSString*) description{
//    return [NSString stringWithFormat:@"id:%ld desc:%@ ispast:%@",self.id.integerValue,self.desc,(self.is_past.boolValue? @"YES" : @"NO")];
//}

@end

//
//  TLAddToMapListCell.h
//  TimeLine
//
//  Created by Evgenii Oborin on 29.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLAddToMapListCell;

@protocol TLAddToMapListCellDelegate <NSObject>

- (void) confirmTaskByCell:(TLAddToMapListCell*) cell;

@end


@interface TLAddToMapListCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@property (weak,nonatomic) id<TLAddToMapListCellDelegate> delegate;

- (void) setButtonIsAdd:(BOOL)isAdd;

@end

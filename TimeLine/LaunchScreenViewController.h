//
//  LaunchScreenViewController.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchScreenViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end

//
//  TLUserLoginResponse.h
//  TimeLine
//
//  Created by Evgenii Oborin on 07.06.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLUserLoginResponse : NSObject

@property (strong,nonatomic) NSString *tokenKey;

@end

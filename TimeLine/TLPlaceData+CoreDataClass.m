//
//  TLPlaceData+CoreDataClass.m
//  TimeLine
//
//  Created by Evgenii Oborin on 27.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLPlaceData+CoreDataClass.h"
#import "TLCommonPlace.h"

@implementation TLPlaceData

+ (instancetype) MR_createEntityInContext:(NSManagedObjectContext *)context{
    TLPlaceData *placeData=[super MR_createEntityInContext:context];
    
    placeData.id=@([[self maxValueWithKey:@"id" inContext:context] integerValue]+1);
    
    return placeData;
    
}

+ (TLPlaceData*) createPlaceDataWithCommonPlace:(TLCommonPlace*)place inContext:(NSManagedObjectContext*)context{
    
    TLPlaceData *placeData=[TLPlaceData MR_createEntityInContext:context];
    
    placeData.name=place.name;
    placeData.address=place.address;
    placeData.rating=place.rating;
    placeData.distance=place.distance;
    
    return placeData;

}

+ (TLPlaceData*) placeDataWithId:(NSNumber*) id {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", id];
    
    return [TLPlaceData MR_findFirstWithPredicate:predicate];
    
}

+ (TLPlaceData*) placeDataWithId:(NSNumber*) id inContext:(NSManagedObjectContext*) context {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id = %@", id];
    
    return [TLPlaceData MR_findFirstWithPredicate:predicate inContext:context];
    
}

+ (id)maxValueWithKey:(NSString *)key {
    TLPlaceData *ut = [TLPlaceData MR_findFirstOrderedByAttribute:key ascending:NO];
    
    return (ut != nil) ? [ut valueForKey:key] : @0;
}

+ (id)maxValueWithKey:(NSString *)key inContext:(NSManagedObjectContext*)context{
    TLPlaceData *ut = [TLPlaceData MR_findFirstOrderedByAttribute:key ascending:NO inContext:context];
    
    return (ut != nil) ? [ut valueForKey:key] : @0;
}

- (NSString*) description {
    return [NSString stringWithFormat:@"ID:%ld Name:%@ distance:%f rating:%@ address:%@",self.id.integerValue,self.name,self.distance.doubleValue,self.rating,self.address];
}

@end

//
//  DatabaseAPI.h
//  CurrencyItsEasy
//
//  Created by Alexey on 3/7/16.
//  Copyright © 2016 inspirelab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseAPI : NSObject

+ (DatabaseAPI *)sharedAPI;

- (void)cleanUpDatabase;

@end

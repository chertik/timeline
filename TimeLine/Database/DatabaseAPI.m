//
//  DatabaseAPI.m
//  CurrencyItsEasy
//
//  Created by Alexey on 3/7/16.
//  Copyright © 2016 inspirelab. All rights reserved.
//

#import "DatabaseAPI.h"
#import "TLDelayedCheckin.h"
#import "TLPlaceData+CoreDataClass.h"
#import "TLPhotoImporter.h"
#import "TLBackendManager.h"

@implementation DatabaseAPI

static DatabaseAPI *sharedAPI;

+ (DatabaseAPI *)sharedAPI {
    if (!sharedAPI || sharedAPI == NULL) {
        sharedAPI = [DatabaseAPI new];
    }
    
    return sharedAPI;
}

- (instancetype)init {
    if (self = [super init]) {
        [MagicalRecord setupCoreDataStackWithStoreNamed:@"CoreDataModel"];        
    }
    
    return self;
}

- (void)cleanUpDatabase {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        [TLUserTask MR_truncateAllInContext:localContext];
        [TLDelayedCheckin MR_truncateAllInContext:localContext];
        [TLPlaceData MR_truncateAllInContext:localContext];
        [TLLocation MR_truncateAllInContext:localContext];
    }];
    
    [[TLBackendManager sharedManager] logoutInBackgroundWithBlock:^(NSError *error) {
        
    }];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isExistVkData"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:kDefaultsKeyLastImportedPhotoDate];
}

@end

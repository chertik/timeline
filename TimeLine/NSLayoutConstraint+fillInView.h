//
//  NSLayoutConstraint+fillInView.h
//  TimeLine
//
//  Created by Evgenii Oborin on 23.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (fillInView)

- (void) fillView:(UIView*) view inVIew:(UIView*)fill;

@end

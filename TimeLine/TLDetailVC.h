//
//  TLDetailVC.h
//  TimeLine
//
//  Created by Evgenii Oborin on 19.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLUserTask;
@class TLUserTaskPhoto;

@protocol TLDetailVCDelegate <NSObject>

- (void) popBack;
- (void) showChangePlaceForTask:(TLUserTask*) task;
- (void) keyBoardWillShowWithHeight:(CGFloat) height;
- (void) keyBoardWillBeHidden;

- (void) setDetailPhoto:(NSOrderedSet<TLUserTaskPhoto*>*)photo;

@end

@interface TLDetailVC : UIViewController

@property (weak,nonatomic) id<TLDetailVCDelegate> delegate;

- (void) setUserTask:(TLUserTask*) userTask;

- (TLUserTask*) currentTask;

@end

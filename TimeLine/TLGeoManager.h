//
//  TLGeoManager.h
//  TimeLine
//
//  Created by Evgenii Oborin on 28.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLPlacesAPIType.h"

//#define kGeoManagerDidSuccessfullyCheckedWithVisit @"kGeoManagerDidSuccessfullyCheckedWithVisit"
#define kGeoManagerDidSuccessfullyChecked @"kGeoManagerDidSuccessfullyChecked"

#define kGeoManagerChangeLocationManagerAuthorizationStatus @"kGeoManagerChangeLocationManagerAuthorizationStatus"
#define kGeoManagerUpdatedCurrentAddress @"kGeoManagerUpdatedCurrentAddress"
#define kGeoManagerUpdatedCurrentLocation @"kGeoManagerGetLocationFirstTime"
#define kGeoManagerUpdatedCurrentRoute @"kGeoManagerUpdatedCurrentRoute"
#define kGeoManagerUpdatedLocationFirstTimeForCurrentLaunch @"kGeoManagerUpdatedLocationFirstTimeForCurrentLaunch"

#define kDefaultsAppWakingHackIsEnabled @"kDefaultsAppWakingHackIsEnabled"

@class TLUserTask;

@protocol TLGeoManagerLocationDelegate <NSObject>


@end

@interface TLGeoManager : NSObject

+ (instancetype) sharedManager;

- (CLLocation*) getCurrentLocation;
- (NSArray<CLLocation*>*) getRoute;

- (void) startUpdatingLocations;
- (void) stopUpdatingLocations;
- (void) enableRouteTracking;
- (void) stopRouteTracking;

- (void) cancelCurrentRouteTracking;

- (void) saveCurrentRouteAndSettings;

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType withDate:(int)checkinDate;
- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock withDate:(int)checkinDate;

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock withDate:(int)checkinDate photo:(UIImage*)photoImage;

- (void) makeCheckinWithLocation:(CLLocation*)location withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock isCurrentAdress:(BOOL) isCurrentAdress withDate:(int)checkinDate photo:(UIImage*) photoImage;

- (void) makeCheckinWithUserTask:(TLUserTask *)userTask withAPI:(TLPlacesAPIType)placesAPIType completionBlock:(void(^)(void))completionBlock failureBlock:(void(^)(void))failureBlock;

- (void) addCurrentRouteIfPossibleLeadingToTask:(TLUserTask*)userTask inContext:(NSManagedObjectContext*) context;

- (void) getAdressesForAllTasks;

@property (strong,atomic) NSString *currentAddress;

@end

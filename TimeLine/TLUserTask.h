//
//  TLUserTask.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 10/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>
#import "TLUserTaskPhoto+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@class TLPlaceData;

@interface TLUserTask : NSManagedObject

- (CLLocationCoordinate2D)locationCoordinate;
+ (TLUserTask *)userTaskWithId:(NSNumber *)id;
+ (TLUserTask *)userTaskWithId:(NSNumber *)id inContext:(NSManagedObjectContext *)context;
+ (id)maxValueWithKey:(NSString *)key;
+ (id)maxValueWithKey:(NSString *)key inContext:(NSManagedObjectContext*) context;

- (void) deleteTaskWithCompletion:(void(^)(void))completionBlock;

- (NSInteger) moveTaskToRecentPastInContext:(NSManagedObjectContext*) context;
- (void) moveToId:(NSInteger) id;
- (void) moveToId:(NSInteger) id inContext:(NSManagedObjectContext *)context;

- (NSInteger) moveTaskToFuture;
- (NSInteger) moveTaskToPastByTimeWithMaxInd:(NSInteger) mIndex;
- (NSInteger) moveTaskToPastByTimeWithMaxInd:(NSInteger) mIndex inContext:(NSManagedObjectContext*) context;

- (void) changePlaceTo:(TLPlaceData*) placeData;
- (void) changePlaceTo:(TLPlaceData *)placeData inContext:(NSManagedObjectContext*) context;

- (CGPoint) mapMarkerAnchorPoint;
- (UIImage*) mapMarker;

- (void) iconForSize:(CGSize)size callback:(void(^)(UIImage *image))callback;

@end

NS_ASSUME_NONNULL_END

#import "TLUserTask+CoreDataProperties.h"

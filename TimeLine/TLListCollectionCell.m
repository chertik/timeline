//
//  TLListCollectionCell.m
//  TimeLine
//
//  Created by Evgenii Oborin on 23.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLListCollectionCell.h"
#import "TLThemeManager.h"

#define cRedArrowName @"RightArrowPink"
#define cGrayArrowName @"RightArrowGray"

@implementation TLListCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.layer.borderColor=dThemeColorGray.CGColor;
    self.layer.borderWidth=2;
    self.layer.cornerRadius=4;
    self.iconView.layer.cornerRadius=6;
    self.iconView.clipsToBounds=YES;
}


- (void) setPastAppearance:(BOOL)isPast{
    if(isPast){
        self.layer.borderColor=dThemeColorGray.CGColor;
        [self.accessoryIcon setImage:[UIImage imageNamed:cGrayArrowName]];
        self.accessoryIcon.tintColor=dThemeColorGray;
    } else {
        self.layer.borderColor=dThemeColorRed.CGColor;
        [self.accessoryIcon setImage:[UIImage imageNamed:cRedArrowName]];
        self.accessoryIcon.tintColor=[TLThemeManager mainAccentColor];
    }
}

@end

//
//  ASMapView.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "ASMapView.h"
#import "Reachability.h"
#import "TLThemeManager.h"

@implementation ASMapView {
    NSMutableArray *polylines;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setupParameters];
        
        NSNumber *lastLat=[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsLastPositionLatitude];
        if(lastLat){
            NSNumber *lastLong=[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsLastPositionLongitude];
            [self setPositionWithLatitude:[lastLat doubleValue] longitude:[lastLong doubleValue] zoom:12];
        } else {
            [self setPositionWithLatitude:59.9549868 longitude:30.3236635 zoom:12];
        }
    }
    
    return self;
}

- (void)setupParameters {
    polylines = [NSMutableArray array];
    
    self.myLocationEnabled = YES;   
}



- (void)setPositionWithLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude zoom:(float)zoom {
    GMSCameraPosition *newCamera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:zoom];
    
    [self setCamera:newCamera];
}

- (void)addMarkerWithCoordinate:(CLLocationCoordinate2D)coordinate image:(UIImage *)image title:(NSString *)title zindex:(NSInteger)zindex anchorPoint:(CGPoint) anchorPoint {
    GMSMarker *marker = [GMSMarker markerWithPosition:coordinate];
    
    marker.title = @"Встреча";
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = image;
    marker.zIndex = (int) zindex;
    
    //icon center
    marker.groundAnchor = anchorPoint;
    
    marker.draggable = YES;
    marker.map = self;
}

- (void)addRouteFrom:(CLLocationCoordinate2D)coordinateA to:(CLLocationCoordinate2D)coordinateB withZIndex:(NSInteger)zindex {
    GMSPolyline *polyline = [[GMSPolyline alloc] init];
    GMSMutablePath *path = [GMSMutablePath path];
    
    NSArray *points = [self calculateRoutesFrom:coordinateA to:coordinateB];
    NSInteger numberOfSteps = points.count;
    
    for (NSInteger index = 0; index < numberOfSteps; index++) {
        CLLocation *location = [points objectAtIndex:index];
        CLLocationCoordinate2D coordinate = location.coordinate;
        [path addCoordinate:coordinate];
    }
    
    polyline.path = path;
    polyline.strokeColor = [UIColor whiteColor];
    polyline.strokeWidth = 7.0f;
    polyline.map = self;
    polyline.geodesic = YES;
    polyline.zIndex = (int) zindex;
    [polylines addObject:polyline];
    
    // Copy the previous polyline, change its color, and mark it as geodesic.
    polyline = [polyline copy];
    polyline.strokeColor = [TLThemeManager mainAccentColor];
    polyline.geodesic = YES;
    polyline.strokeWidth = 4.0f;
    polyline.map = self;
    polyline.zIndex = (int) zindex;
    [polylines addObject:polyline];
    
    
    
    /*GMSStrokeStyle *redToGray = [GMSStrokeStyle gradientFromColor:[UIColor colorWithRed:230/255.0f green:130/255.0f blue:112/255.0f alpha:1]
                                                          toColor:[UIColor lightGrayColor]];
    GMSStrokeStyle *red=[GMSStrokeStyle solidColor:[UIColor colorWithRed:230/255.0f green:130/255.0f blue:112/255.0f alpha:1]];
    GMSStrokeStyle *gray=[GMSStrokeStyle solidColor:[UIColor lightGrayColor]];
    
    double length=[path lengthOfKind:kGMSLengthGeodesic];
    
    
    NSArray *spans=GMSStyleSpans(path, @[red,redToGray,gray], @[[NSNumber numberWithDouble:length*0.6],
                                                                [NSNumber numberWithDouble:length*0.15],
                                                                [NSNumber numberWithDouble:length*0.25]], kGMSLengthGeodesic);
    
    polyline.spans=spans;*/
    
}

- (void)removePolylineWithZIndex:(NSInteger)zindex {
    for (int i = (int)polylines.count - 1; i >= 0; i--) {
        GMSPolyline *p = [polylines objectAtIndex:i];
        
        if (p.zIndex == zindex) {
            p.map = nil;
            
            [polylines removeObject:p];
        }
    }
}


- (NSArray *)calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t {
    if([Reachability reachabilityForInternetConnection].currentReachabilityStatus==NotReachable) return nil;
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=false&avoid=highways&mode=driving", saddr, daddr]];
    
//    NSError *error = nil;
    
    /*NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    
    [request setURL:url];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSURLResponse *response = nil;
    
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: &error];
    
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONWritingPrettyPrinted error:nil];*/
    
    __block NSDictionary *json = @{};
    
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        dispatch_semaphore_signal(sem);
    }] resume];
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    return [self decodePolyLine:[self parseResponse:json]];
}

- (NSString *)parseResponse:(NSDictionary *)response {
    NSArray *routes = [response objectForKey:@"routes"];
    NSDictionary *route = [routes lastObject];
    if (route) {
        NSString *overviewPolyline = [[route objectForKey:
                                       @"overview_polyline"] objectForKey:@"points"];
        return overviewPolyline;
    }
    return @"";
}

- (NSMutableArray *)decodePolyLine:(NSString *)encodedStr {
    NSMutableString *encoded = [[NSMutableString alloc]
                                initWithCapacity:[encodedStr length]];
    [encoded appendString:encodedStr];
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0,
                                                    [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1)
                          : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1)
                          : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:
                                [latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:location];
    }
    return array;
}

@end

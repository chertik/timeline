//
//  UIImage+TLIcon.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 11/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "UIImage+TLIcon.h"
#import "TLIconButton.h"

#define multiSize 3.0f

@implementation UIImage (TLIcon)

+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border  isPink:(BOOL) isPink{
   
    return [UIImage imageWithColorIcon:icon size:size square:square border:border isPhoto:NO isPink:isPink];
}

+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border isPhoto:(BOOL)isPhoto isPink:(BOOL) isPink{
    TLIconButton *iconButton = [[TLIconButton alloc] initWithFrame:CGRectMake(0, 0, multiSize * size.width, multiSize * size.height)];
    
    iconButton.isPhoto=isPhoto;

    [iconButton setColor:[UIColor whiteColor]];
    [iconButton setImage:icon forState:UIControlStateNormal];
    [iconButton setStyle:isPink? TLIconButtonStylePink : TLIconButtonStyleColor];
    
    
    if (!square)
        [iconButton setShape:ASShapeRhombus];
    
    if (!border)
        [iconButton setType:ASShapeButtonTypeNonborder];
    
    return [self imageWithView:iconButton size:size];
}

+ (UIImage *)imageWithGrayIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border {

    
    return [UIImage imageWithGrayIcon:icon size:size square:square border:border isPhoto:NO];
}

+ (UIImage *)imageWithGrayIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border isPhoto:(BOOL) isPhoto {
    TLIconButton *iconButton = [[TLIconButton alloc] initWithFrame:CGRectMake(0, 0, multiSize * size.width, multiSize * size.height)];
    
   
    

    iconButton.isPhoto=isPhoto;
    
    if (!square)
        [iconButton setShape:ASShapeRhombus];
    
    if (!border)
        [iconButton setType:ASShapeButtonTypeNonborder];
    
    [iconButton setImage:icon forState:UIControlStateNormal];
    [iconButton setStyle:TLIconButtonStyleGray];
    
    
    return [self imageWithView:iconButton size:size];
}

+ (UIImage *)imageWithWhiteIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border {

    
    return [UIImage imageWithWhiteIcon:icon size:size square:square border:border isPhoto:NO];
}

+ (UIImage *)imageWithWhiteIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border isPhoto:(BOOL)isPhoto {
    TLIconButton *iconButton = [[TLIconButton alloc] initWithFrame:CGRectMake(0, 0, multiSize * size.width, multiSize * size.height)];
    
    iconButton.isPhoto=isPhoto;
    
    [iconButton setImage:icon forState:UIControlStateNormal];
    [iconButton setStyle:TLIconButtonStyleWhite];
    
    
    
    if (!square)
        [iconButton setShape:ASShapeRhombus];
    
    if (!border)
        [iconButton setType:ASShapeButtonTypeNonborder];
    
    return [self imageWithView:iconButton size:size];
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, 4);
    
    CGContextSetInterpolationQuality(UIGraphicsGetCurrentContext(), kCGInterpolationHigh);

    [image drawInRect:CGRectIntegral(CGRectMake(0, 0, size.width, size.height))];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    

    
    return newImage;
}

+ (UIImage *)imageWithView:(UIView *)view size:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return [self imageWithImage:img scaledToSize:size];
}
+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border shadow:(BOOL)shadow  isPink:(BOOL) isPink{
    
    return [UIImage imageWithColorIcon:icon size:size square:square border:border shadow:shadow isPhoto:NO isPink:isPink];
    
}

+ (UIImage *)imageWithColorIcon:(UIImage *)icon size:(CGSize)size square:(BOOL)square border:(BOOL)border shadow:(BOOL)shadow isPhoto:(BOOL)isPhoto  isPink:(BOOL) isPink
{
    UIImage *withoutShadow=[self imageWithColorIcon:icon size:size square:square border:border isPhoto:isPhoto  isPink:(BOOL) isPink];
    
    if(shadow)
    {
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width*1.2,size.height*1.2),NO, 0);
        
        
        CGContextRef currentContext=UIGraphicsGetCurrentContext();
        // Create gradient
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGFloat locations[] = {0.0,0.4, 1.0};
        
        UIColor *centerColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
        UIColor *centerColor2 = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
        UIColor *edgeColor = [UIColor clearColor];
        
        NSArray *colors = [NSArray arrayWithObjects:(__bridge id)centerColor.CGColor, (__bridge id)centerColor2.CGColor, (__bridge id)edgeColor.CGColor, nil];
        CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)colors, locations);
        
        // Scaling transformation and keeping track of the inverse
        CGAffineTransform scaleT = CGAffineTransformMakeScale(2.5, 1.0);
        CGAffineTransform invScaleT = CGAffineTransformInvert(scaleT);
        
        // Extract the Sx and Sy elements from the inverse matrix
        // (See the Quartz documentation for the math behind the matrices)
        CGPoint invS = CGPointMake(invScaleT.a, invScaleT.d);
        
        // Transform center and radius of gradient with the inverse
        CGPoint center = CGPointMake((size.width*1.2 / 2) * invS.x, (size.height*1.1 ) * invS.y);
        CGFloat radius = (size.width*0.15) * invS.x;
        
        // Draw the gradient with the scale transform on the context
        CGContextScaleCTM(currentContext, scaleT.a, scaleT.d);
        CGContextDrawRadialGradient(currentContext, gradient, center, 0, center, radius, kCGGradientDrawsBeforeStartLocation);
        
        // Reset the context
        CGContextScaleCTM(currentContext, invS.x, invS.y);
        
        // Continue to draw whatever else ...
        
        // Clean up the memory used by Quartz
        CGGradientRelease(gradient);
        CGColorSpaceRelease(colorSpace);
        
        
        CGContextSaveGState(currentContext);
        
        CGContextSetShadow(currentContext, CGSizeZero, 10);
        [withoutShadow drawAtPoint:CGPointMake(size.width*0.1, size.height*0.1)];
        [withoutShadow drawAtPoint:CGPointMake(size.width*0.1, size.height*0.1)];
        CGContextRestoreGState(currentContext);
        
        
        
        /* UIImageView *imgView=[[UIImageView alloc]initWithImage:withoutShadow];
         
         CGRect ovalRect =CGRectMake(0, 0, 10, 50); //CGRectMake(size.width*(0.5-0.4/2), size.height - 4, size.width *0.4, 4);
         UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:ovalRect];
         [imgView.layer setShadowPath:[path CGPath]];
         
         imgView.layer.shadowColor = [UIColor blackColor].CGColor;
         imgView.layer.shadowOpacity = 0.7f;
         imgView.layer.shadowOffset = CGSizeZero;
         imgView.layer.shadowRadius = 2.0f;
         imgView.layer.masksToBounds = NO;
         
         
         
         
         
         CGContextTranslateCTM(currentContext, 0.1*size.width, 0.1*size.height);
         [imgView.layer renderInContext:currentContext];
         CGContextTranslateCTM(currentContext,  -0.1*size.width, -0.1*size.height);*/
        
        
        UIImage *withShadows=UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        
        return withShadows;
 
    } else {
        return withoutShadow;
    }
}

@end

//
//  LaunchScreenViewController.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "LaunchScreenViewController.h"

@interface LaunchScreenViewController ()

@end

@implementation LaunchScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
//#if DEBUG
//    [self performSegueWithIdentifier:segueToFirstViewController sender:self];    
//#endif
    
    CGRect newFrame = _logoImageView.frame;
    
    CGFloat newWidth=118;
    CGFloat screenWidth=[UIScreen mainScreen].bounds.size.width;
    
    
    newFrame.origin.y = 36;
    newFrame.origin.x = screenWidth/2 -5 -newWidth/2;
    newFrame.size.height = 40;
    newFrame.size.width = 118;
    
    [UIView animateWithDuration:0.75f delay:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [_logoImageView setFrame:newFrame];
    } completion:^(BOOL finished) {
        [self performSegueWithIdentifier:segueToFirstViewController sender:self];
    }];
}

@end

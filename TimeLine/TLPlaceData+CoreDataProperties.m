//
//  TLPlaceData+CoreDataProperties.m
//  TimeLine
//
//  Created by Evgenii Oborin on 27.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLPlaceData+CoreDataProperties.h"

@implementation TLPlaceData (CoreDataProperties)

+ (NSFetchRequest<TLPlaceData *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TLPlaceData"];
}

@dynamic id;
@dynamic name;
@dynamic distance;
@dynamic address;
@dynamic rating;

@end

//
//  TLTimelineCollectionCell.m
//  TimeLine
//
//  Created by Evgenii Oborin on 29.09.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLTimelineCollectionCell.h"
#import "Constants.h"

@interface TLTimelineCollectionCell ()




@end

@implementation TLTimelineCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.borderView.layer.cornerRadius=4;
    self.borderView.layer.borderWidth=2;
    self.borderView.layer.borderColor=dThemeColorGray.CGColor;
    

}

- (void) showDeleteButtonAnimated:(BOOL)animated{
    if(!animated){
        self.deleteCellButton.alpha=1;
    } else {
        [UIView animateWithDuration:0.1 animations:^{
            self.deleteCellButton.alpha=1;
        }];
    }
}

- (void) hideDeleteButtonAnimated:(BOOL)animated{
    if(!animated){
        self.deleteCellButton.alpha=0;
    } else {
        [UIView animateWithDuration:0.1 animations:^{
            self.deleteCellButton.alpha=0;
        }];
    }
}

- (void) startWiggling {
    if ([self.layer animationForKey:@"wiggle"])  return;
    if ([self.layer animationForKey:@"bounce"])  return;
    
    CGFloat angle = 0.04;
    
    CAKeyframeAnimation *wiggle = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    wiggle.values = @[@(-angle),@(angle)];
    
    wiggle.autoreverses = YES;
    wiggle.duration = [self randomInterval:0.1 variance:0.025];
    wiggle.repeatCount = INFINITY;
    
    [self.layer addAnimation:wiggle forKey: @"wiggle" ];
    
    CAKeyframeAnimation *bounce = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.y"];
    bounce.values = @[@4.0, @0.0];
    
    bounce.autoreverses = YES;
    bounce.duration = [self randomInterval:0.12 variance:0.025];
    bounce.repeatCount = INFINITY;
    
    [self.layer addAnimation:bounce forKey: @"bounce" ];
}

- (void) stopWiggling {
    [self.layer removeAllAnimations];
    [self hideDeleteButtonAnimated:NO];
    
}

- (void) stopWigglingAnimated {
    
    if(!([self.layer animationForKey:@"wiggle"] || [self.layer animationForKey:@"bounce"])) return;
    
    [self.layer removeAllAnimations];
    
    CALayer *currentLayer = self.layer.presentationLayer;
    self.layer.transform = currentLayer.transform;
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.layer.transform = CATransform3DIdentity;
                     }
                     completion:NULL];
    
    
}

- (NSTimeInterval) randomInterval: (NSTimeInterval)interval variance:(double) variance{
    return interval + variance * ((arc4random_uniform(1000) - 500.0) / 500.0);
}

- (IBAction)deleteButtonTapped:(id)sender {
    
    [self.delegate didTappedDeleteAtCell:self];
}





@end

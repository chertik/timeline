//
//  AppDelegate.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 04/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "DatabaseAPI.h"
#import "TLDelayedCheckin.h"
#import "TLGeoManager.h"
#import <HockeySDK/HockeySDK.h>
#import "TLCommonPlace.h"
#import "TLPlaceData+CoreDataClass.h"
#import "TLThemeManager.h"
#import "TLPhotoImporter.h"
#import <VK_ios_sdk/VKSdk.h>

@import FBSDKCoreKit;
@import SVProgressHUD;
@import GoogleMaps;
@import GooglePlaces;
@import CoreLocation;

@interface AppDelegate () 

//@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) UIAlertView *notificationAlert;

@end

@implementation AppDelegate



#pragma mark - entry/exit methods

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{
                                                              kUserdefaultsFilterSettingsAccuracyThreshold:@(20),
                                                              kUserdefaultsFilterSettingsDistanceThreshold:@(0),
                                                              kUserdefaultsFilterSettingsSpideringfilterDistanceFromVisit:@(60),
                                                              kUserdefaultsFilterSettingsLowPassInnertiaCoeefincy:@(0.5),
                                                              sSelectedThemeKey:@(TLThemeOcher),
                                                              kUserdefaultsPassedAuthorization:@(0)
                                                              }];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
//    UIView *statusBackground=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 1, 1)];
//    statusBackground.backgroundColor=[TLThemeManager statusBarBackgroundColor];
//    statusBackground.translatesAutoresizingMaskIntoConstraints=NO;
//    
//    UIWindow *window=self.window;
//    [window addSubview:statusBackground];
//    NSDictionary *views=@{
//                          @"view":statusBackground
//                          };
//    NSArray *hLayoutConstraints=[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:views];
//    NSArray *vLayoutConstraints=[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view(20)]" options:0 metrics:nil views:views];
//    [window addConstraints:hLayoutConstraints];
//    [window addConstraints:vLayoutConstraints];
    
    NSNumber *routeTrackSetting=[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsRouteTrackingEnabled];
    if(!routeTrackSetting){
        routeTrackSetting=@(YES);
        [[NSUserDefaults standardUserDefaults] setObject:routeTrackSetting forKey:kSettingsRouteTrackingEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSNumber *loggingEnabledSetting=[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsLoggingEnabled];
    if(!loggingEnabledSetting){
        loggingEnabledSetting=@(NO);
        [[NSUserDefaults standardUserDefaults] setObject:loggingEnabledSetting forKey:kSettingsLoggingEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSNumber *wakingHackIsEnabledSetting=[[NSUserDefaults standardUserDefaults] objectForKey:kDefaultsAppWakingHackIsEnabled];
    if(!wakingHackIsEnabledSetting){
        wakingHackIsEnabledSetting=@(YES);
        [[NSUserDefaults standardUserDefaults] setObject:wakingHackIsEnabledSetting forKey:kDefaultsAppWakingHackIsEnabled];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound|UIUserNotificationTypeAlert|UIUserNotificationTypeBadge categories:nil]];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [[NSUserDefaults standardUserDefaults] setObject:@(TLPlacesAPITypeFoursquare) forKey:kPlacesAPIType];
  
    [GMSServices provideAPIKey:kGoogleMapsKey];
    [GMSPlacesClient provideAPIKey:kGoogleMapsKey];
    [Fabric with:@[[Crashlytics class]]];
//    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"58a83929c3b341e9b2e79bb1eef4ff1d"];
//    [[BITHockeyManager sharedHockeyManager] startManager];
//    [[BITHockeyManager sharedHockeyManager].authenticator
//     authenticateInstallation];
    
    [TLGeoManager sharedManager];
    [DatabaseAPI sharedAPI];
    [SVProgressHUD setMinimumDismissTimeInterval:2];
//    [[DatabaseAPI sharedAPI] cleanUpDatabase];
//    NSArray *array = [TLUserTask MR_findAll];
  /*if (array.count == 0 || array.count == 1) {
        [[DatabaseAPI sharedAPI] cleanUpDatabase];
        
        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
            
        
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd-MM"];
            [formatter setDefaultDate:[NSDate date]];
            
            NSInteger ids = 1;
            
            TLUserTask *userTaskMag = [TLUserTask MR_createEntityInContext:localContext];
            
            
            userTaskMag.id = @(ids++);
            userTaskMag.latitude = @(59.876970f);
            userTaskMag.longitude = @(30.392677f);
            userTaskMag.desc = @"Магазин";
            userTaskMag.icon_path = @"atm";
            userTaskMag.is_visible = @1;
            userTaskMag.is_timeline = @1;
            userTaskMag.is_past = @1;
            userTaskMag.datetime=[formatter dateFromString:@"14-09"];
            
            TLUserTask *userTaskBank = [TLUserTask MR_createEntityInContext:localContext];

            userTaskBank.id = @(ids++);
            userTaskBank.latitude = @(59.851457f);
            userTaskBank.longitude = @(30.247845f);
            userTaskBank.desc = @"Банкомат";
            userTaskBank.icon_path = @"atm";
            userTaskBank.is_visible = @1;
            userTaskBank.is_timeline = @1;
            userTaskBank.is_past = @1;
            userTaskBank.datetime=[formatter dateFromString:@"16-09"];
            
            TLUserTask *userTaskPhoto = [TLUserTask MR_createEntityInContext:localContext];
            
            userTaskPhoto.id = @(ids++);
            userTaskPhoto.latitude = @(59.867335f);
            userTaskPhoto.longitude = @(30.247163f);
            userTaskPhoto.desc = @"Фото из отпуска";
            userTaskPhoto.icon_path = @"photo";
            userTaskPhoto.is_visible = @1;
            userTaskPhoto.is_timeline = @1;
            userTaskPhoto.is_past = @1;
            userTaskPhoto.datetime=[formatter dateFromString:@"09-10"];
            
            TLUserTask *userTaskHolid = [TLUserTask MR_createEntityInContext:localContext];
            
            userTaskHolid.id = @(ids++);
            userTaskHolid.latitude = @(59.867335f);
            userTaskHolid.longitude = @(30.247163f);
            userTaskHolid.desc = @"Отпуск";
            userTaskHolid.icon_path = @"holiday";
            userTaskHolid.is_visible = @1;
            userTaskHolid.is_timeline = @1;
            userTaskHolid.is_past = @1;
            userTaskHolid.datetime=[formatter dateFromString:@"10-10"];
            
            
            TLUserTask *userTaskMag2 = [TLUserTask MR_createEntityInContext:localContext];
            
            userTaskMag2.id = @(ids++);
            userTaskMag2.latitude = @(59.868235f);
            userTaskMag2.longitude = @(30.392377f);
            userTaskMag2.desc = @"Магазин";
            userTaskMag2.icon_path = @"atm";
            userTaskMag2.is_visible = @1;
            userTaskMag2.is_timeline = @1;
            userTaskMag2.datetime=[formatter dateFromString:@"14-11"];
            
            TLUserTask *userTask1 = [TLUserTask MR_createEntityInContext:localContext];
            
            userTask1.id = @(ids++);
            userTask1.latitude = @(59.9359194f);
            userTask1.longitude = @(30.3109965f);
            userTask1.desc = @"Банкомат";
            userTask1.icon_path = @"atm";
            userTask1.route_id = @7;
            userTask1.is_visible = @1;
            userTask1.is_timeline = @1;
            userTask1.datetime=[formatter dateFromString:@"02-12"];
            
            TLUserTask *userTask2 = [TLUserTask MR_createEntityInContext:localContext];
            
            userTask2.id = @(ids++);
            userTask2.latitude = @(59.932711f);
            userTask2.longitude = @(30.3457113f);
            userTask2.desc = @"Встреча с партнерами";
            userTask2.icon_path = @"cafe";
            userTask2.route_id = @8;
            userTask1.is_visible = @1;
            userTask1.is_timeline = @1;
            userTask2.datetime=[formatter dateFromString:@"04-12"];
            
            TLUserTask *userTask3 = [TLUserTask MR_createEntityInContext:localContext];
            
            userTask3.id = @(ids++);
            userTask3.latitude = @(59.937679f);
            userTask3.longitude = @(30.355070f);
            userTask3.desc = @"Отпуск";
            userTask3.icon_path = @"holiday";
            userTask3.route_id = @4;
            userTask3.is_visible = @1;
            userTask3.is_timeline = @1;
            userTask3.datetime=[formatter dateFromString:@"04-12"];
            
            
            
            TLUserTask *userTask4 = [TLUserTask MR_createEntityInContext:localContext];
            
            userTask4.id = @(ids++);
            userTask4.latitude = @(59.949979f);
            userTask4.longitude = @(30.369970f);
            userTask4.desc = @"Прогулка с Олей";
            userTask4.photo_path = @"photoic128-3.jpg";
            userTask4.icon_path = @"photoic128-3.jpg";
            userTask4.route_id = @4;
            userTask4.is_visible = @1;
            userTask4.is_timeline = @1;
            userTask4.datetime=[formatter dateFromString:@"10-12"];
            
            TLUserTask *userTask5 = [TLUserTask MR_createEntityInContext:localContext];
            
            userTask5.id = @(ids++);
            userTask5.latitude = @(59.939279f);
            userTask5.longitude = @(30.389070f);
            userTask5.desc = @"Кафе";
            userTask5.icon_path = @"cafe";
            userTask5.route_id = @4;
            userTask5.is_visible = @1;
            userTask5.is_timeline = @1;
            userTask5.datetime=[formatter dateFromString:@"11-12"];
            
            TLUserTask *userTask6 = [TLUserTask MR_createEntityInContext:localContext];
            
            userTask6.id = @(ids++);
            userTask6.latitude = @(59.949679f);
            userTask6.longitude = @(30.389070f);
            userTask6.desc = @"Отпуск";
            userTask6.icon_path = @"holiday";
            userTask6.route_id = @4;
            userTask6.is_visible = @1;
            userTask6.is_timeline = @1;
            userTask6.datetime=[formatter dateFromString:@"15-12"];
                     
        }];
        
        
        
    }*/
    
    
    //CHECK FOR past tasks
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        
        NSPredicate *predicate=[NSPredicate predicateWithFormat:@"is_past = %@",@0];
        NSArray *futureTasks=[TLUserTask MR_findAllSortedBy:@"id" ascending:YES withPredicate:predicate inContext:localContext];
        
        for(int i=0; i<futureTasks.count; i++){
            TLUserTask *task=futureTasks[i];
            if ([task.datetime timeIntervalSinceNow]<=0) {
                task.is_past=@1;
            } else{
                break;
            }
        }
    }];
    
    /*
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
       TLDelayedCheckin *delayed=[TLDelayedCheckin MR_createEntityInContext:localContext];
        delayed.time=[NSDate date];
        delayed.latitude=@(59.949679f);
        delayed.longitude=@(30.389070f);
      
        TLPlaceData *placeData=[TLPlaceData MR_createEntityInContext:localContext];
        placeData.name=@"blablabla";
        placeData.address=@"ddsds";
        placeData.distance=@(100);
        NSOrderedSet *set=[[NSOrderedSet alloc] initWithObjects:placeData, nil];
        delayed.placesList=set;
        delayed.isDefined=@1;
        
//        TLDelayedCheckin *delayed2=[TLDelayedCheckin MR_createEntityInContext:localContext];
//        delayed2.time=[NSDate date];
//        delayed2.latitude=@(59.944679f);
//        delayed2.longitude=@(30.385070f);
//        
//        TLDelayedCheckin *delayed3=[TLDelayedCheckin MR_createEntityInContext:localContext];
//        delayed3.time=[NSDate date];
//        delayed3.latitude=@(59.944659f);
//        delayed3.longitude=@(30.385370f);
//        
//        TLDelayedCheckin *delayed4=[TLDelayedCheckin MR_createEntityInContext:localContext];
//        delayed4.time=[NSDate date];
//        delayed4.latitude=@(59.949679f);
//        delayed4.longitude=@(30.389070f);
//        
//        TLPlaceData *placeData4=[TLPlaceData MR_createEntityInContext:localContext];
//        placeData4.name=@"blablabla";
//        placeData4.address=@"ddsds";
//        placeData4.distance=@(100);
//        NSOrderedSet *set4=[[NSOrderedSet alloc] initWithObjects:placeData4, nil];
//        delayed4.placesList=set4;
//        delayed4.isDefined=@1;


        
    }];*/
    
    NSLog(@"did finish launch");
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog(@"applicationWillResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.


    NSNumber *routeTrackingEnabled=[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsRouteTrackingEnabled];
    if(!routeTrackingEnabled.boolValue){
        [[TLGeoManager sharedManager] stopUpdatingLocations];
    }
    
    [[TLGeoManager sharedManager] saveCurrentRouteAndSettings];
    NSLog(@"applicationDidEnterBackground");
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[TLGeoManager sharedManager] startUpdatingLocations];
    
    NSNumber *routeTrackingEnabled=[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsRouteTrackingEnabled];
    if(routeTrackingEnabled.boolValue){
        [[TLGeoManager sharedManager] enableRouteTracking];
    }
    

    NSLog(@"applicationWillEnterForeground");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[TLGeoManager sharedManager] startUpdatingLocations];
    [[TLPhotoImporter sharedManager] checkForNewPhotosAndImport];
    
    NSNumber *routeTrackingEnabled=[[NSUserDefaults standardUserDefaults] objectForKey:kSettingsRouteTrackingEnabled];
    if(routeTrackingEnabled.boolValue){
        [[TLGeoManager sharedManager] enableRouteTracking];
    }
    NSLog(@"applicationDidBecomeActive");
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//    [[TLGeoManager sharedManager] cancelCurrentRouteTracking];
}

//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
//    // Play a sound and show an alert only if the application is active, to avoid doubly notifiying the user.
//    if ([application applicationState] == UIApplicationStateActive) {
//        // Initialize the alert view.
//        if (!_notificationAlert) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
//                                                            message:nil
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"Ok"
//                                                  otherButtonTitles:nil];
//            [self setNotificationAlert:alert];
//        }
//        
//        
//        // Set the title of the alert with the notification's body.
//        [_notificationAlert setTitle:[notification alertBody]];
//        
//        // Play the sound and show the alert.
//        [self.notificationAlert show];
//    }
//}

//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
//    [VKSdk processOpenURL:url fromApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]];
//    return YES;
//}
    
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
    [VKSdk processOpenURL:url fromApplication:sourceApplication];
    return YES;
} 
    

@end

//
//  UIImage+Additional.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 10/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Additional)

- (UIImage *)filledWithColor:(UIColor *)color;

+ (UIImage *)filledImageFrom:(UIImage *)image withColor:(UIColor *)color;

@end

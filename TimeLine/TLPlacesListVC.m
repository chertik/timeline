//
//  TLPlacesListVC.m
//  TimeLine
//
//  Created by Evgenii Oborin on 26.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//



#import "TLPlacesListVC.h"
#import "TLPlacesListCell.h"
#import "TLPlaceData+CoreDataClass.h"
#import "TLThemeManager.h"


#define midPinkColor [UIColor colorWithRed:223/255.0 green:122/255.0 blue:143/255.0 alpha:1]

@interface TLPlacesListVC () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIView *topGradientView;
@property (strong, nonatomic) NSOrderedSet *placesList;

@end

@implementation TLPlacesListVC

static NSString* const reuseID=@"TLPlacesListCommonCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    //Setting Gradient To TopView
    CAGradientLayer *topGradient=[[CAGradientLayer alloc] init];
    topGradient.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.topGradientView.bounds.size.height);
    topGradient.colors=[NSArray arrayWithObjects:(id)[TLThemeManager lighterGrayColor].CGColor, (id) dThemeColorRed.CGColor, nil];
    topGradient.startPoint=CGPointMake(0,0.5);
    topGradient.endPoint=CGPointMake(1, 0.5);
    topGradient.locations=@[@0,@1];
    self.topGradientView.backgroundColor=[UIColor clearColor];
    [self.topGradientView.layer insertSublayer:topGradient atIndex:0];
    
    //Blur view setup
    
    CGRect bounds=self.view.bounds;
    bounds.size.width=[UIScreen mainScreen].bounds.size.width;
    bounds.size.height=[UIScreen mainScreen].bounds.size.height;
    self.view.bounds=bounds;
    
    Class customClass=NSClassFromString(@"_UICustomBlurEffect");
    UIBlurEffect *customBlurEffect=[[customClass alloc] init];
    [customBlurEffect setValue:@2.5 forKey:@"blurRadius"];
    UIVisualEffectView *blurView=[[UIVisualEffectView alloc] initWithEffect:customBlurEffect];
    blurView.frame=self.view.bounds;
    [self.view insertSubview:blurView atIndex:0];

    //Setups for table view
    self.tableView.backgroundView.backgroundColor=[UIColor clearColor];
    self.tableView.backgroundColor=[UIColor clearColor];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setCurrentTask:(TLUserTask *)currentTask{
    
    _currentTask=currentTask;
    self.placesList=currentTask.placesList;
    

}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.placesList.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TLPlacesListCell *cell=[tableView dequeueReusableCellWithIdentifier:reuseID];
    
    TLPlaceData *placeData=self.placesList[indexPath.row];
    
    cell.nameLabel.text=placeData.name;
    cell.addressLabel.text=placeData.address;
    cell.distanceLabel.text=[NSString stringWithFormat:@"%.0f м.",placeData.distance.doubleValue];
    
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TLPlaceData *placeData=self.placesList[indexPath.row];
    [self.currentTask changePlaceTo:placeData];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kTLPlacesListChangedPlaceNotification object:nil];
        [self.delegate animateAppearanceOfBars];
        [self dismissViewControllerAnimated:YES completion:nil];
    });
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

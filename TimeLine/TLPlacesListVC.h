//
//  TLPlacesListVC.h
//  TimeLine
//
//  Created by Evgenii Oborin on 26.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLUserTask;

@protocol TLPlacesListVCDelegate <NSObject>

- (void) animateAppearanceOfBars;

@end

@interface TLPlacesListVC : UIViewController

@property (strong,nonatomic) TLUserTask *currentTask;

@property (weak,nonatomic) id<TLPlacesListVCDelegate> delegate;

@end

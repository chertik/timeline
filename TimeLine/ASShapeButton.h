//
//  ASShapeButton.h
//  TimeLine
//
//  Created by Shagaleev Alexey on 10/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ASShape) {
    ASShapeNone,
    ASShapeSquare,
    ASShapeRhombus
};

typedef NS_ENUM(NSInteger, ASShapeButtonType) {
    ASShapeButtonTypeNone,
    ASShapeButtonTypeBorder,
    ASShapeButtonTypeNonborder
};

@interface ASShapeButton : UIButton

@property (nonatomic, retain) UIColor *color;
@property (nonatomic, readonly) ASShape shape;
@property (nonatomic, readonly) ASShapeButtonType type;
@property (nonatomic) BOOL isPhoto;

- (void)setShape:(ASShape)shape;
- (void)setType:(ASShapeButtonType)type;

@end

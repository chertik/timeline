//
//  TLPlaceData+CoreDataClass.h
//  TimeLine
//
//  Created by Evgenii Oborin on 27.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class TLCommonPlace;

@interface TLPlaceData : NSManagedObject

+ (TLPlaceData*) createPlaceDataWithCommonPlace:(TLCommonPlace*)place inContext:(NSManagedObjectContext*)context;
+ (TLPlaceData*) placeDataWithId:(NSNumber*) id ;
+ (TLPlaceData*) placeDataWithId:(NSNumber*) id inContext:(NSManagedObjectContext*) context;

@end

NS_ASSUME_NONNULL_END

#import "TLPlaceData+CoreDataProperties.h"

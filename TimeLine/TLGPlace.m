//
//  TLGPlace.m
//  TimeLine
//
//  Created by Evgenii Oborin on 22.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLGPlace.h"
//#import <RestKit/RestKit.h>

@import RestKit;

@implementation TLGPlaceAdressComponentType

- (NSString*) description {
    return self.componentType;
}

- (BOOL) isEqualToType:(NSString*) type {
    return [self.componentType isEqualToString:type];
}

@end

@implementation TLGPlaceAdressComponent

- (NSString*) description {
    NSString *typeStr=@"[";
    for(TLGPlaceAdressComponentType *type in self.types){
        typeStr=[typeStr stringByAppendingString:[NSString stringWithFormat:@"%@,",type]];
    }
    typeStr=[typeStr stringByAppendingString:@"]"];

    return [NSString stringWithFormat:@"%@ types:%@",self.shortName,typeStr];
}

- (BOOL) containtsType:(NSString*) type{
    for(TLGPlaceAdressComponentType *compType in self.types){
        if ([compType isEqualToType:type]){
            return YES;
        }
    }
    return NO;
}

@end

@implementation TLGPlaceDetails

- (NSString*) description {
//    NSString *comps=@"[";
//    for(TLGPlaceAdressComponent *adrC in self.addressComponents){
//        comps=[comps stringByAppendingString:[NSString stringWithFormat:@"%@\n",adrC]];
//    }
//    comps=[comps stringByAppendingString:@"]"];
    return [NSString stringWithFormat:@"rating:%f shortAdr:%@",self.rating.floatValue,[self shortAddress]];
}

- (TLGPlaceAdressComponent*) componentOfType:(NSString*) type{
    for(TLGPlaceAdressComponent *component in self.addressComponents){
        if([component containtsType:type]){
            return component;
        }
    }
    return nil;
}


- (NSString *) shortAddress{

    NSString *shortAddress;
    
    TLGPlaceAdressComponent *routeComp=[self componentOfType:@"route"];
    if(routeComp){
        TLGPlaceAdressComponent *streetNumber=[self componentOfType:@"street_number"];
        shortAddress=[NSString stringWithFormat:@"%@, %@",routeComp.shortName,streetNumber.shortName];
        return shortAddress;
    }
    
    TLGPlaceAdressComponent *premiseComp=[self componentOfType:@"premise"];
    if(premiseComp){
        return premiseComp.longName;
    }
    return self.formattedAddress;
}

@end

@implementation TLGPlace

- (CLLocation*) location{
    CLLocation *location=[[CLLocation alloc] initWithLatitude:self.lat.doubleValue longitude:self.lon.doubleValue];
    return location;
}

- (NSString*) description{
    return [NSString stringWithFormat:@"%@, dist:%f m. Details:%@",self.name,self.distance.doubleValue,self.details];
}

@end

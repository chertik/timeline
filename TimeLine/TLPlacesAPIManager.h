//
//  TLPlacesAPIManager.h
//  TimeLine
//
//  Created by Evgenii Oborin on 25.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLPlacesAPIType.h"

@class TLCommonPlace;
@class TLGPlace;

@interface TLPlacesAPIManager : NSObject

+ (instancetype) sharedManager;

- (void) getPlacesForCoordinates:(CLLocationCoordinate2D) coord withAPI:(TLPlacesAPIType)type withCompletion:(void (^)(NSArray<TLCommonPlace*> *places))completionBlock failure:(void(^)(NSError *error))failureBlock;


- (void) getGoogleAddressForLocation:(CLLocation*)location withCompletion:(void (^)(NSString *address))completionBlock failure:(void(^)(NSError *error))failureBlock;

@end

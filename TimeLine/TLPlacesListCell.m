//
//  TLPlacesListCell.m
//  TimeLine
//
//  Created by Evgenii Oborin on 27.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLPlacesListCell.h"
#import "TLThemeManager.h"

@interface TLPlacesListCell ()

@property (weak, nonatomic) IBOutlet UIView *gradientDividerView;

@end

@implementation TLPlacesListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CAGradientLayer *gradLayer=[CAGradientLayer layer];
    gradLayer.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.gradientDividerView.bounds.size.height);
    gradLayer.colors=@[(id)[TLThemeManager lighterGrayColor].CGColor,(id)dThemeColorRed.CGColor];
    gradLayer.startPoint=CGPointMake(0, 0);
    gradLayer.endPoint=CGPointMake(1, 0);
    
    self.gradientDividerView.backgroundColor=[UIColor clearColor];
    [self.gradientDividerView.layer addSublayer:gradLayer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
 
    [UIView animateWithDuration:0.1 animations:^{
        self.backgroundColor=selected ? [[UIColor whiteColor] colorWithAlphaComponent:0.3] : [UIColor clearColor];
    }];

}


@end

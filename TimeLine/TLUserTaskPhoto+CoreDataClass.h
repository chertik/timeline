//
//  TLUserTaskPhoto+CoreDataClass.h
//  TimeLine
//
//  Created by Evgenii Oborin on 04.11.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSObject;

NS_ASSUME_NONNULL_BEGIN

@interface TLUserTaskPhoto : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TLUserTaskPhoto+CoreDataProperties.h"

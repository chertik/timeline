//
//  TLAddToMapList.h
//  TimeLine
//
//  Created by Evgenii Oborin on 29.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TLUserTask;

@protocol TLAddToMapListDelegate <NSObject>

- (void) didClearedAllTasks;

- (void) refreshMapData;

@end

@interface TLAddToMapList : UICollectionViewController

@property (weak,nonatomic) id<TLAddToMapListDelegate> delegate;

- (void) addTask:(TLUserTask*) task;
- (void) clearTasks;
- (BOOL) removerRecentTask;

@end

//
//  TLDetailVC.m
//  TimeLine
//
//  Created by Evgenii Oborin on 19.10.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "TLDetailVC.h"
#import "TLUserTask.h"
#import "UIImage+TLIcon.h"
#import "UIButton+hitTestInsets.h"
#import "TLGeoManager.h"
#import "TLThemeManager.h"
//#import <SVProgressHUD.h>

@import SVProgressHUD;

@interface TLDetailVC () <UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

#define dImageAutocheckin [UIImage imageNamed:@"AutocheckinGray"]
#define dImageManualcheckin [UIImage imageNamed:@"ManualChekin"]
#define dImageMakePhoto [UIImage imageNamed:@"MakePhoto"]
#define dImageLock [UIImage imageNamed:@"GrayLock"]

@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *changePlaceButton;
@property (weak, nonatomic) IBOutlet UIButton *hideTaskButton;
@property (weak, nonatomic) IBOutlet UIButton *checkinButton;
@property (weak, nonatomic) IBOutlet UITextField *descriptionField;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIButton *backArrowButton;
@property (weak, nonatomic) IBOutlet UIView *commentaryView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *manualCheckinIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingCurrentAddressIndicator;

@property (weak, nonatomic) IBOutlet UIButton *makePhotoButton;

@property (weak,nonatomic) TLUserTask *currentTask;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descripionLabelTrailingConstr;

@end

@implementation TLDetailVC
{
    BOOL loadingCheckin;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    //Gradient border
    self.contentView.layer.cornerRadius=8;
    
    CAGradientLayer *gradient=[CAGradientLayer layer];
    [gradient setFrame:self.contentView.bounds];
    gradient.colors=@[(id) [TLThemeManager lighterGrayColor].CGColor,(id)[TLThemeManager gradientIntermediateColor].CGColor, (id) [TLThemeManager mainAccentColor].CGColor];
    gradient.locations=@[@0,@0.7,@1];
    gradient.startPoint=CGPointMake(0, 0);
    gradient.endPoint=CGPointMake(1, 0);
    
    CAShapeLayer *shapeLayer =[[CAShapeLayer alloc] init];
    shapeLayer.lineWidth = 2*2;
    shapeLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.contentView.bounds cornerRadius:8].CGPath;
    shapeLayer.fillColor = nil;
    shapeLayer.strokeColor = [UIColor blackColor].CGColor;
    gradient.mask = shapeLayer;
    
//    CGFloat borderWidth=2;
//    
//    CALayer *solidLayer=[CALayer layer];
//    solidLayer.backgroundColor=[UIColor whiteColor].CGColor;
//    solidLayer.frame=CGRectMake(borderWidth, borderWidth, gradient.bounds.size.width-borderWidth*2, gradient.bounds.size.height-borderWidth*2);
//    solidLayer.cornerRadius=6;
    
//    [self.contentView.layer insertSublayer:solidLayer atIndex:0];
    [self.contentView.layer insertSublayer:gradient atIndex:0];

    
    
    [self.backArrowButton setHitTestEdgeInsets:UIEdgeInsetsMake(-98, -8, -98, -10)];
    self.commentaryView.layer.cornerRadius=4;
    self.iconView.layer.cornerRadius=8;
    self.iconView.clipsToBounds=YES;
    
    loadingCheckin=NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedPlace:) name:kTLPlacesListChangedPlaceNotification object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCurrentAddress:) name:kGeoManagerUpdatedCurrentAddress object:nil];
}

- (void) changeCurrentAddress:(NSNotification*) note{
    if(!self.currentTask){
        self.descriptionField.text=[[TLGeoManager sharedManager] currentAddress];
        self.descripionLabelTrailingConstr.constant=8+20+8;
        self.loadingCurrentAddressIndicator.alpha=0;
        self.descriptionField.alpha=1;
    }
}

- (void) keyboardWasShown:(NSNotification*) aNotification{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [self.delegate keyBoardWillShowWithHeight:kbSize.height];
}

- (void) keyboardWillBeHidden:(NSNotification*) aNotification{
    [self.delegate keyBoardWillBeHidden];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (TLUserTask*) currentTask{
    return _currentTask;
}

- (void) setUserTask:(TLUserTask *)userTask {
    self.currentTask=userTask;
    
    if(userTask){
        self.descriptionField.text=userTask.desc;
        [userTask iconForSize:CGSizeMake(60, 60) callback:^(UIImage * _Nonnull image) {
            [self.iconView setImage:image];
        }];
        
        if(userTask.is_autocheckin.boolValue){
            [self changeCheckinButtonImage:dImageAutocheckin red:userTask.is_checked animated:NO];
        } else {
            [self changeCheckinButtonImage:dImageManualcheckin red:userTask.is_checked animated:NO];
        }
        
        [self.checkinButton setUserInteractionEnabled:!userTask.is_checked];
        
        [self changeHideButtonImageToRed:!userTask.is_visible.boolValue];
        if(userTask.taskPhoto){
    
            [self.delegate setDetailPhoto: userTask.taskPhoto];
            [self changeMakePhotoButtonToChecked:YES animated:NO];
        } else {
            [self changeMakePhotoButtonToChecked:NO animated:NO];
            [self.delegate setDetailPhoto:nil];
        }
        
        self.manualCheckinIndicator.hidden=YES;
        
        loadingCheckin=NO;
        self.changePlaceButton.hidden=NO;
        self.makePhotoButton.enabled=YES;
        self.hideTaskButton.enabled=YES;
        self.loadingCurrentAddressIndicator.alpha=0;
        
        
        if(userTask.placesList.count==0){
            self.changePlaceButton.hidden=YES;
            self.descripionLabelTrailingConstr.priority=UILayoutPriorityDefaultHigh;
            [self.view layoutIfNeeded];
        } else{
            self.changePlaceButton.hidden=NO;
            self.descripionLabelTrailingConstr.priority=UILayoutPriorityDefaultLow;
            [self.view layoutIfNeeded];
        }
    } else {
        
        self.changePlaceButton.hidden=YES;
        self.makePhotoButton.enabled=NO;
        self.hideTaskButton.enabled=NO;
        [self.checkinButton setUserInteractionEnabled:YES];
        
        NSString *currentAddress=[TLGeoManager sharedManager].currentAddress;
        if(currentAddress){
            self.descriptionField.text=[TLGeoManager sharedManager].currentAddress;
            self.loadingCurrentAddressIndicator.alpha=0;
            self.descriptionField.alpha=1;
            self.descripionLabelTrailingConstr.constant=8;
        } else {
            self.descriptionField.text=@"Получение адресса..";
            self.descriptionField.alpha=0.6;
            self.loadingCurrentAddressIndicator.alpha=1;
            self.descripionLabelTrailingConstr.constant=8+20+8;
        }
        
        [self.iconView setImage:[UIImage imageNamed:@"CurAddrIcAltrntv"]];
        
        self.descripionLabelTrailingConstr.priority=UILayoutPriorityDefaultHigh;
        [self.view layoutIfNeeded];
        
        [self changeCheckinButtonImage:dImageManualcheckin red:NO animated:NO];
        
    }
    
    
}

- (IBAction)popBack:(id)sender {
    [self.delegate popBack];
    [self acceptNewNameAndDismissKeyboard];
}

- (void) acceptNewNameAndDismissKeyboard{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        TLUserTask *taskInCont=[TLUserTask userTaskWithId:self.currentTask.id inContext:localContext];
        taskInCont.desc=self.descriptionField.text;
    }];
    
    [self dismissKeyboard];
}

- (void) dismissKeyboard{
    [self.descriptionField resignFirstResponder];
    [self.descriptionField setUserInteractionEnabled:NO];
}

- (void) changedPlace:(NSNotification*)notification{
    
    self.descriptionField.text=self.currentTask.desc;
    
}

- (IBAction) changePlace:(id)sender {
    [self.delegate showChangePlaceForTask:self.currentTask];

}

- (IBAction)rename:(id)sender {
    [self.descriptionField setUserInteractionEnabled:YES];
    [self.descriptionField becomeFirstResponder];
}


- (void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) changeMakePhotoButtonToChecked:(BOOL) isChecked{
    [self changeMakePhotoButtonToChecked:isChecked animated:YES];
}
- (void) changeMakePhotoButtonToChecked:(BOOL) isChecked animated:(BOOL)animated{
    [UIView animateWithDuration: animated ? 0.2 : 0.0 animations:^{
        [self.makePhotoButton setImage:isChecked?dImageAutocheckin : dImageMakePhoto forState:UIControlStateNormal];
        [self.makePhotoButton setTintColor:isChecked ? dThemeColorRed : dThemeColorGray];

    }];

}

- (void) changeCheckinButtonImage:(UIImage*) image red:(BOOL)isRed{
    [self changeCheckinButtonImage:image red:isRed animated:YES];
}


- (void) changeCheckinButtonImage:(UIImage*) image red:(BOOL)isRed animated:(BOOL)animated{
    
    [UIView animateWithDuration: animated ? 0.2 : 0.0 animations:^{
        if(image){
            [self.checkinButton setImage:image forState:UIControlStateNormal];
        }
        [self.checkinButton setTintColor:isRed ? dThemeColorRed : dThemeColorGray];
    }];
}

- (void) changeHideButtonImageToRed:(BOOL)isRed{
    [self changeHideButtonImageToRed:isRed animated:YES];
}

- (void) changeHideButtonImageToRed:(BOOL)isRed animated:(BOOL)animated{
    
    [UIView animateWithDuration: animated ? 0.2 : 0.0 animations:^{
        [self.hideTaskButton setTintColor:isRed ? dThemeColorRed : dThemeColorGray];
    }];
}

- (void) updateTask{
    self.currentTask=[TLUserTask userTaskWithId:self.currentTask.id];
}

- (IBAction)hideButtonHandleTap:(id)sender {
    
    if(self.currentTask.is_visible.boolValue){
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            TLUserTask *taskInContext=[TLUserTask userTaskWithId:self.currentTask.id inContext:localContext];
            taskInContext.is_visible=@0;
        }];
        [self changeHideButtonImageToRed:YES];
        [self updateTask];
    } else {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            TLUserTask *taskInContext=[TLUserTask userTaskWithId:self.currentTask.id inContext:localContext];
            taskInContext.is_visible=@1;
        }];
        [self changeHideButtonImageToRed:NO];
        [self updateTask];
    }
    
}


- (IBAction)checkInButtonHandelTap:(id)sender {
    if(self.currentTask){
        
        //FOR TASKS
        if(!self.currentTask.is_checked.boolValue){
            if(self.currentTask.is_autocheckin){
                BOOL wasVisible=self.currentTask.is_visible.boolValue;
                [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
                    TLUserTask *taskInContext=[TLUserTask userTaskWithId:self.currentTask.id inContext:localContext];
                    taskInContext.is_checked=@1;
                    if(self.currentTask.is_autocheckin.boolValue && !wasVisible){
                        taskInContext.is_visible=@1;
                    }
                }];
                
                [self changeCheckinButtonImage:self.currentTask.is_autocheckin? dImageAutocheckin : dImageManualcheckin red:YES];
                if(self.currentTask.is_autocheckin.boolValue && !wasVisible){
                    [self changeHideButtonImageToRed:NO];
                }
                [self.checkinButton setUserInteractionEnabled:NO];
                [self updateTask];
            } else {
                
                self.manualCheckinIndicator.hidden=NO;
                loadingCheckin=YES;
                TLPlacesAPIType placesAPIType=[[[NSUserDefaults standardUserDefaults] objectForKey:kPlacesAPIType] integerValue];
                [[TLGeoManager sharedManager] makeCheckinWithUserTask:self.currentTask withAPI:placesAPIType completionBlock:^{
                    self.manualCheckinIndicator.hidden=YES;
                    [SVProgressHUD showSuccessWithStatus:@"Чекин подтвержден"];
                    [self changeCheckinButtonImage:dImageManualcheckin red:YES];
                    self.checkinButton.userInteractionEnabled=NO;
                    [self updateTask];
                } failureBlock:^{
                    [SVProgressHUD showErrorWithStatus:@"Не удалось загрузить данные для чекина"];
                    self.manualCheckinIndicator.hidden=YES;
                    [self updateTask];
                }];
                
            }
        
        }
    } else {
        //FOR CURRENT ADDRESS
        self.manualCheckinIndicator.hidden=NO;
        loadingCheckin=YES;
        TLPlacesAPIType placesAPIType=[[[NSUserDefaults standardUserDefaults] objectForKey:kPlacesAPIType] integerValue];
        [[TLGeoManager sharedManager] makeCheckinWithLocation:[[TLGeoManager sharedManager] getCurrentLocation] withAPI:placesAPIType completionBlock:^{
            self.manualCheckinIndicator.hidden=YES;
            [SVProgressHUD showSuccessWithStatus:@"Чекин подтвержден"];
        } failureBlock:^{
            [SVProgressHUD showErrorWithStatus:@"Не удалось загрузить данные для чекина"];
            self.manualCheckinIndicator.hidden=YES;
        } isCurrentAdress:YES withDate:0 photo:nil];

    }

}

- (IBAction)takePhotoButtonTap:(id)sender {
    
    UIImagePickerController *imageController = [[UIImagePickerController alloc] init];
    imageController.delegate = self;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Выбрать фото"
                                                                  message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *libButton = [UIAlertAction actionWithTitle:@"Выбрать из галлереи"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * _Nonnull action) {
                                                          imageController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                          [self presentViewController:imageController
                                                                             animated:YES
                                                                           completion:nil];

                                                      }];
    

    if([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIAlertAction *cameraButton=[UIAlertAction actionWithTitle:@"Снять на камеру"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction * _Nonnull action) {
                                                               imageController.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                               [self presentViewController:imageController
                                                                                  animated:YES
                                                                                completion:nil];
                                                           }];
        [alert addAction:cameraButton];
    }
    

    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle: @"Отмена"
                                                           style: UIAlertActionStyleCancel
                                                         handler:nil];
    [alert addAction:libButton];
    [alert addAction:cancelButton];
    

    [self presentViewController:alert animated: YES completion: nil];
}

#pragma mark - <UIImagePicker delegate>

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = info[UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        TLUserTask *taskInCont=[TLUserTask userTaskWithId:self.currentTask.id inContext:localContext];
        
        TLUserTaskPhoto *photo=[TLUserTaskPhoto MR_createEntityInContext:localContext];
        photo.image=image;
        NSOrderedSet<TLUserTaskPhoto*> *photoSet = [[NSOrderedSet alloc] initWithObjects:photo, nil];
        taskInCont.taskPhoto=photoSet;
        
        [self changeMakePhotoButtonToChecked:YES];
        [self.delegate setDetailPhoto:photoSet];
    }];
    
}

#pragma mark - <UITextField Delegate>

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self acceptNewNameAndDismissKeyboard];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

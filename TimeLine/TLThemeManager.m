//
//  TLThemeManager.m
//  TimeLine
//
//  Created by Evgenii Oborin on 13.04.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLThemeManager.h"



@implementation TLThemeManager

+ (TLTheme) currentTheme{
    NSNumber *themeNumber= [[NSUserDefaults standardUserDefaults] objectForKey:sSelectedThemeKey];
    if(themeNumber){
        return themeNumber.integerValue;
    } else {
        return TLThemeOcher;
    }
}

+ (UIColor*) mainAccentColor{
    switch ([self currentTheme]) {
        default:
        case TLThemeOcher:
            return [UIColor colorWithRed:228/255.0 green:148/255.0 blue:74/255.0 alpha:1];
            break;
    }
}

+ (void) applyTheme:(TLTheme) theme{
    
    [[NSUserDefaults standardUserDefaults] setValue:@(theme) forKey:sSelectedThemeKey];
}

+ (UIColor*) lighterGrayColor{
    switch ([self currentTheme]) {
        default:
        case TLThemeOcher:
            return [UIColor colorWithRed:163/255.0 green:163/255.0 blue:163/255.0 alpha:1];
            break;
    }
}

+ (UIColor*) gradientIntermediateColor{
    switch ([self currentTheme]) {
        default:
        case TLThemeOcher:
            return [UIColor colorWithRed:227/255.0 green:183/255.0 blue:109/255.0 alpha:1];
            break;
    }
}

+ (UIColor*) statusBarBackgroundColor{
    switch ([self currentTheme]) {
        default:
        case TLThemeOcher:
            return [UIColor colorWithRed:233/255.0 green:237/255.0 blue:231/255.0 alpha:1];
            break;
    }
}





@end

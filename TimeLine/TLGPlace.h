//
//  TLGPlace.h
//  TimeLine
//
//  Created by Evgenii Oborin on 22.08.16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLGPlaceAdressComponentType : NSObject

@property (strong,nonatomic) NSString *componentType;

@end

@interface TLGPlaceAdressComponent : NSObject

@property (strong,nonatomic) NSString *longName;
@property (strong, nonatomic) NSString *shortName;
@property (strong,nonatomic) NSArray *types; //<TLGPlaceAdressComponentType*>

@end

@interface TLGPlaceDetails : NSObject

@property (strong,nonatomic) NSNumber *rating;
@property (strong,nonatomic) NSString *formattedAddress; //formatted_address
@property (strong,nonatomic) NSArray *addressComponents; //<TLGPlaceAressComponent*>

- (NSString *) shortAddress;

@end


@interface TLGPlace : NSObject

@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *placeID;
@property (strong,nonatomic) NSNumber *lat;
@property (strong,nonatomic) NSNumber *lon;
@property (strong,nonatomic) NSNumber *distance;

@property (strong,nonatomic) TLGPlaceDetails *details;

- (CLLocation*) location;

@end



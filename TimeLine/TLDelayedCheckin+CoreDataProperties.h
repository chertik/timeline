//
//  TLDelayedCheckin+CoreDataProperties.h
//  TimeLine
//
//  Created by Evgenii Oborin on 13.03.17.
//  Copyright © 2017 InspireLab. All rights reserved.
//

#import "TLDelayedCheckin.h"


NS_ASSUME_NONNULL_BEGIN

@interface TLDelayedCheckin (CoreDataProperties)

+ (NSFetchRequest<TLDelayedCheckin *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *isDefined;
@property (nullable, nonatomic, copy) NSNumber *latitude;
@property (nullable, nonatomic, copy) NSNumber *longitude;
@property (nullable, nonatomic, copy) NSNumber *orderValue;
@property (nullable, nonatomic, copy) NSDate *time;
@property (nullable, nonatomic, retain) NSOrderedSet<TLPlaceData *> *placesList;
@property (nullable, nonatomic, retain) NSOrderedSet<TLLocation *> *route;

@end

@interface TLDelayedCheckin (CoreDataGeneratedAccessors)

- (void)insertObject:(TLPlaceData *)value inPlacesListAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPlacesListAtIndex:(NSUInteger)idx;
- (void)insertPlacesList:(NSArray<TLPlaceData *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePlacesListAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPlacesListAtIndex:(NSUInteger)idx withObject:(TLPlaceData *)value;
- (void)replacePlacesListAtIndexes:(NSIndexSet *)indexes withPlacesList:(NSArray<TLPlaceData *> *)values;
- (void)addPlacesListObject:(TLPlaceData *)value;
- (void)removePlacesListObject:(TLPlaceData *)value;
- (void)addPlacesList:(NSOrderedSet<TLPlaceData *> *)values;
- (void)removePlacesList:(NSOrderedSet<TLPlaceData *> *)values;

- (void)insertObject:(TLLocation *)value inRouteAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRouteAtIndex:(NSUInteger)idx;
- (void)insertRoute:(NSArray<TLLocation *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeRouteAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInRouteAtIndex:(NSUInteger)idx withObject:(TLLocation *)value;
- (void)replaceRouteAtIndexes:(NSIndexSet *)indexes withRoute:(NSArray<TLLocation *> *)values;
- (void)addRouteObject:(TLLocation *)value;
- (void)removeRouteObject:(TLLocation *)value;
- (void)addRoute:(NSOrderedSet<TLLocation *> *)values;
- (void)removeRoute:(NSOrderedSet<TLLocation *> *)values;

@end

NS_ASSUME_NONNULL_END

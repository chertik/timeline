//
//  ASShapeButton.m
//  TimeLine
//
//  Created by Shagaleev Alexey on 10/04/16.
//  Copyright © 2016 InspireLab. All rights reserved.
//

#import "ASShapeButton.h"
#import "CALayer+Additional.h"
#import "UIImage+Additional.h"

#define kBorderWidth 2.0f
#define kCornerRadius 10.0f

@implementation ASShapeButton {
    CALayer *borderLayer;
}

@synthesize color = _color;
@synthesize shape = _shape;
@synthesize type = _type;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.color = [UIColor whiteColor];
        _type = ASShapeButtonTypeBorder;
        _shape = ASShapeNone;
        
        
        [self setupUI];
    }
    
    return self;
}

- (instancetype)init {
    if (self = [super init]) {
        _type = ASShapeButtonTypeBorder;
        _shape = ASShapeNone;
        _isPhoto=NO;
        
        [self setupUI];
    }
    
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _type = ASShapeButtonTypeBorder;
        _shape = ASShapeNone;
        
        [self setupUI];
    }
    
    return self;
}

- (instancetype)initWithImage:(UIImage *)image shape:(ASShape)shape {
    if (self = [super init]) {
        _shape = shape;
        _type = ASShapeButtonTypeBorder;
        
        [self setupUI];
    }
    
    return self;
}

- (void)setupUI {
    if (self.color == nil)
        self.color = [UIColor whiteColor];
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    
    if (self.imageView.image) {
        [self replaceImageColor];
    }
    
    [self drawBorder];
}

- (void)replaceImageColor {
    UIImage *image = self.imageView.image;
    
    image = [self prepareImage:image];
    
    [self setImage:image forState:UIControlStateNormal];
    [self.imageView setTintColor:self.color];
}

- (UIImage *)prepareImage:(UIImage *)image {
    if(_isPhoto){
        return [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)drawBorder {
    if (borderLayer)
        [borderLayer removeFromSuperlayer];
    
    /*switch (self.shape) {
        case ASShapeSquare:
            borderLayer = [self squareLayerWithColor:self.color];
            break;
            
        case ASShapeRhombus:
            borderLayer = [self rhombusLayerWithColor:self.color];
            break;
            
        default:
            break;
    }*/
    borderLayer = [self squareLayerWithColor:self.color];
    
    CGRect frame = self.bounds;
    CGFloat offset = 0.0f;
    
    if (self.shape == ASShapeRhombus) {
        CGFloat coefficent = 0.75f;
        
        frame.origin.x += frame.size.width * (1 - coefficent) / 2;
        frame.origin.y += frame.size.height * (1 - coefficent) / 2;
        
        frame.size.width *= coefficent;
        frame.size.height *= coefficent;
    }
    
    frame.origin.x += offset;
    frame.origin.y += offset;
    
    frame.size.width -= 2 * offset;
    frame.size.height -= 2 * offset;
    
    borderLayer.frame = frame;
    
    if (self.shape == ASShapeRhombus) {
        CGAffineTransform rotateTransform = CGAffineTransformMakeRotation(M_PI / 4.0);
       
        [borderLayer setAffineTransform:rotateTransform];
    }
    
    switch (self.type) {
        case ASShapeButtonTypeBorder:
            [self.imageView setTintColor:self.color];
            break;
            
        case ASShapeButtonTypeNonborder: {
            borderLayer.borderWidth = 0.0f;
            borderLayer.backgroundColor = self.color.CGColor;
            
            [self.imageView setTintColor:[UIColor clearColor]];
            
            break;
        }
            
        default:
            break;
    }
    
    
    [self.layer addSublayer:borderLayer];
}

- (CALayer *)squareLayerWithColor:(UIColor *)color {
    CALayer *layer = [CALayer squareLayerWithBorderWidth:2
                                             borderColor:color
                                            cornerRadius:12.0f];
    return layer;
}

- (CALayer *)rhombusLayerWithColor:(UIColor *)color {
    CALayer *layer = [CALayer rhombusLayerWithBorderWidth:1.0f
                                              borderColor:color
                                             cornerRadius:4.0f];
    
    return layer;
}

#pragma mark - Replaced



- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat offset = self.bounds.size.height * 0.25f; //9.0f;
    
    if(_isPhoto){
        CGRect frameImage = self.bounds;//CGRectMake(offset, offset, self.bounds.size.width - 2 * offset, self.bounds.size.height - 2 * offset);
        
        self.imageView.frame = frameImage;
        
        CAShapeLayer *mask=[CAShapeLayer layer];
        
        CGRect frame = self.bounds;
        if (self.shape == ASShapeRhombus) {
            CGFloat coefficent = 0.75f;
            
            frame.origin.x += frame.size.width * (1 - coefficent) / 2;
            frame.origin.y += frame.size.height * (1 - coefficent) / 2;
            
            frame.size.width *= coefficent;
            frame.size.height *= coefficent;
        }
        
        
        UIBezierPath *path=[UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:12.0];
        
        CGAffineTransform translateTransform=CGAffineTransformMakeTranslation(-CGRectGetMidX(frame), -CGRectGetMidY(frame));
        CGAffineTransform backTranslateTransform=CGAffineTransformMakeTranslation(CGRectGetMidX(frame), CGRectGetMidY(frame));
        
        CGAffineTransform rotateTransform = CGAffineTransformMakeRotation(M_PI / 4.0);
        [path applyTransform:translateTransform];
        [path applyTransform:rotateTransform];
        [path applyTransform:backTranslateTransform];
        
        mask.path=path.CGPath;
        
        self.imageView.layer.mask=mask;
        
    } else {
        CGRect frameImage = CGRectMake(offset, offset, self.bounds.size.width - 2 * offset, self.bounds.size.height - 2 * offset);
        self.imageView.frame = frameImage;
    }
    
    [self bringSubviewToFront:self.imageView];
    
    if (self.shape == ASShapeSquare) {
        borderLayer.frame = self.bounds;
    }
    
    
}

#pragma mark - Getters/Setters

- (void)setColor:(UIColor *)color {
    _color = color;
    
    [self replaceImageColor];
}

- (void)setShape:(ASShape)shape {
    _shape = shape;
    
    [self drawBorder];
}

- (void)setType:(ASShapeButtonType)type {
    _type = type;
    
    [self drawBorder];
}

- (void) setIsPhoto:(BOOL)isPhoto{
    _isPhoto=isPhoto;
    
    [self layoutSubviews];
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state {
    [super setImage:[self prepareImage:image] forState:state];
}

@end

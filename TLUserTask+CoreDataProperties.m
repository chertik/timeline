//
//  TLUserTask+CoreDataProperties.m
//  
//
//  Created by Evgenii Oborin on 13.03.17.
//
//

#import "TLUserTask+CoreDataProperties.h"

@implementation TLUserTask (CoreDataProperties)

+ (NSFetchRequest<TLUserTask *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"TLUserTask"];
}

@dynamic address;
@dynamic datetime;
@dynamic dayName;
@dynamic desc;
@dynamic icon_path;
@dynamic id;
@dynamic is_autocheckin;
@dynamic is_checked;
@dynamic is_current_location_holder;
@dynamic is_past;
@dynamic is_timeline;
@dynamic is_visible;
@dynamic latitude;
@dynamic longitude;
@dynamic photo_path;
@dynamic polyline_id;
@dynamic route_id;
@dynamic selected_place_id;
@dynamic z_index;
@dynamic placesList;
@dynamic taskPhoto;
@dynamic routeToNextTask;

@end

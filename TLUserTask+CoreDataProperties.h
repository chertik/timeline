//
//  TLUserTask+CoreDataProperties.h
//  
//
//  Created by Evgenii Oborin on 13.03.17.
//
//

#import "TLUserTask+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface TLUserTask (CoreDataProperties)

+ (NSFetchRequest<TLUserTask *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSDate *datetime;
@property (nullable, nonatomic, copy) NSString *dayName;
@property (nullable, nonatomic, copy) NSString *desc;
@property (nullable, nonatomic, copy) NSString *icon_path;
@property (nullable, nonatomic, copy) NSNumber *id;
@property (nullable, nonatomic, copy) NSNumber *is_autocheckin;
@property (nullable, nonatomic, copy) NSNumber *is_checked;
@property (nullable, nonatomic, copy) NSNumber *is_current_location_holder;
@property (nullable, nonatomic, copy) NSNumber *is_past;
@property (nullable, nonatomic, copy) NSNumber *is_timeline;
@property (nullable, nonatomic, copy) NSNumber *is_visible;
@property (nullable, nonatomic, copy) NSNumber *latitude;
@property (nullable, nonatomic, copy) NSNumber *longitude;
@property (nullable, nonatomic, copy) NSString *photo_path;
@property (nullable, nonatomic, copy) NSNumber *polyline_id;
@property (nullable, nonatomic, copy) NSNumber *route_id;
@property (nullable, nonatomic, copy) NSNumber *selected_place_id;
@property (nullable, nonatomic, copy) NSNumber *z_index;
@property (nullable, nonatomic, retain) NSOrderedSet<TLPlaceData *> *placesList;
@property (nullable, nonatomic, retain) TLUserTaskPhoto *taskPhoto;
@property (nullable, nonatomic, retain) NSOrderedSet<TLLocation *> *routeToNextTask;

@end

@interface TLUserTask (CoreDataGeneratedAccessors)

- (void)insertObject:(TLPlaceData *)value inPlacesListAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPlacesListAtIndex:(NSUInteger)idx;
- (void)insertPlacesList:(NSArray<TLPlaceData *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePlacesListAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPlacesListAtIndex:(NSUInteger)idx withObject:(TLPlaceData *)value;
- (void)replacePlacesListAtIndexes:(NSIndexSet *)indexes withPlacesList:(NSArray<TLPlaceData *> *)values;
- (void)addPlacesListObject:(TLPlaceData *)value;
- (void)removePlacesListObject:(TLPlaceData *)value;
- (void)addPlacesList:(NSOrderedSet<TLPlaceData *> *)values;
- (void)removePlacesList:(NSOrderedSet<TLPlaceData *> *)values;

- (void)insertObject:(TLLocation *)value inRouteToNextTaskAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRouteToNextTaskAtIndex:(NSUInteger)idx;
- (void)insertRouteToNextTask:(NSArray<TLLocation *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeRouteToNextTaskAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInRouteToNextTaskAtIndex:(NSUInteger)idx withObject:(TLLocation *)value;
- (void)replaceRouteToNextTaskAtIndexes:(NSIndexSet *)indexes withRouteToNextTask:(NSArray<TLLocation *> *)values;
- (void)addRouteToNextTaskObject:(TLLocation *)value;
- (void)removeRouteToNextTaskObject:(TLLocation *)value;
- (void)addRouteToNextTask:(NSOrderedSet<TLLocation *> *)values;
- (void)removeRouteToNextTask:(NSOrderedSet<TLLocation *> *)values;

@end

NS_ASSUME_NONNULL_END

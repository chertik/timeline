//
//  TLUserTask+CoreDataClass.h
//  
//
//  Created by Evgenii Oborin on 13.03.17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TLLocation, TLPlaceData, TLUserTaskPhoto;

NS_ASSUME_NONNULL_BEGIN

@interface TLUserTask : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "TLUserTask+CoreDataProperties.h"
